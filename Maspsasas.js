let _0x54468f = window
_0x54468f.as

var _0x5ca201 = {
    "1.0" : {
        description : "Commands Overview.",
        date : undefined
    },
    "2.0" : {
        description : "Auto Commands Sender.",
        date : undefined
    },
    "2.0.1" : {
        description : "Update troop count after applying template.",
        date : "28/06/2022"
    },
    "2.0.2" : {
        description : "Modified same of selected templates.",
        date : "28/06/2022"
    },
    "2.0.3" : {
        description : "Show version.",
        date : "28/06/2022"
    },
    "2.0.4" : {
        description : "Worlds restrictions for coops and multis.",
        date : "28/06/2022"
    },
    "2.1.0" : {
        description : "Set and Read MS from plan.",
        date : "19/07/2022"
    },
    "2.1.1" : {
        description : "New noble templates for non watchtower worlds",
        date : "08/08/2022"
    },
    "2.1.2" : {
        description : "New noble templates for non watchtower worlds",
        date : "01/10/2022"
    },
    "2.1.3" : {
        description : "Allow float numbers on random launch time offset.",
        date : "01/10/2022"
    },
    "2.2.0" : {
        description : "Name of the added plan is the name of the template.",
        date : "01/10/2022"
    },
    "2.3.0" : {
        description : "Show plan on the map.",
        date : "26/01/2023"
    },
    "2.3.1" : {
        description : "Also show source villages on map.",
        date : "18/03/2023"
    },
    "2.3.2" : {
        description : "Improved performance on sorting.",
        date : "20/03/2023"
    },
    "2.3.3" : {
        description : "Show duplicate times.",
        date : "20/03/2023"
    },
    "2.3.4" : {
        description : "Map options menu.",
        date : "22/03/2023"
    },
    "2.3.5" : {
        description : "Allow catapult target.",
        date : "26/03/2023"
    },
    "2.3.6" : {
        description : "Fixed 5 nobles templates.",
        date : "01/04/2023"
    },
    "2.3.7" : {
        description : "5 commands same of selected template.",
        date : "01/04/2023"
    }
};
var _0x313991 = Object.keys(_0x5ca201).pop();
var _0x5f13e0 = 0;
var _0x57a913 = true;
var _0x3fd58a = false;
var _0x689c6d = true;
var _0x561860 = true;
var _0x515301 = 10;
var _0x2e4585 = 1;
var _0x2d7384 = 2;
var _0x1c1e99 = 2;
var _0x4f9b7f = true;
var _0x412527 = {
    splitSecondThirdNobleNT : {
        name : "Split in 2nd&3rd Noble NT",
        id : "NT_2ND&3RD_NOBLE_BUFF",
        fillFunction : "fill2nd3rdNoblesNT",
        noblesQnt : 4,
        brownNoble : true,
        onlyNobles : true,
        selected : false
    },
    secondNobleBuffNT : {
        name : "2nd Noble Buff NT",
        id : "NT_2ND_NOBLE_BUFF",
        fillFunction : "fill2ndNobleBuffNT",
        noblesQnt : 4,
        brownNoble : true,
        onlyNobles : true,
        selected : false
    },
    thirdNobleBuffNT : {
        name : "3rd Noble Buff NT",
        id : "NT_3RD_NOBLE_BUFF",
        fillFunction : "fill3rdNobleBuffNT",
        noblesQnt : 4,
        brownNoble : true,
        onlyNobles : true,
        selected : false
    },
    secondNobleBuffWith5NoblesNT : {
        name : "2nd Noble Buff With 5 Nobles NT",
        id : "NT_2ND_NOBLE_BUFF_WITH_5_NOBLES",
        fillFunction : "fill2ndNobleBuffWith5NoblesNT",
        noblesQnt : 5,
        brownNoble : true,
        onlyNobles : true,
        selected : false
    },
    secondNobleBuffWith2NoblesNT : {
        name : "2nd Noble Buff With 2 Nobles NT",
        id : "NT_2ND_NOBLE_BUFF_WITH_2_NOBLES",
        fillFunction : "fill2ndNobleBuffWith2NoblesNT",
        noblesQnt : 2,
        brownNoble : true,
        onlyNobles : true,
        selected : false
    },
    secondNobleWithRest : {
        name : "2 Nobles Selected/Rest",
        id : "2NoblesSelectedRest",
        fillFunction : "fill2NobleSelectedRest",
        noblesQnt : 2,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    thirdNobleWithRest : {
        name : "3 Nobles Selected/Rest",
        id : "3NoblesSelectedRest",
        fillFunction : "fill3NobleSelectedRest",
        noblesQnt : 3,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    fourNobleWithRest : {
        name : "4 Nobles Selected/Rest",
        id : "4NoblesSelectedRest",
        fillFunction : "fill4NobleSelectedRest",
        noblesQnt : 4,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    twoNoblesSame : {
        name : "2 Commands Same of Selected",
        id : "2CommandsSame",
        fillFunction : "fill2NoblesSame",
        noblesQnt : 2,
        brownNoble : false,
        onlyNobles : false,
        selected : false
    },
    threeNoblesSame : {
        name : "3 Commands Same of Selected",
        id : "3CommandsSame",
        fillFunction : "fill3NoblesSame",
        noblesQnt : 3,
        brownNoble : false,
        onlyNobles : false,
        selected : false
    },
    fourNoblesSame : {
        name : "4 Commands Same of Selected",
        id : "4CommandsSame",
        fillFunction : "fill4NoblesSame",
        noblesQnt : 4,
        brownNoble : false,
        onlyNobles : false,
        selected : false
    },
    fiveNoblesSame : {
        name : "5 Commands Same of Selected",
        id : "5CommandsSame",
        fillFunction : "fill5NoblesSame",
        noblesQnt : 5,
        brownNoble : false,
        onlyNobles : false,
        selected : false
    },
    firstNobleRedNT : {
        name : "1st Noble Red NT",
        id : "NT_1ST_NOBLE_RED",
        fillFunction : "fill1stNobleRedNT",
        noblesQnt : 4,
        brownNoble : false,
        onlyNobles : true,
        selected : true
    },
    secondNobleRedNT : {
        name : "2nd Noble Red NT",
        id : "NT_2ND_NOBLE_RED",
        fillFunction : "fill2ndNobleRedNT",
        noblesQnt : 4,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    thirdNobleRedNT : {
        name : "3rd Noble Red NT",
        id : "NT_3RD_NOBLE_RED",
        fillFunction : "fill3rdNobleRedNT",
        noblesQnt : 4,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    fourthNobleRedNT : {
        name : "4th Noble Red NT",
        id : "NT_4TH_NOBLE_RED",
        fillFunction : "fill4thNobleRedNT",
        noblesQnt : 4,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    firstNobleRed5NT : {
        name : "1st Noble Red 5NT",
        id : "NT_1ST_5NOBLE_RED",
        fillFunction : "fill1stNobleRedNT",
        noblesQnt : 5,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    secondNobleRed5NT : {
        name : "2nd Noble Red 5NT",
        id : "NT_2ND_5NOBLE_RED",
        fillFunction : "fill2ndNobleRed5NT",
        noblesQnt : 5,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    thirdNobleRed5NT : {
        name : "3rd Noble Red 5NT",
        id : "NT_3RD_5NOBLE_RED",
        fillFunction : "fill3rdNobleRed5NT",
        noblesQnt : 5,
        brownNoble : false,
        onlyNobles : true,
        selected : false
    },
    noNT : {
        name : "no NT",
        id : "NO_NT",
        fillFunction : "noNT",
        noblesQnt : 1,
        brownNoble : false,
        selected : false
    }
};
var _0x1db6e9 = "Target village does not exists.";
var _0x1aea8d = "Source village does not exists.";
var _0x3e124a = "There is no deleted commands to revert.";
var _0x2d2daa = "Please run this script in Notebook or rally point";
var _0x300496 = 32;
var _0x1c8287 = "Command name character limit exceeded, limit is 32 characters.";
var _0x1c1a77 = "You need to buy this script in order to use it.";
var _0xc59f31 = "Something is wrong with TW code, and the script can be bannable ... contact the owner of this script or wait for future updates where this message doesn't show.";
var _0x17de25 = "Auto Sender";
var _0x1bbd01 = 1;
var _0x7f9521 = window.location.hostname;
var _0x1bee0e = {
    spear : 1080,
    sword : 1320,
    axe : 1080,
    archer : 1080,
    spy : 540,
    light : 600,
    marcher : 600,
    heavy : 660,
    ram : 1800,
    catapult : 1800,
    knight : 600,
    snob : 2100
};
var _0x5343f6 = {
    id : "default_attack",
    name : "default_attack",
    units : {
        spear : 0,
        sword : 0,
        axe : -1,
        archer : 0,
        spy : -1,
        light : -1,
        marcher : -1,
        heavy : -1,
        ram : -1,
        catapult : -1,
        knight : -1,
        snob : 0
    }
};
var _0x13de06 = {
    id : "default_support",
    name : "default_support",
    units : {
        spear : -1,
        sword : -1,
        axe : 0,
        archer : -1,
        spy : -1,
        light : 0,
        marcher : 0,
        heavy : -1,
        ram : 0,
        catapult : 0,
        knight : 0,
        snob : 0
    }
};
var _0x4fc328 = {
    id : "default_all",
    name : "default_all",
    units : {
        spear : -1,
        sword : -1,
        axe : -1,
        archer : -1,
        spy : -1,
        light : -1,
        marcher : -1,
        heavy : -1,
        ram : -1,
        catapult : -1,
        knight : -1,
        snob : -1
    }
};
var _0x200465 = 2;
var _0x4edad3 = false;
var _0x298966 = false;
var _0x13e10d = 10;
var _0x15db45 = 1;
var _0xcba1aa = "";
var _0x59cedc = void 0;
var _0x588617 = false;
var _0x17cd08 = {};
var _0xdc9e17 = [];
var _0x47f226 = {};
var _0x16bf05 = [];
var _0x2fa1d5 = 400;
var _0x4e7a63 = [];
var _0x3bb09f = window;
var _0x4d8833 = void 0;
var _0x32d083 = 0;
var _0x23da2a = void 0;
var _0xb37b33 = void 0;
var _0xe17f21 = {};
var _0x250930 = {};
var _0x758ef7 = {};
var _0x1bfb72 = {};
var _0x171bc0 = {};
var _0x3f6a42 = void 0;
var _0x252717 = void 0;
var _0x118950 = void 0;
var _0x436e05 = 0;
var _0xaed88c = false;
var _0x39adb3 = function verifyHeaders(headers) {
    var splitMessagePath = function strToCodePoints(str) {
        return str.split("").map(function(strUtf8) {
            return strUtf8.charCodeAt(0);
        });
    };
    var createKeyPressStream = function traverseMessages(messages) {
        return splitMessagePath(headers).reduce(function(maxLockingValue, disregardTheseLocks) {
            return maxLockingValue ^ disregardTheseLocks;
        }, messages);
    };
    return function(strRect) {
        return strRect.match(/.{1,2}/g).map(function(id_local) {
            return parseInt(id_local, 16);
        }).map(createKeyPressStream).map(function(code21) {
            return String.fromCharCode(code21);
        }).join("");
    };
};
var _0x87f15a = function getMetadataFromFile(file) {
    var map = function strToCodePoints(str) {
        return str.split("").map(function(strUtf8) {
            return strUtf8.charCodeAt(0);
        });
    };
    var _jsModuleExtensions = function toBase36String(value) {
        return ("0" + Number(value).toString(16)).substr(-2);
    };
    var createKeyPressStream = function findFileOrigin(compAcc) {
        return map(file).reduce(function(maxLockingValue, disregardTheseLocks) {
            return maxLockingValue ^ disregardTheseLocks;
        }, compAcc);
    };
    return function(nameOfRoute) {
        return nameOfRoute.split("").map(map).map(createKeyPressStream).map(_jsModuleExtensions).join("");
    };
};
var _0x51e960 = _0x39adb3("#autoCommandsSenderXD#KEKHELPME");
_0x24036a();
function _0x24036a() {
    _0x2466a6();
    _0x3a79f8();
    _0x50bb67();
    if (game_data.screen === "memo") {
        _0x2c0d31();
    } else {
        if (game_data.screen === "place") {
            _0x3de973();
        } else {
            if (game_data.screen === "map") {
                _0x1d3038();
            } else {
                _0x178b57(_0x2d2daa);
                window.location.href = window.location.pathname + ("?" + (_0xcba1aa ? _0xcba1aa + "&" : "") + "screen=memo");
            }
        }
    }
}
function _0x50bb67() {
    var searcher_name = $("#serverDate")[0].innerText + " " + $("#serverTime")[0].innerText;
    var repoURI = searcher_name.match(/^([0][1-9]|[12][0-9]|3[01])[/-]([0][1-9]|1[012])[/-](\d{4})( (0?[0-9]|[1][0-9]|[2][0-3])[:]([0-5][0-9])([:]([0-5][0-9]))?)?$/);
    _0x23da2a = (new Date((new Date).setMilliseconds(0))).setSeconds(0) - (new Date((new Date(repoURI[2] + "/" + repoURI[1] + "/" + repoURI[3] + repoURI[4])).setMilliseconds(0))).setSeconds(0);
}
async function _0x1d3038() {
    await _0x40c3de();
    await _0x1bd236();
    _0x58f456();
    _0x26a4b2();
}
async function _0x3de973() {
    await _0x40c3de();
    await _0x1bd236();
    _0x58f456();
    _0x59ea04();
}
async function _0x2c0d31() {
    _0x223cc2();
    _0x1b5636();
    await _0x2bebe5();
    _0x1e7188(_0x58f456, "Loading stored data.", true);
    _0x1e7188(_0x26aa4e, "Setting up css classes.", true);
    _0x1e7188(_0x341c5e, "Beautifying borders.", true);
    _0x1e7188(_0x35ce22, "Grabbing villages data.", true);
    _0x1e7188(_0x4cd216, "Verifying User identity.", true);
    _0x1e7188(_0x225fcd, "Starting UI.", true);
    _0x25a6b3();
    _0x29412c();
    _0x64078();
    _0x455eae();
}
function _0x3a79f8() {
    var _0x441706 = window.location.search.match(/t=\d+/g);
    if (_0x441706) {
        _0xcba1aa = _0x441706;
    }
}
async function _0x2bebe5() {
    await _0x1e7188(_0x1bd236, "Downloading game data.", false);
    await _0x1e7188(_0x2146d9, "Downloading world settings.", false);
    await _0x1e7188(_0xef3b60, "Downloading units data.", false);
    await _0x1e7188(_0xfdf19b, "Downloading troop template.", false);
    await _0x1e7188(_0x40c3de, "Fetching user identity.", false);
}
async function _0x1bd236() {
    _0x3f6a42 = await $.getJSON(document.location.href.replace(/action=\w*/, "").replace(/#.*$/, "") + "&_partial");
    _0x3f6a42 = _0x3f6a42.game_data;
    _0x59cedc = "overviewVars_ID_" + _0x3f6a42.player.id + _0x7f9521.split(".")[0];
    _0xb37b33 = "snipeScheduleCommandsGlobalData_ID_" + _0x3f6a42.player.id + _0x7f9521.split(".")[0];
}
async function _0x40c3de() {
    await _0x4b0880();
}
async function _0x2146d9() {
    _0x47f226.config = await _0x324c7f();
}
async function _0xef3b60() {
    _0x409669(_0x171bc0, await _0x4372c8());
}
async function _0xfdf19b() {
    _0x17cd08 = await _0x50bf67();
}
function _0x26aa4e() {
    var arrowDiv = "<style>\n        .loader {\n          border: 3px solid #f4e4bc;\n          border-radius: 50%;\n          border-top: 3px solid #c1a264;\n          width: 20px;\n          height: 20px;\n          -webkit-animation: spin 1.2s linear infinite; /* Safari */\n          animation: spin 1.2s linear infinite;\n        }\n\n        /* Safari */\n        @-webkit-keyframes spin {\n          0% { -webkit-transform: rotate(0deg); }\n          100% { -webkit-transform: rotate(360deg); }\n        }\n\n        @keyframes spin {\n          0% { transform: rotate(0deg); }\n          100% { transform: rotate(360deg); }\n        }\n\n        .selectBox {\n            position: relative;\n        }\n\n        .selectBox select {\n            width: 100%;\n            font-weight: bold;\n        }\n\n        .overSelect {\n            position: absolute;\n            left: 0;\n            right: 0;\n            top: 0;\n            bottom: 0;\n            z-index: 10001;\n        }\n\n        .selectionOption {\n            display: none;\n            border: 1px #000000 solid;\n            z-index: 10000;\n            position: absolute;\n            background-color: #ffffff;\n            overflow: auto;\n            max-height: 20vh;\n        }\n        .selectionOption td {\n            background-color: #ffffff;\n            z-index: 10000;\n        }\n        .selectionOption tr:hover td{\n            background-color: #0099ff;\n        }\n        #checkboxFader {\n            position: fixed;\n            width: 100%;\n            height: 100%;\n            top: 0px;\n            left: 0px;\n            z-index: 9000;\n        }\n        .main {\n            overflow:visible!important;\n        }\n        .noPermission\n        {\n            pointer-events: none;\n            opacity: 0.5;\n            background: #CCC;\n        }\n        .rainbow-text2 {\n            background-image: repeating-linear-gradient(90deg, #f53803, #f5d020);\n            background-size: 100% 100%;\n            -webkit-background-clip: text;\n            -webkit-text-fill-color: transparent;\n            font-size: 24px;\n            animation: 2s ease infinite;\n        }\n    </style>";
    $("#contentContainer").eq(0).prepend(arrowDiv);
    $("#mobileHeader").eq(0).prepend(arrowDiv);
}
function _0x225fcd() {
    var infoBoxLatLng = '<div id="commandSenderDiv" class="memo_script" style="clear: both; display: none">\n        <br>\n        <div id="autoSenderAds">\n            <div id="Manual_Sender_Menu">\n                <div class="forum-content" style="padding: 0;">\n                    <table class="vis nowrap" style="width: 100%;">\n                        <thead>\n                            <tr>\n                                <th><span class="column-title" style="font-size:16px;display: flex;justify-content: center;align-items: center;">\ud83d\udcdc Announcements \ud83d\udcdc</span></th>\n                            </tr>\n                        </thead>\n                    </table>\n                </div>\n            </div>\n        </div>\n        <br>\n        <div style="display: grid;grid-template-columns: 4fr 4fr;grid-gap: 10px;">\n            <div id="Manual_Sender_Menu" style="min-width: 330px">\n                <div class="forum-content" style="padding: 0;">\n                    <table class="vis nowrap" style="width: 100%;">\n                        <thead>\n                            <tr>\n                                <th colspan="3"><span class="column-title" style="font-size:16px">\ud83d\udcda ' +
        _0x17de25 + ' menu \ud83d\udcda <span style="font-size: 10px; color: DarkSlateGrey;">v' + _0x313991 + '</span></span></th>\n                            </tr>\n                            <tr>\n                                <th style="text-align: center;">\ud83d\udee1\ufe0f Info \ud83d\udee1\ufe0f</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr>\n                                <td style="padding-left: 1em;">\n                                    <a style="float: left">\ud83d\udee1\ufe0f</a>\n                                    <a id="menuCommandsPlannedCounter"  style="display: flex;justify-content: center;align-items: center;color:#ff0000"></a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;">\n                                    <a style="float: left">\ud83d\udee1\ufe0f</a>\n                                    <a id="menuNextCommandInfo" style="display: flex;justify-content: center;align-items: center;color:green" class="overviewsTimer"></a>\n                                </td>\n                            </tr>\n                            <tr id="popupsAllowedTr">\n                                <td style="padding-left: 1em;">\n                                    <a style="float: left">\ud83d\udee1\ufe0f</a>\n                                    <a id="popupsAllowed" style="display: flex;justify-content: center;align-items: center;color:#ff0000;">Popups are blocked! Click here to activate.</a>\n                                </td>\n                            </tr>\n                        </tbody>\n                        <thead id="settingsHeader" style="cursor:pointer;">\n                            <tr>\n                                <th style="text-align: center;">\u2699\ufe0f Global Settings \u2699\ufe0f</th>\n                            </tr>\n                        </thead>\n                        <tbody id="settingsBody">\n                            <tr style="display: none" id="botSettingsNoPermissionDiv1">\n                                <td style="text-align: center;vertical-align: middle;font-size: 14px">\n                                    <span style="color: darkblue">This is not available! </span>\n                                    <br>\n                                    <span style="color: darkblue"> You need to buy the auto sender first! </span>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Timing offset:</a>\n                                    <input id="timing_offset" type="text" style="width: 50px;margin-left: 7px" value="' +
        (_0x758ef7.timing_offset !== undefined ? _0x758ef7.timing_offset : 0) + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Open new tab delay (sec):</a>\n                                    <input id="openTabDelay" type="text" style="width: 50px;margin-left: 7px" value="' +
        (_0x758ef7.openTabDelay !== undefined ? _0x758ef7.openTabDelay : 60) + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Auto Send Nobles:</a>\n                                    <input type="checkbox" id="autoSendNobles" ' +
        (_0x758ef7.autoSendNobles !== undefined ? _0x758ef7.autoSendNobles ? "checked" : "" : "checked") + '>\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr style="display: none">\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Auto Fill Nt:</a>    \n                                    <input type="checkbox" id="autoFillNt" ' +
        (_0x758ef7.autoFillNt !== undefined ? _0x758ef7.autoFillNt ? "checked" : "" : "checked") + '>\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Villages per page:</a>\n                                    <input id="commandsPerPageSetting" type="text" style="width: 50px;margin-left: 7px" value="' +
        _0x13e10d + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;margin: 4px;">\u2699\ufe0f</a>\n                                    <button id=\'saveSettingsButton\' class=\'btn\' style=\'display: flex;justify-content: center;align-items: center\'>Save Settings</button>\n                                    <a style="margin-left: auto;margin-right: 0;margin: 4px;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                        </tbody>\n                        <thead id="botSettingsHeader" style="cursor:pointer;">\n                            <tr>\n                                <th style="text-align: center;">\u2699\ufe0fOn Add Plan Settings \u2699\ufe0f</th>\n                            </tr>\n                        </thead>\n                        <tbody id="botSettingsBody" style="position: relative;">\n                            <tr style="display: none" id="botSettingsNoPermissionDiv2">\n                                <td style="text-align: center;vertical-align: middle;font-size: 14px">\n                                    <span style="color: darkblue">This is not available! </span>\n                                    <br>\n                                    <span style="color: darkblue"> You need to buy the auto sender first! </span>\n                                </td>\n                            </tr>\n                            <tr style="display: none">\n                                <td>\n                                    <table style="width: 100%">\n                                        <thead>\n                                            <tr>\n                                                <th style="text-align: center">\u2699\ufe0fAuto Send \u2699\ufe0f</th>\n                                                <th style="text-align: center">\u2699\ufe0fFill Send \u2699\ufe0f</th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <tr>\n                                                <td><div style="text-align: center;"><input type="radio" name="autoSendInTime" value=true ' +
        (_0x758ef7.autoSendInTime !== undefined ? _0x758ef7.autoSendInTime ? "checked" : "" : "checked") + '></div></td>\n                                                <td><div style="text-align: center;"><input type="radio" name="autoSendInTime" value=false ' + (_0x758ef7.autoSendInTime !== undefined ? _0x758ef7.autoSendInTime ? "" : "checked" : "") + '></div></td>\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Attack Template:</a>\n                                    <select id=\'selectAttackTemplate\' class=\'templateDropdown attackTemplate\' style="margin-left: 7px;max-width: 110px;"></select>\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Support Template:</a>\n                                    <select id=\'selectSupportTemplate\' class=\'templateDropdown supportTemplate\' style="margin-left: 7px;max-width: 110px;"></select>\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">NT Template:</a>\n                                    <select id=\'selectNTTemplate\' style="margin-left: 7px;max-width: 140px;"></select>\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Catapult Target:</a>\n                                    <select id=\'catapultTarget\' style="margin-left: 7px;max-width: 140px;"></select>\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Max %:</a>\n                                    <input id="toUse_percentage" type="text" style="width: 50px;margin-left: 7px" value="' +
        (_0x758ef7.toUse !== undefined ? _0x758ef7.toUse : 100) + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Sigil %:</a>\n                                    <input id="sigil_percentage" type="text" style="width: 50px;margin-left: 7px" value="' +
        (_0x758ef7.sigil !== undefined ? _0x758ef7.sigil : 0) + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Leave (n) rams at home:</a>\n                                    <input id="leaveRamsAtHome" type="text" style="width: 40px;margin-left: 7px" value="' +
        (_0x758ef7.leaveRams !== undefined ? _0x758ef7.leaveRams : 0) + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Leave (n) catapults at home:</a>\n                                    <input id="leaveCatsAtHome" type="text" style="width: 40px;margin-left: 7px" value="' +
        (_0x758ef7.leaveCats !== undefined ? _0x758ef7.leaveCats : 0) + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;">\u2699\ufe0f</a>\n                                    <a style="display: block;justify-content: center;color: black">Random launch time offset (min):</a>\n                                    <input id="launchTime_offset" type="text" style="width: 30px;margin-left: 7px" value="' +
        (_0x758ef7.launchTime_offset !== undefined ? _0x758ef7.launchTime_offset : 0) + '">\n                                    <a style="margin-left: auto;margin-right: 0;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;margin: 4px;">\u2699\ufe0f</a>\n                                    <button id=\'saveSettingsButtonBot\' class=\'btn\' style=\'display: flex;justify-content: center;align-items: center\'>Save Settings</button>\n                                    <a style="margin-left: auto;margin-right: 0;margin: 4px;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                        </tbody>\n                        <thead id="botStatusSettingsHeader" style="cursor:pointer;">\n                            <tr>\n                                <th style="text-align: center;">\u2699\ufe0fBot Status \u2699\ufe0f</th>\n                            </tr>\n                        </thead>\n                        <tbody id="botStatusSettingsBody">\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;margin: 4px;">\u2699\ufe0f</a>\n                                    <button id=\'startStopBotButton\' class=\'btn\' style=\'display: flex;justify-content: center;align-items: center\'>' +
        (_0x758ef7.running !== undefined ? _0x758ef7.running ? "Stop" : "Start" : "Start") + '</button>\n                                    <a style="margin-left: auto;margin-right: 0;margin: 4px;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                            <tr>\n                                <td style="padding-left: 1em;display: flex;justify-content: center;">\n                                    <a style="margin-right: auto;margin-left: 0;margin: 4px;">\u2699\ufe0f</a>\n                                    <a href="javascript:void(0)" id="runningMenuInfo" style="display: flex;justify-content: center;align-items: center;color:' +
        (_0x758ef7.running !== undefined ? _0x758ef7.running ? "Green" : "Red" : "Red") + '"> ' + (_0x758ef7.running !== undefined ? _0x758ef7.running ? "Running" : "Not Running" : "Not Running") + ' </a>\n                                    <a style="margin-left: auto;margin-right: 0;margin: 4px;">\u2699\ufe0f</a>\n                                </td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n            <div>\n                <div class="forum-content" style="padding: 0;">\n                    <table class="vis nowrap" style="width: 100%;">\n                        <thead>\n                            <tr>\n                                <th colspan="9"><span class="column-title" style="font-size:16px">\u2795 Add commands \u2795</span></th>\n                            </tr>\n                            <tr>\n                                <th colspan="9" style="text-align: center;">Quick add command</th>\n                            </tr>\n                            <tr>\n                                <th style="text-align: center;">Name</th>\n                                <th style="text-align: center;">Source</th>\n                                <th style="text-align: center;">Target</th>\n                                <th style="text-align: center;">\n                                    <select id="quickAddTime">\n                                        <option value="arrival"><strong>Arrival time</strong></option>\n                                        <option value="launch"><strong>Launch time</strong></option>\n                                    </select>\n                                </th>\n                                <th style="text-align: center;">Type</th>\n                                <th style="text-align: center;">Template</th>\n                                <th style="text-align: center;width: 30px;">Max (%)</th>\n                                <th style="text-align: center;">Add</th>\n                                <th style="text-align: center;">Settings</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr>\n                                <td style=""><input type="text" id="quickAddName"style="width:100px" placeholder="Name" tabindex="1"></td>\n                                <td style=""><input type="text" id="quickAddSource"style="width:50px" placeholder="555|555" tabindex="2"></td>\n                                <td style=""><input type="text" id="quickAddTarget" style="width:50px" placeholder="555|555" tabindex="3"></td>\n                                <td style=""><input type="datetime-local" id="quickAddDate" step=".001" style="width: auto;"></td>\n                                </td>\n                                <td style="">\n                                    <select id="quickAddCommandTypeSelection" style="width: 70px;">\n                                    <option value="Attack" >Attack</option>\n                                    <option value="Support">Support</option>\n                                    </select>\n                                </td>\n                                <td>\n                                    <select id=\'quickAddSelectTemplate\' class=\'templateDropdown attackTemplate\' style="margin-left: 7px"></select>\n                                </td>\n                                <td style=""><input type="text" id="quickAddToUse"style="width: 50px;text-align: center;vertical-align: middle;" value="100" tabindex="4"></td>\n                                <td style=""><input id="quickAddButton" value="Add" class="btn" style="display: flex;justify-content: center;align-items: center;width:50px" type="submit"></input>\n                                </td>\n                                <td><a id="quickAddEdit" href="javascript:void(0)" style="font-size:20px;display: flex;justify-content: center;align-items: center;">\u2795</a></td>\n                            </tr>\n                        </tbody>\n                        <thead>\n                            <tr>\n                                <th colspan="9" style="text-align: center;">Add Plan</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr class="edit_row">\n                                <td colspan="9">\n                                    <textarea id="addPlanTextArea" style="width:98%" rows="4" placeholder="Put the planned text here..."></textarea>\n                                </td>\n                            </tr>\n                        </tbody>\n                        <td colspan="9">\n                            <button id="addPlanButton" class="btn">Add plan</button>\n                            <span style="padding-left: 10px"><a id="addPlanHelpButton" href="#" class="help-link help_link" data-topic="tribe" data-section="stronghold">Help</a></span>\n\t\t\t\t\t\t\t<div style="padding-left: 10px;display: inline-block;">\n\t\t\t\t\t\t\t\t<a style="color: black">Read MS from plan:</a>\n\t\t\t\t\t\t\t\t<input type="checkbox" id="readMSFromPlan" ' +
        (_0x758ef7.readMSFromPlan !== undefined ? _0x758ef7.readMSFromPlan ? "checked" : "" : "checked") + '>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div style="padding-left: 10px;display: inline-block;">\n\t\t\t\t\t\t\t\t<a style="color: black">Set MS to plan:</a>\n\t\t\t\t\t\t\t\t<input id="setMSWholePlane" type="text" style="width: 50px;margin-left: 7px" value="' + (_0x758ef7.setMSWholePlane !== undefined ? _0x758ef7.setMSWholePlane : 0) + '">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</p>\n                        </td>\n                    </table>\n                </div>\n            </div>\n        </div>\n        <br>\n        <table class="vis" width="100%" style="margin-bottom: 2px">\n            <tbody>\n                <tr>\n                    <td align="center" id="commands_table_pages">\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n        <div style="padding: 0;" class="forum-content">\n            <table style="width: 100%;", class="vis overview_table">\n                <thead id="comandsTableHeader">\n                    <tr>\n                        <th colspan="20">\n                            <span class="column-title" style="font-size:16px">\u2694\ufe0f Planned Commands \u2694\ufe0f</span>\n                        </th>\n                    </tr>\n                </thead>\n                <tbody id="comandsTableRows">\n                </tbody>\n            </table>\n            <br>\n            <table>\n                <tbody>\n                    <tr>\n                        <td><a><input id="send_all" class="btn" name="send" type="submit" value="Send selected"></a>\n                        </td>\n                        <td><a><input id="delete_all" class="btn" name="delete" type="submit" value="Delete selected"></a>\n                        </td>\n                        <td><a><input id="delete_all_old_commands" class="btn" name="delete" type="submit" value="Delete old commands"></a>\n                        </td>\n                        <td><a><input id="delete_all_done_commands" class="btn" name="delete" type="submit" value="Delete done commands"></a>\n                        </td>\n                        <td><a><input id="revert_deleted_commands" class="btn" name="delete" type="submit" value="Revert deleted commands"></a>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n        <br>\n        <div>\n            <span style=\'font-family:Verdana, Arial;float:left;margin: 7px;\'>Problems? Join my <a style="color: -webkit-link" href="https://discord.gg/JpHMjH8QtB">Discord</a> or message me: <a href=\'./game.php?village=212&screen=info_player&id=2871948\'>Im Kumin</a> </span>\n        </div>\n        <div style="float: right">\n            <button id="startImportDataButton" class="btn">Import Data</button>\n            <button id="exportDataButton" class="btn">Export Data</button>\n            <button id="exportDataToFileButton" class="btn">Export Data To File</button>\n            <textarea id="importExportInput" type="text" style="display: none"></textarea>\n            <button id="importDataButton" class="btn" style="display: none">Import</button>\n            <button id="cancelImportDataButton" class="btn" style="display: none">Cancel</button>\n        </div>\n    </div>';
    _0x1be711(infoBoxLatLng, "#memoPlanner", {
        callback : _0x3ab066
    });
}
function _0x2a2191(key) {
    return [].concat(_toConsumableArray(new Set($.map(_0x250930, function(tableContents) {
        return String(tableContents[key]);
    }))));
}
function _0xbd432d() {
    var infoBoxLatLng = '<th id="tableHeader"><input type="checkbox" id="select_all_commands"></th>\n        <th id="thSelectionCheckboxtype"></th>\n        <th id="thSelectionCheckboxname"></th>\n        <th id="thSelectionCheckboxsource"></th>\n        <th id="thSelectionCheckboxtarget"></th>\n        <th><a>Launch Time</a></th>\n        <th id="thSelectionCheckboxslowestUnit"></th>\n        <th><a>Send</a></th>\n        <th id="thSelectionCheckboxdone"></th>\n        <th><a>Edit</a></th>';
    if ($("#tableHeader").length) {
        $("#comandsTableHeader")[0].deleteRow(1);
    }
    _0x1be711(infoBoxLatLng, "#comandsTableHeader", {
        wrapper : function createTable(name, table) {
            return table.insertRow().innerHTML = name;
        },
        callback : function bindFilterEvents() {
            $("#select_all_commands").off("change");
            $("#select_all_commands").on("change", function() {
                $("[id^='select_command_']").prop("checked", this.checked);
            });
        }
    });
    _0xfbae43("Name", _0x2a2191("name"), "name");
    _0xfbae43("Type", _0x2a2191("type"), "type", function(p_Interval) {
        return '<img src="https://dsen.innogamescdn.com/asset/34fb11bc/graphic/command/' + p_Interval.toLowerCase() + '.png">';
    });
    _0xfbae43("Source", _0x2a2191("source"), "source");
    _0xfbae43("Target", _0x2a2191("target"), "target");
    _0xfbae43("Slowest Unit", _0x2a2191("slowestUnit"), "slowestUnit", function(canCreateDiscussions) {
        return '<img src="/graphic/unit/unit_' + canCreateDiscussions + '.png">';
    });
    _0xfbae43("Done", _0x2a2191("done"), "done");
}
function _0x23ae1a(level) {
    $("#comandsTableRows > tr").remove();
    var allKeys = _0x14c4ee(level);
    var crossfilterable_layers = allKeys;
    if (_0x15db45 > 0) {
        crossfilterable_layers = allKeys.slice((_0x15db45 - 1) * _0x13e10d, _0x15db45 * _0x13e10d);
    }
    var layer_i = 0;
    for (; layer_i < crossfilterable_layers.length; layer_i++) {
        var layer = crossfilterable_layers[layer_i];
        var bytesCheck = layer_i % 2 ? "row_a" : "row_b";
        _0x59cd1e(layer, bytesCheck);
    }
}
function _0x59cd1e(data, callback) {
    var _0x5db5ab = "command_" + data.id;
    var reorderQuestions = function scoreNextPageLinkCandidate(link) {
        return window.location.pathname + "?" + (_0xcba1aa ? _0xcba1aa + "&" : "") + "village=" + data.sourceVillageId + "&screen=info_village&id=" + link;
    };
    var infoBoxLatLng = '<tr class=" ' + callback + ' " id="' + _0x5db5ab + '">\n        <td><input type="checkbox" id="select_command_' + _0x5db5ab + '" data-id=' + data.id + "></td>\n        <td>" + ('<img src="/graphic/command/' + data.type.toLowerCase() + '.png">') + "</td>\n        <td> " + data.name + ' </td>\n        <td> <a href="' + reorderQuestions(data.sourceVillageId) + '" target="_blank">' + data.source + ' </a> </td>\n        <td> <a href="' + reorderQuestions(data.targetVillageId) +
        '" target="_blank">' + data.target + " </a> </td>\n        <td> " + _0x168f49(data.launchTime) + ' <b  class="overviewsTimer commandTimer" data-endtime="' + (new Date(data.launchTime)).getTime() + '" data-randomtime="' + (parseInt(data.randomOffset) > 0 ? (new Date(data.randomOffsetTime)).getTime() : 0) + '" data-format="%hh%:%mm%:%ss%"></b> ' + (data.duplicateError ? "\u26a0\ufe0f (Duplicate time)" : "") + '</td>\n        <td><img src="/graphic/unit/unit_' + data.slowestUnit + '.png"></td>\n        <td><a><input id="send_' +
        _0x5db5ab + '" data-id=' + data.id + ' class="btn" name="send" type="submit" value="Send"></a></td>\n        <td> ' + data.done + " </td>\n        <td><a data-id=" + data.id + ' id="edit_' + _0x5db5ab + '" href="javascript:void(0)" style="font-size:20px;display: flex;justify-content: center;align-items: center;">\u2795</a></td>\n        </tr>';
    _0x1be711(infoBoxLatLng, "#comandsTableRows", {
        wrapper : function createTable(table, thead) {
            return thead.insertRow().outerHTML = table;
        },
        callback : _0x40c308
    });
}
function _0x40c308() {
    $("[id^=edit_command_]").off("click");
    $("[id^=send_command_]").off("click");
    $("[id^=edit_command_]").on("click", function() {
        _0xbb8b6f(this.dataset.id);
    });
    $("[id^=send_command_]").on("click", function() {
        _0x39f9b0(this.dataset.id);
    });
}
function _0x3ab066() {
    $("#send_all").off("click");
    $("#delete_all").off("click");
    $("#quickAddEdit").off("click");
    $("#quickAddButton").off("click");
    $("#addPlanButton").off("click");
    $("#saveSettingsButton").off("click");
    $("#saveSettingsButtonBot").off("click");
    $("#startImportDataButton").off("click");
    $("#importDataButton").off("click");
    $("#cancelImportDataButton").off("click");
    $("#exportDataButton").off("click");
    $("#exportDataToFileButton").off("click");
    $("#delete_all_old_commands").off("click");
    $("#delete_all_done_commands").off("click");
    $("#revert_deleted_commands").off("click");
    $("#quickAddCommandTypeSelection").off("change");
    $("#send_all").on("click", _0x3eac40);
    $("#delete_all").on("click", _0x35efd1);
    $("#quickAddEdit").on("click", function() {
        return _0xbb8b6f(-1);
    });
    $("#quickAddButton").on("click", _0xd92dd0);
    $("#addPlanButton").on("click", _0x233332);
    $("#quickAddDate").val(_0x23546f(new Date));
    $("#saveSettingsButton").on("click", _0x19bd2c);
    $("#saveSettingsButtonBot").on("click", _0x19bd2c);
    $("#startImportDataButton").on("click", _0x50d196);
    $("#importDataButton").on("click", _0x1a5e66);
    $("#cancelImportDataButton").on("click", _0x258672);
    $("#exportDataButton").on("click", _0x95231a);
    $("#exportDataToFileButton").on("click", _0x4391c1);
    $("#delete_all_old_commands").on("click", _0x4aa88a);
    $("#delete_all_done_commands").on("click", _0x41408b);
    $("#revert_deleted_commands").on("click", _0xf99292);
    $("#quickAddCommandTypeSelection").on("change", function() {
        if (this.value == "Support") {
            $("#quickAddSelectTemplate").val(_0xe17f21.settings.currentSupportTemplate);
        } else {
            $("#quickAddSelectTemplate").val(_0xe17f21.settings.currentAttackTemplate);
        }
    });
    $("#addPlanHelpButton").on("click", _0xc20a0f);
    $("#botSettingsHeader").on("click", _0x13a044);
    $("#settingsHeader").on("click", _0x3579b7);
    $("#botStatusSettingsHeader").on("click", _0x49ad9f);
    $("#startStopBotButton").on("click", _0x5889ec);
    _0x37e21d("init");
    $("#popupsAllowedTr").on("click", _0x37e21d);
    _0x1b4457();
    _0x23a351();
    _0x9a5185();
    _0x248f7e();
    _0x5473f6("#memoPlanner");
    _0x288fca(".commandTimer", {
        htmlWrapper : function function_print(start) {
            if (start == "OnHold") {
                return '(<span style="color: green;">On Hold</span>)';
            } else {
                if (start != "end") {
                    return '(<span style="color: blue;">' + start + "</span>)";
                } else {
                    return '(<span style="color: red;">Command overdue</span>)';
                }
            }
        },
        refreshTime : 500
    });
    _0x1adb75();
    _0x22eea6();
}
function _0x1b4457() {
    $("#readMSFromPlan").on("change", _0x3d7415);
    $("#setMSWholePlane").on("change", _0x3d7415);
    if (_0x758ef7.readMSFromPlan) {
        $("#setMSWholePlane").attr("disabled", true);
    } else {
        $("#setMSWholePlane").attr("disabled", false);
    }
}
function _0x3d7415() {
    _0x758ef7.readMSFromPlan = $("#readMSFromPlan").is(":checked");
    _0x3fd58a = _0x758ef7.readMSFromPlan;
    if (_0x758ef7.readMSFromPlan) {
        $("#setMSWholePlane").attr("disabled", true);
    } else {
        $("#setMSWholePlane").attr("disabled", false);
    }
    var total_pageviews_raw = document.getElementById("setMSWholePlane").value;
    var total_pageviews = 0;
    if (total_pageviews_raw != "") {
        total_pageviews = parseInt(total_pageviews_raw);
    }
    _0x758ef7.setMSWholePlane = total_pageviews;
    _0x3e22e1();
}
function _0x37e21d(type) {
    if (_0x758ef7.allowPopups) {
        $("#popupsAllowedTr").hide(100);
        return;
    }
    var retryLinkHref = window.open(null, "", "width=1,height=1");
    try {
        retryLinkHref.close();
        if (type == "init") {
            _0x758ef7.allowPopups = true;
            $("#popupsAllowedTr").hide(100);
        }
    } catch (_0x44c2da) {
        _0x758ef7.allowPopups = false;
        $("#popupsAllowedTr").show(100);
    }
    _0x3e22e1();
}
function _0x1adb75() {
    var _0x444090 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
    if (_0x444090) {
        _0xbd432d();
    }
    _0x39ac66();
    _0x5f4715();
}
function _0x5f4715() {
    var subsublevel = _0x12ddc9();
    _0x23ae1a(subsublevel);
    _0x40c308();
    _0x3e22e1();
}
function _0xd92dd0() {
    var plainText = document.getElementById("quickAddName").value;
    var localId1 = document.getElementById("quickAddSource").value;
    var vulnData = document.getElementById("quickAddTarget").value;
    var countWrap = document.getElementById("quickAddToUse").value;
    var previousWidgetPos = void 0;
    var numCarets = void 0;
    if ($("#quickAddTime")[0].value == "arrival") {
        numCarets = new Date(document.getElementById("quickAddDate").value);
    } else {
        previousWidgetPos = new Date(document.getElementById("quickAddDate").value);
    }
    var ansData = $("#quickAddCommandTypeSelection")[0].value;
    _0xbc5ecf(plainText, localId1, vulnData, previousWidgetPos, numCarets, ansData, {
        id : -1,
        useDefaultTemplate : false,
        fromQuickAdd : true,
        toUse : countWrap
    });
    _0x1adb75();
}
function _0x233332() {

    var annotation_date = document.getElementById("addPlanTextArea").value;
    var crossfilterable_layers = annotation_date.split("\n");
    var layer_i = 0;
    for (; layer_i < crossfilterable_layers.length; layer_i++) {
        var xpathExp = crossfilterable_layers[layer_i];
        var conditions = _0x221af4(xpathExp);
        if (conditions == null || conditions.length <= 1) {
            continue;
        }
        var conditionVariable = conditions[0];
        var item = conditions[1];
        var col = _0x6cd487(xpathExp);
        var indContent = _0x186339(xpathExp);
        var SKIP_TESTS = _0x506b21(xpathExp);
        _0xbc5ecf(undefined, conditionVariable, item, col, undefined, SKIP_TESTS, {
            useDefaultTemplate : true,
            slowestUnit : indContent,
            id : -1
        });
    }
    _0x1adb75();
}
function _0xbc5ecf(defaultValue, localTVarId, item, index, count, type, _ref2) {
    var _ref2$target = _ref2.units;
    var target = _ref2$target === undefined ? {} : _ref2$target;
    var key = _ref2.id;
    var k = key === undefined ? -1 : key;
    var _ref2$cancelable = _ref2.useDefaultTemplate;
    var inputWin = _ref2$cancelable === undefined ? false : _ref2$cancelable;
    var route = _ref2.slowestUnit;
    var name = route === undefined ? "" : route;
    var _ref2$Pending = _ref2.fromQuickAdd;
    var winRef = _ref2$Pending === undefined ? false : _ref2$Pending;
    var _ref2$areStatePropsEq = _ref2.toUse;
    var areStatePropsEqual = _ref2$areStatePropsEq === undefined ? _0x758ef7.toUse : _ref2$areStatePropsEq;
    var _ref$selectorFactory = _ref2.autoSend;
    var selectorFactory = _ref$selectorFactory === undefined ? true : _ref$selectorFactory;
    var _ref2$areMergedPropsE = _ref2.randomOffset;
    var areMergedPropsEqual = _ref2$areMergedPropsE === undefined ? _0x758ef7.launchTime_offset : _ref2$areMergedPropsE;
    var dstAddrStr = _ref2.randomOffsetTime;
    var _ref2$method = _ref2.sigil;
    var sigil = _ref2$method === undefined ? _0x758ef7.sigil : _ref2$method;
    var _ref2$pure = _ref2.leaveCats;
    var pure = _ref2$pure === undefined ? _0x758ef7.leaveCats : _ref2$pure;
    var _ref2$areOwnPropsEqua = _ref2.leaveRams;
    var areOwnPropsEqual = _ref2$areOwnPropsEqua === undefined ? _0x758ef7.leaveRams : _ref2$areOwnPropsEqua;
    var _ref2$Failure = _ref2.ntTemplate;
    var Failure = _ref2$Failure === undefined ? _0x13e2cf() : _ref2$Failure;
    var _ref2$Success = _ref2.catapultTarget;
    var Success = _ref2$Success === undefined ? _0x758ef7.currentCatapultTarget : _ref2$Success;

    _0x2024a4();
    var val = void 0;
    if (inputWin || winRef) {
        if (type == "Support") {
            val = _0x17cd08[_0x758ef7.currentSupportTemplate];
        } else {
            val = _0x17cd08[_0x758ef7.currentAttackTemplate];
        }
        if (winRef) {
            val = _0x17cd08[$("#quickAddSelectTemplate")[0].value];
        }
        target = Object.assign({}, val.units);
    }
    var value = defaultValue;
    if (!value) {
        value = val.name;
    }
    if (!_0x1bfb72[localTVarId]) {
        _0x178b57(_0x1aea8d);
        return;
    } else {
        if (!_0x1bfb72[item]) {
            _0x178b57(_0x1db6e9);
            return;
        } else {
            if (value.length > _0x300496) {
                _0x178b57(_0x1c8287);
                return;
            }
        }
    }
    $.each(target, function(name, current_version) {
        return target[name] = parseInt(current_version != "" ? current_version : 0);
    });
    if (_0x171bc0[name]) {
        if (target[name] == 0) {
            target[name] = 1;
        }
    }
    var rankings = [];
    $.each(_0x171bc0, function(awsKey, clip) {
        var _0x9cf7ff = Math.ceil(clip.speed);
        rankings.push({
            key : awsKey,
            thisSpeed : _0x9cf7ff
        });
    });
    var keys = rankings.sort(function whereOverflowLess(elem1Rect, elem2Rect) {
        if (elem1Rect.thisSpeed < elem2Rect.thisSpeed) {
            return 1;
        } else {
            if (elem1Rect.thisSpeed > elem2Rect.thisSpeed) {
                return -1;
            } else {
                return 0;
            }
        }
    });
    var i;
    for (i in keys) {
        if (target[keys[i].key] != 0) {
            if (_0x171bc0[name]) {
                if (keys[i].speed > _0x171bc0[name].speed) {
                    target[keys[i]] = 0;
                }
            } else {
                name = keys[i].key;
                break;
            }
        }
    }
    if (type == "Support" && target.knight != 0 && _0x3f6a42.units.includes("knight")) {
        name = "knight";
    }
    if (k < 0) {
        UI.SuccessMessage("New Command programmed");
    }
    k = k < 0 ? _0xe17f21.commandIdCounter++ : k;
    var data = {
        id : k,
        name : value,
        sourceVillageId : _0x1bfb72[localTVarId].id,
        targetVillageId : _0x1bfb72[item].id,
        source : localTVarId,
        target : item,
        launchTime : index,
        arrivalTime : count,
        slowestUnit : name,
        units : target,
        type : type,
        done : false,
        toUse : areStatePropsEqual,
        autoSend : selectorFactory,
        preparedByBot : false,
        randomOffset : areMergedPropsEqual,
        randomOffsetTime : dstAddrStr,
        sigil : sigil,
        leaveCats : pure,
        leaveRams : areOwnPropsEqual,
        ntTemplate : Failure,
        catapultTarget : Success,
        duplicateError : false
    };
    _0x50f8b5(data);
    _0x250930[k] = data;
    _0x3e22e1();
}
function _0x39f9b0() {

    var i = 0;
    for (; i < arguments.length; i++) {
        setTimeout(function(inTargetId, event, eventsDict) {
            var serializedAnnotations = _0x250930[eventsDict[event]];
            var url = window.location.pathname + "?" + (inTargetId ? inTargetId + "&" : "") + "village=" + serializedAnnotations.sourceVillageId + "&screen=place&target=" + serializedAnnotations.targetVillageId;
            var root = window.open(url);
            root.sessionStorage.setItem("commandsOverviewCommandData", JSON.stringify(serializedAnnotations), "_blank", "toolbar=0,location=0,menubar=0");
        }, _0x2fa1d5 * (i + Math.random() * 0.1), _0xcba1aa, i, arguments);
    }
    _0x1adb75();
}
function _0x1a30ea(a, b) {
    var fixture = document.createElement("iframe");
    return fixture.setAttribute("id", a), fixture.setAttribute("src", b), fixture.style.width = window.innerWidth / 4 - 10 + "px", fixture.style.height = window.innerHeight - 90 + "px", document.body.appendChild(fixture), fixture;
}
function _0x39ac66() {
    if (_0x4edad3) {
        _0x4aa88a("auto");
    }
    if (_0x298966) {
        _0x41408b("auto");
    }
}
function _0x4aa88a(zIndex) {
    console.log("Deleting old commands");
    var _0x49bda1 = 0;
    var rowArr = [];
    $.each(_0x250930, function(i, canCreateDiscussions) {
        if (canCreateDiscussions) {
            var preset = new Date(Timing.getCurrentServerTime() - _0x23da2a - _0x200465 * 60000);
            if (_0x3e08ce(_0x250930[i], preset) < 0) {
                if (zIndex != "auto") {
                    rowArr.push(_0x250930[i]);
                }
                _0x49bda1++;
                delete _0x250930[i];
            }
        }
    });
    if (rowArr.length > 0) {
        _0xdc9e17.push(rowArr);
    }
    console.log("Deleted " + _0x49bda1 + " commands");
    _0x5f4715();
}
function _0x41408b(zIndex) {
    var _0x376ee5 = 0;
    var rowArr = [];
    $.each(_0x250930, function(i, sectionInfo) {
        if (sectionInfo) {
            if (sectionInfo.done) {
                if (zIndex != "auto") {
                    rowArr.push(_0x250930[i]);
                }
                _0x376ee5++;
                delete _0x250930[i];
            }
        }
    });
    if (rowArr.length > 0) {
        _0xdc9e17.push(rowArr);
    }
    console.log("Deleted " + _0x376ee5 + " done commands");
    _0x5f4715();
}
function _0x3eac40() {
    _0x39f9b0.apply(undefined, _toConsumableArray($.map($("[id^='select_command_']").filter(":checked"), function(slotElement) {
        return slotElement.dataset.id;
    })));
}
function _0x35efd1() {
    _0x4200d4.apply(undefined, _toConsumableArray($.map($("[id^='select_command_']").filter(":checked"), function(slotElement) {
        return slotElement.dataset.id;
    })));
}
function _0x4200d4() {
    var templateGroups = [];
    var i = 0;
    for (; i < arguments.length; i++) {
        templateGroups.push(_0x250930[arguments[i]]);
        delete _0x250930[arguments[i]];
    }
    if (templateGroups.length > 0) {
        _0xdc9e17.push(templateGroups);
    }
    console.log("Deleted " + arguments.length + " commands");
    _0x1adb75();
}
function _0xf99292() {
    if (_0xdc9e17.length == 0) {
        _0x178b57(_0x3e124a);
        return;
    }
    var crossfilterable_layers = _0xdc9e17.pop();
    var layer_i = 0;
    for (; layer_i < crossfilterable_layers.length; layer_i++) {
        var layer = crossfilterable_layers[layer_i];
        _0x250930[layer.id] = layer;
    }
    console.log("Reverted " + crossfilterable_layers.length + " commands");
    _0x1adb75();
}
function _0x14c4ee(key) {
    var encryptedRandomIv = Object.values(key);
    var _0x1b6672 = encryptedRandomIv.length === Object.keys(_0x250930).length;
    if (encryptedRandomIv.length < 1) {
        return [];
    }
    var renderingPath = [];
    $.each(key, function(canCreateDiscussions, view) {
        renderingPath.push(view);
    });
    var artistTrack = renderingPath.sort(_0x3e08ce);
    if (_0x1b6672) {
        _0x16bf05 = artistTrack;
        _0x289677(encryptedRandomIv, artistTrack);
    }
    return artistTrack;
}
function _0x289677() {
    var i;
    for (i in _0x16bf05) {
        var unloadedImgElement = _0x16bf05[i];
        var captureOpts = null;
        if (i > 0) {
            captureOpts = _0x16bf05[i - 1];
        }
        if (captureOpts != null && true && _0x23f2b7(unloadedImgElement, captureOpts)) {
            unloadedImgElement.duplicateError = true;
            captureOpts.duplicateError = true;
        } else {
            unloadedImgElement.duplicateError = false;
        }
    }
}
function _0x23f2b7(app, opts) {
    var auth0_time = (new Date(app.launchTime)).getTime();
    var local_time = (new Date(opts.launchTime)).getTime();
    return Math.abs(auth0_time - local_time) < _0x1c1e99 * 1000;
}
function _0x24dc8a() {
    var result = null;
    var i = 0;
    for (; i < _0x16bf05.length && result == null; i++) {
        var message = _0x16bf05[i];
        if (_0x3e08ce(message, new Date(new Date - _0x23da2a)) > 0) {
            result = message;
        }
    }
    return result;
}
function _0x56e4f3(canCreateDiscussions) {
    var signatureNames = [];
    var sigNum = 0;
    var _i = 0;
    for (; _i < _0x16bf05.length; _i++) {
        var i = _0x16bf05[_i];
        var expected_date2 = new Date(Timing.getCurrentServerTime() + canCreateDiscussions * 1000 - _0x23da2a);
        if (!i.done && _0x320313(i, expected_date2) < 0 && _0x320313(i, new Date(new Date - _0x23da2a)) > 0) {
            signatureNames[sigNum] = i;
            sigNum++;
        }
    }
    return signatureNames;
}
function _0xfbae43(text, val, type) {
    var getFileJSON = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function(fileName) {
        return fileName;
    };
    var value = '<form>\n        <div class="multipleSelection">\n            <div class="selectBox" data-id="' + type + '" id="selectBox_' + type + '">\n                <select>\n                    <option selected hidden>' + text + '</option>\n                </select>\n                <div class="overSelect"></div>\n            </div>\n\n            <div id="checkBoxes_' + type + '" class=\'selectionOption\' style="display:none;">\n                <table>\n                    <tbody>\n                        <tr>\n                            <td><input type="checkbox" class="selectionCheckboxAll" id="select_' +
        type + '_all" data-id="' + type + '" checked></td><td>All</td>\n                        </tr>\n                        ' + $.map(val, function(fileData, _transactionName) {
            return '\n                        <tr>\n                            <td><input type="checkbox" id="select_' + type + "_" + _transactionName + '" class="selectionCheckbox" checked data-option="' + fileData + '" data-id="' + type + '"></td><td>' + getFileJSON(fileData) + "</td>\n                        </tr>";
        }).join("\n") + "\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </form>\n    ";
    return _0x1be711(value, "#thSelectionCheckbox" + type, {
        callback : _0x339968
    }), value;
}
function _0x339968() {
    if (!$("#checkboxFader").length) {
        $("#memoPlanner").append('<div id="checkboxFader" style="display:none;"></div> ');
    }
    var reconnectingCallback = function _fixCKEDITORBug() {
        var boxChild = $("#checkBoxes_" + this.dataset.id)[0];
        var isSelected = boxChild.style.display == "none" ? "block" : "none";
        $("#checkboxFader")[0].style.display = isSelected;
        boxChild.style.display = isSelected;
    };
    $(".selectBox").off("click");
    $("#checkboxFader").off("click");
    $(".selectionCheckboxAll").off("change");
    $(".selectionCheckbox").off("change");
    $(".selectBox").on("click", reconnectingCallback);
    $("#checkboxFader").on("click", function() {
        $(".selectionOption").css("display", "none");
        $("#checkboxFader").css("display", "none");
    });
    $(".selectionCheckboxAll").on("change", function() {
        var nodeID = this.dataset.id;
        $("[id^=select_" + nodeID + "_]").prop("checked", this.checked);
        _0x5f4715();
    });
    $(".selectionCheckbox").on("change", _0x5f4715);
}
function _0x12ddc9() {
    var assetsDirsGroups = [].concat(_toConsumableArray($(".selectionCheckbox:checkbox:checked").map(function(canCreateDiscussions, event) {
        return [[event.dataset.id, event.dataset.option]];
    })));
    var resource = assetsDirsGroups.reduce(function(obj, _qualifiedName$split6) {
        var _qualifiedName$split62 = _slicedToArray(_qualifiedName$split6, 2);
        var commandName = _qualifiedName$split62[0];
        var domainName = _qualifiedName$split62[1];
        if (!Object.keys(obj).includes(commandName)) {
            obj[commandName] = [];
        }
        return obj[commandName].push(domainName), obj;
    }, {});
    $(".selectBox").each(function(canCreateDiscussions, slotElement) {
        resource[slotElement.dataset.id] = resource[slotElement.dataset.id] ? resource[slotElement.dataset.id] : [];
    });
    var elements = [];
    return $.each(_0x250930, function(index, item) {
        var orTmp = resource.name.includes(item.name);
        var andTmp = resource.type.includes(item.type);
        var tmp = resource.source.includes(item.source);
        var hasNamedAttributes = resource.target.includes(item.target);
        var fetchEntityErr = resource.slowestUnit.includes(item.slowestUnit);
        var deactivatedEntityErr = resource.done.includes(item.done.toString());
        if (orTmp && andTmp && tmp && hasNamedAttributes && fetchEntityErr && deactivatedEntityErr) {
            elements.push(_defineProperty({}, index, item));
        }
    }), _0x2a72a3(elements.length), _0x3edfc4(elements);
}
function _0x12a8ea() {
    if (_0xe17f21.commandIdCounter == null) {
        _0xe17f21.commandIdCounter = 0;
    }
    if (!_0xe17f21.commands) {
        _0xe17f21.commands = {};
    }
    _0x409669(_0x250930, _0xe17f21.commands);
}
function _0x25a6b3() {
    function expect(name) {
        return '<tr><td class="nowrap"><a href="javascript:void(0)" class="unit_link" data-unit="' + name + '"><img src="/graphic/unit/unit_' + name + '.png"></a> <input id="unit_input_' + name + '" name="' + name + '" type="text" tabindex="' + _0x145407++ + '" value="' + _0x74ec12(name, -1).value + '" class="unitsInput" Disabled></td><td align="center"><input type="checkbox" tabindex="' + _0x145407++ + '" id="' + name + '_all" class="popupCheckbox" checked = "' + _0x74ec12(name, -1).checked + '" data-unit="' +
            name + '"> </td></tr>';
    }
    function generateConfigDropdown(result) {
        var _0x22ee36 = '<tr class="' + (_0x3f89cf++ % 2 ? "row_a" : "row_b") + '">\n            <td><a class="templateLink" data-templateid="' + result.id + '" href="javascript:void(0);">' + result.name + "</a></td>\n        </tr>";
        return _0x22ee36;
    }
    var _0x145407 = 0;
    var _0x3f89cf = 0;
    var photoText = '<div class="popup_box_container" id="config_popup" style="display:none;">\n        <div class="popup_box show" id="popup_box_popup_command" style="min-width: 700px;">\n            <div class="popup_box_content">\n                <a class="popup_box_close tooltip-delayed" id="popup_cross" href="javascript:void(0)"> </a>\n                <h3>Set attack units</h3>\n                <div style="display: grid;grid-template-columns: auto auto;grid-gap: 10px;">\n                    <div style="min-width: 600px;">\n                        <table>\n                            <tbody>\n                                <tr>\n                                    <td valign="top">\n                                        <table class="vis" width="100%">\n                                            <tbody>\n                                                <tr>\n                                                    <th>Infantry</th><th>Send All</th>\n                                                </tr>\n                                                ' +
        expect("spear") + "\n                                                " + expect("sword") + "\n                                                " + expect("axe") + "\n                                                " + (Object.keys(_0x171bc0).includes("archer") ? expect("archer") : "") + '\n                                            </tbody>\n                                        </table>\n                                    </td>\n                                    <td valign="top">\n                                        <table class="vis" width="100%">\n                                            <tbody>\n                                                <tr>\n                                                    <th>Cavalry</th><th>Send All</th>\n                                                </tr>\n                                                ' +
        expect("spy") + "\n                                                " + expect("light") + "\n                                                " + (Object.keys(_0x171bc0).includes("marcher") ? expect("marcher") : "") + "\n                                                " + expect("heavy") + '\n                                            </tbody>\n                                        </table>\n                                    </td>\n                                    <td valign="top">\n                                        <table class="vis" width="100%">\n                                            <tbody>\n                                                <tr>\n                                                    <th>Siege Weapons</th><th>Send All</th>\n                                                </tr>\n                                                ' +
        expect("ram") + "\n                                                " + expect("catapult") + '\n                                            </tbody>\n                                        </table>\n                                    </td>\n                                    <td valign="top">\n                                        <table class="vis" width="100%">\n                                            <tbody>\n                                                <tr>\n                                                    <th>Others</th><th>Send All</th>\n                                                </tr>\n                                                ' +
        (Object.keys(_0x171bc0).includes("knight") ? expect("knight", 1) : "") + "\n                                                " + expect("snob", 2) + '\n                                            </tbody>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    <div class="vis" style="margin: 4px 10px 0px 10px; min-width: 125px; float:right; height:max-content">\n                        <h4>\n                            <a href="' +
        window.location.origin + '/game.php?screen=place&mode=templates">Troop templates</a>\n                        </h4>\n                        <table class="vis" style="width: 100%">\n                            <tbody>\n                               ' + $.map(_0x17cd08, function(nOsd) {
            return generateConfigDropdown(nOsd);
        }).join("\n") + '\n                            </tbody>\n                        </table>\n                    </div>\n                </div>\n                <br>\n                <table>\n                    <thead>\n                        <tr>\n                            <th style="text-align: center;">Name</th>\n                            <th style="text-align: center;">Source</th>\n                            <th style="text-align: center;">Target</th>\n                            <th style="text-align: center;">\n                                <select id="popupTime">\n                                        <option value="arrival"><strong>Arrival time</strong></option>\n                                        <option value="launch"><strong>Launch time</strong></option>\n                                </select>\n                            </th>\n                            <th style="text-align: center;">Type</th>\n                            <th style="text-align: center;">Max(%)</th>\n                            <th style="text-align: center;">Sigil(%)</th>\n                            <th style="text-align: center;">Leave Cats</th>\n                            <th style="text-align: center;">Leave Rams</th>\n                            <th style="text-align: center;">Random Launch Offset</th>\n                            <th style="text-align: center;">Catapult Target</th>\n                            <th style="text-align: center;">NT Template</th>\n                            <th style="text-align: center;">Auto Send?</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr>\n                            <td style=""><input type="text" id="popupName"style="width:100px" placeholder="Name"></td>\n                            <td style=""><input type="text" id="popupSource"style="width:45px" placeholder="555|555"></td>\n                            <td style=""><input type="text" id="popupTarget" style="width:45px" placeholder="555|555"></td>\n                            <td style=""><input type="datetime-local" id="popupDate" step=".001"></td>\n                            <td style="">\n                                <select id="popupCommandTypeSelection" style="width: 70px;">\n                                    <option value="Attack" >Attack</option>\n                                    <option value="Support">Support</option>\n                                </select>\n                            </td>\n                            <td style=""><input type="text" id="popupToUse" style="width:50px" value="100">\n                            <td style=""><input type="text" id="popupSigil" style="width:50px" value="100">\n                            <td style=""><input type="text" id="popupLeaveCats" style="width:50px" value="100">\n                            <td style=""><input type="text" id="popupLeaveRams" style="width:50px" value="100">\n                            <td style=""><input type="text" id="popupRandomLaunchTimeOffset" style="width:50px" value="100">\n                            <td style="">\n                                <select id="popUpCatapultTarget" style="width: 70px;"></select>\n                            </td>\n                            <td style="">\n                                <select id="popupNtTemplate" style="width: 70px;"></select>\n                            </td>\n                            <td style="text-align: center;vertical-align: middle;"><input type="checkbox" id="popupAutoSend">\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n                <table>\n                    <tbody>\n                        <tr>\n                            <td id="popupSetDiv">\n                                <input id="popupSet" tabindex="15" class="btn" name="set_troops" type="submit" value="Save">\n                            </td>\n                            <td>\n                                <input id="popupSetNew" tabindex="15" class="btn" name="set_troops" type="submit" value="Create New">\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n    <div class="fader" id="popup_fader" style="display:none;"></div>';
    $("#memoPlanner").append(photoText);
    var $dds = $(".popupCheckbox");
    $dds.off("change");
    $dds.on("change", function() {
        var item = $(this)[0];
        var unit = item.dataset.unit;
        $("#unit_input_" + unit)[0].disabled = item.checked;
    });
    var emoteIcon = $(".templateLink");
    emoteIcon.off("click");
    emoteIcon.on("click", function() {
        _0x1bb34d(_0x17cd08[this.dataset.templateid]);
    });
    $("#popup_fader").off("click");
    $("#popup_cross").off("click");
    $("#popup_fader").on("click", function() {
        _0xd80caa(false);
    });
    $("#popup_cross").on("click", function() {
        _0xd80caa(false);
    });
    $.each(_0x412527, function(inputMethodId) {
        $("#popupNtTemplate").append($("<option>", {
            id : inputMethodId,
            text : _0x412527[inputMethodId].name
        }));
    });
    $.each(_0x3f6a42.village.buildings, function(prefix) {
        $("#popUpCatapultTarget").append($("<option>", {
            id : prefix + "PopUp",
            text : prefix
        }));
    });
}
function _0x1bb34d(selfInfo) {
    _0x3628ee(selfInfo.units);
}
function _0x74ec12(width, view) {
    var current = view;
    var isDeleteDisabled = current != -1 ? false : true;
    var nothingFoundWindowValue = current != -1 ? false : true;
    var val = current > 0 ? current : "";
    return {
        disabled : isDeleteDisabled,
        checked : nothingFoundWindowValue,
        value : val
    };
}
function _0x3628ee(name) {
    $.each(name, function(id, select) {
        if (Object.keys(_0x171bc0).includes(id)) {
            $("#unit_input_" + id)[0].value = _0x74ec12(id, select).value;
            $("#unit_input_" + id)[0].disabled = _0x74ec12(id, select).disabled;
            $("#" + id + "_all")[0].checked = _0x74ec12(id, select).checked;
        }
    });
}
function _0x5993c2(i) {
    var u = {};
    if (!_0x250930[i]) {
        $.each(_0x171bc0, function(upgradeId) {
            return u[upgradeId] = -1;
        });
    } else {
        u = _0x250930[i].units;
    }
    _0x3628ee(u);
    if (i != -1) {
        $("#popupName")[0].value = _0x250930[i].name;
        $("#popupSource")[0].value = _0x250930[i].source;
        $("#popupTarget")[0].value = _0x250930[i].target;
        $("#popupDate").val(_0x23546f(new Date(_0x250930[i].arrivalTime)));
        $("#popupCommandTypeSelection").val(_0x250930[i].type);
        $("#popupToUse").val(_0x250930[i].toUse ? _0x250930[i].toUse : 100);
        $("#popupAutoSend").prop("checked", _0x250930[i].autoSend);
        $("#popupRandomLaunchTimeOffset").val(_0x250930[i].randomOffset ? _0x250930[i].randomOffset : 0);
        $("#popupSigil").val(_0x250930[i].sigil ? _0x250930[i].sigil : 0);
        $("#popupLeaveCats").val(_0x250930[i].leaveCats ? _0x250930[i].leaveCats : 0);
        $("#popupLeaveRams").val(_0x250930[i].leaveRams ? _0x250930[i].leaveRams : 0);
        $("#" + _0x250930[i].ntTemplate).prop("selected", true);
        $("#" + _0x250930[i].catapultTarget + "PopUp").prop("selected", true);
    } else {
        $("#popupName")[0].value = $("#quickAddName")[0].value;
        $("#popupSource")[0].value = $("#quickAddSource")[0].value;
        $("#popupTarget")[0].value = $("#quickAddTarget")[0].value;
        $("#popupDate").val(_0x23546f(new Date($("#quickAddDate")[0].value)));
        $("#popupCommandTypeSelection")[0].value = $("#quickAddCommandTypeSelection")[0].value;
        $("#popupToUse").val(100);
        $("#popupSigil").val(0);
        $("#popupLeaveCats").val(0);
        $("#popupLeaveRams").val(0);
        $("#popupAutoSend").prop("checked", true);
        $("#popupRandomLaunchTimeOffset").val(0);
        $("#popupNtTemplate").val(_0x412527[_0x13e2cf()].id);
        $("#" + _0x13e2cf()).prop("selected", true);
        $("#popUpCatapultTarget").val(_0x758ef7.currentCatapultTarget);
        $("#" + _0x758ef7.currentCatapultTarget + "PopUp").prop("selected", true);
    }
}
function _0xbb8b6f() {
    var intTempPreference = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : -1;
    intTempPreference = parseInt(intTempPreference);
    _0x5993c2(intTempPreference);
    $("#config_popup")[0].style.display = "flex";
    $("#popup_fader")[0].style.display = "flex";
    if (intTempPreference == -1) {
        $("#popupSetDiv")[0].style.display = "none";
    } else {
        $("#popupSetDiv")[0].style.display = "flex";
    }
    $("#popupSet").off("click");
    $("#popupSet").on("click", function() {
        _0xd80caa(true, intTempPreference);
    });
    $("#popupSetNew").off("click");
    $("#popupSetNew").on("click", function() {
        _0xd80caa(true, -1, false);
    });
}
function _0xd80caa(isIron) {
    var CAPTURE_ID = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : -1;
    var _0x1ba74b = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    if (isIron) {
        var DEFAULT_MAX_RETRY_TIME_MS = $("#popupName")[0].value;
        var localId1 = $("#popupSource")[0].value;
        var vulnData = $("#popupTarget")[0].value;
        var previousWidgetPos = void 0;
        var consumedCount = void 0;
        if ($("#popupTime")[0].value == "launch") {
            previousWidgetPos = new Date($("#popupDate")[0].value);
        } else {
            consumedCount = new Date($("#popupDate")[0].value);
        }
        var ansData = $("#popupCommandTypeSelection")[0].value;
        var units = _0x3edfc4($.map(_0x171bc0, function(canCreateDiscussions, _header) {
            var auto = $("#unit_input_" + _header)[0].value;
            var enabled = $("#" + _header + "_all")[0].checked;
            return _defineProperty({}, _header, enabled ? -1 : auto);
        }));
        var countWrap = $("#popupToUse")[0].value;
        var _0x48eeff = $("#popupAutoSend").is(":checked");
        var code_content = $("#popupRandomLaunchTimeOffset")[0].value;
        var sigil = $("#popupSigil")[0].value;
        var helperTagCallback = $("#popupLeaveCats")[0].value;
        var backendCommandsURL = $("#popupLeaveRams")[0].value;
        var autoReview = "firstNobleRedNT";
        var btnYes = $("#popUpCatapultTarget").find(":selected").attr("text");
        var data = $("#popupNtTemplate").find(":selected").attr("id");
        if (data) {
            autoReview = data;
        }
        _0xbc5ecf(DEFAULT_MAX_RETRY_TIME_MS, localId1, vulnData, previousWidgetPos, consumedCount, ansData, {
            units : units,
            id : CAPTURE_ID,
            useDefaultTemplate : false,
            toUse : countWrap,
            autoSend : _0x48eeff,
            sigil : sigil,
            randomOffset : code_content,
            leaveCats : helperTagCallback,
            leaveRams : backendCommandsURL,
            ntTemplate : autoReview,
            catapultTarget : btnYes
        });
        _0x1adb75(false);
    }
    if (_0x1ba74b) {
        $("#config_popup")[0].style.display = "none";
        $("#popup_fader")[0].style.display = "none";
    }
}
function _0x50bf67() {
    return new Promise(function(require, saveNotifs) {
        $.get("/game.php?" + (_0xcba1aa ? _0xcba1aa + "&" : "") + "village=" + _0x3f6a42.village.id + "&screen=place&mode=templates", function(svgString) {
            var parser = new DOMParser;
            var doc = parser.parseFromString(svgString, "text/html");
            var xfavorite = doc.getElementById("troop_template").childNodes[5];
            var searcher_name = xfavorite.innerHTML.split("\n")[2];
            var m = JSON.parse(searcher_name.match(/.+=(.+);/)[1]);
            console.log(m)
            require(_0x3dd1be(m));
        }).fail(function() {
            return saveNotifs(_0x178b57("Error getting troop templates"));
        });
    });
}
var _0x4ca614
var _0x127e10
function _0x3dd1be(filters) {
    console.log(filters)
    var clojIsReversed = {
        _0x127e10 : {
            id : filters["id"],
            name : filters["name"],
            units : "axe"
        }
    };
    return _0x56e922(clojIsReversed), $.each(filters, function($7) {
        var commands = {
            _0x4ca614 : parseInt(filters[$7][_0x4ca614]) < 0 ? -1 : parseInt(filters[$7][_0x4ca614])
        };
        $.each(_0x171bc0, function(canCreateDiscussions) {
        });
        var parameters = filters[$7].use_all;
        $.each(parameters, function(index) {
            commands[parameters[index]] = -1;
        });
    }), clojIsReversed;
}
function _0x56e922(isSlidingUp) {
    isSlidingUp[_0x5343f6.id] = _0x5343f6;
    isSlidingUp[_0x13de06.id] = _0x13de06;
    isSlidingUp[_0x4fc328.id] = _0x4fc328;
}
function _0x9a5185() {
    var fields_to_add = _0x17cd08;
    $.each(fields_to_add, function(i) {
        $(".templateDropdown").append($("<option>", {
            value : fields_to_add[i].id,
            text : fields_to_add[i].name
        }));
    });
    $(".attackTemplate").val(_0x758ef7.currentAttackTemplate);
    $(".supportTemplate").val(_0x758ef7.currentSupportTemplate);
}
function _0x248f7e() {
    var data = _0x3f6a42.village.buildings;
    $.each(data, function(textToFormat) {
        $("#catapultTarget").append($("<option>", {
            id : textToFormat,
            text : textToFormat
        }));
    });
    $("#catapultTarget").val(_0x758ef7.currentCatapultTarget);
}
function _0x223cc2() {
    _0x319ca5();
    _0x4e6491();
}
function _0x319ca5() {
    $(".memo-tab").not("#tab_Planner").click(_0x3eaaa5);
    Memo["_addTab"] = Memo.addTab;
    Memo.addTab = async function() {
        Memo["_addTab"]();
        _0x3eaaa5();
    };
}
function _0x12aab7() {
    if (_0xe17f21.focused == null) {
        _0xe17f21.focused = false;
    }
}
function _0x4e6491() {
    var vFuncs = document.getElementById("tab-bar");
    var logentry_div = document.createElement("div");
    logentry_div.setAttribute("id", "tab_Planner");
    logentry_div.setAttribute("class", "memo-tab");
    vFuncs.parentNode.insertBefore(logentry_div, vFuncs.nextSibling);
    _0x389b85();
}
function _0x527fce() {
    if (_0xe17f21.focused) {
        return;
    }
    $(".memo-tab").not("#tab_Planner").off("click");
    $(".memo-tab").not("#tab_Planner").click(_0x3eaaa5);
    $(".memo_container").hide();
    _0xe17f21.focused = true;
    $("#memoPlanner")[0].style.display = "block";
    $("#tab_Planner")[0].innerHTML = '\n    <span class="memo-tab-label">\n        <strong>' + _0x17de25 + "</strong>\n    </span>";
    $("#tab-bar > div").removeClass("memo-tab-selected");
    var objectLabel = $("#tab_" + Memo.selectedTab)[0].innerText;
    $("#tab_" + Memo.selectedTab + " > .memo-tab-label")[0].innerHTML = '<a href="#" onclick="Memo.selectTab(' + Memo.selectedTab + '); return false">' + objectLabel + "</a>";
    $("#tab_Planner")[0].class = "memo-tab memo-tab-selected";
}
function _0x3eaaa5() {
    if (!_0xe17f21.focused) {
        return;
    }
    _0xe17f21.focused = false;
    $("#memoPlanner")[0].style.display = "none";
    $("#memo_" + Memo.selectedTab)[0].style.display = "block";
    $("#tab_Planner")[0].class = "memo-tab";
    _0x389b85();
}
function _0x389b85() {
    $("#tab_Planner")[0].innerHTML = '<span class="memo-tab-label">\n        <a id="plannerButton" href="javascript:void(0)">' + _0x17de25 + "</a>\n        </span>";
    $("#plannerButton").off("click");
    $("#plannerButton").on("click", _0x527fce);
}

// setTimeout(function() { _0x28452e()   }, (Math.random() * 1000) + 5000);

// function _0x28452e() {
//   return new Promise(function (_0x49832c, _0x2b6fac) {
//     $.get('/interface.php?func=get_config')
//       .done(function (_0x37e63d) {
//         console.log(_0x37e63d)
//         _0x49832c(_0x4883bd(_0x37e63d).config)
//       })
//   })
// }


// function _0x324c7f() {
//   return new Promise(function (_0x49832c, _0x2b6fac) {
//     $.get('/interface.php?func=get_config')
//       .done(function (_0x37e63d) {
//         console.log(_0x37e63d)
//         _0x49832c(_0x4883bd(_0x37e63d).config)
//       })
//   })
// }


function _0x324c7f() {
    return new Promise(function(cont, saveNotifs) {
        $.get("/interface.php?func=get_config").done(function(provider) {
            cont(_0x425843(provider));
        }).fail(function() {
            return saveNotifs(_0x178b57("Error world settings"));
        });
    });
}

function _0x425843(options) {
    console.log(options)
    try {
        var fields = _defineProperty({
        }, "_0xb5715d", []);
        if (options.children.length > 0) {
            var value = 0;
            for (; value < options.children.length; value++) {
                var that = options.children.item(value);
                var i = that.nodeName;
                if (typeof fields[i] == "undefined") {
                } else {
                    if (typeof fields[i].push == "undefined") {
                        var currField = fields[i];
                        fields[i].push(currField);
                    }
                    fields[i].push(_0x425843(that));
                }
            }
        } else {
            fields = options.textContent;
        }
        return fields;
    } catch (userCodeResponse) {
        console.log(userCodeResponse.message);
    }
}


function _0x458034(arr) {
    var resols = [];
    return resols.push(parseInt(arr.slice(0, -2))), resols.push(parseInt(arr.slice(-3))), resols;
}
function _0x6cd487(strRect) {
    var _0x21fcb5 = strRect.match(/(\d{1,2}([:.])\d{1,2}([:.])\d{1,2}(([.])\d{3})?)/g);
    var charcompOutput = strRect.match(/(\d{1,4}([\-\/])\d{1,2}([\-\/])\d{1,4})/g);
    var d = new Date(_0x21fcb5[0] + " " + charcompOutput[0].replace(/(\d{1,2})([\/\-])(\d{1,2})([\/\-])(\d{4})/g, "$3$2$1$4$5"));
    if (!_0x3fd58a) {
        if (d.getMilliseconds() >= 500) {
            d.setSeconds(d.getSeconds() + 1);
        }
        d.setMilliseconds(_0x758ef7.setMSWholePlane);
    }
    return d;
}
function _0x221af4(xpathExp) {
    var _0x3e77fd = xpathExp.match(/\d+\|\d+/gi);
    return _0x3e77fd ? _0x3e77fd : null;
}
function _0x186339(keyCombo) {
    var loadFile = keyCombo.match(/(?<=\[unit\])(.*?)(?=\[\/unit\])/gi);
    if (!loadFile) {
        $.each(_0x1bee0e, function(file) {
            if (keyCombo.toLowerCase().includes(file)) {
                loadFile = file;
            } else {
                if (file == "snob" && (keyCombo.toLowerCase().includes("noble") || keyCombo.toLowerCase().includes("nobleman"))) {
                    loadFile = file;
                } else {
                    if (file == "catapult" && keyCombo.toLowerCase().includes("cat")) {
                        loadFile = file;
                    }
                }
            }
        });
    } else {
        loadFile = loadFile[0];
    }
    return loadFile;
}
function _0x506b21(xpathExp) {
    if (xpathExp.toLowerCase().includes("support")) {
        return "Support";
    } else {
        return "Attack";
    }
}
function _0x4bd042(permissions, individual) {
    var groupPermissionsRef = _0x458034(permissions);
    var indContent = _0x458034(individual);
    var lightI = Math.abs(indContent[0] - groupPermissionsRef[0]);
    var lightJ = Math.abs(indContent[1] - groupPermissionsRef[1]);
    return Math.sqrt(lightI * lightI + lightJ * lightJ);
}
function _0x3e08ce(data, instance) {
    var current = data;
    if (!(data instanceof Date)) {
        current = new Date(data.launchTime);
    }
    var previous = instance;
    if (!(instance instanceof Date)) {
        previous = new Date(instance.launchTime);
    }
    if (current.valueOf() > previous.valueOf()) {
        return 1;
    } else {
        if (current.valueOf() < previous.valueOf()) {
            return -1;
        } else {
            return 0;
        }
    }
}
function _0x320313(instance, data) {
    var previous = instance;
    if (!(instance instanceof Date)) {
        previous = new Date(instance.launchTime);
        if (instance.randomOffset > 0) {
            previous = new Date(instance.randomOffsetTime);
        }
        if (!previous) {
            previous = new Date(instance.launchTime);
        }
    }
    var current = data;
    if (!(data instanceof Date)) {
        current = new Date(data.launchTime);
        if (data.randomOffset > 0) {
            current = new Date(data.randomOffsetTime);
        }
        if (!current) {
            previous = new Date(instance.launchTime);
        }
    }
    if (previous.valueOf() > current.valueOf()) {
        return 1;
    } else {
        if (previous.valueOf() < current.valueOf()) {
            return -1;
        } else {
            return 0;
        }
    }
}
function _0x23546f(dateObject) {
    var date = new Date(dateObject);
    return date.setMinutes(dateObject.getMinutes() - dateObject.getTimezoneOffset()), date.toISOString().slice(0, 23);
}
function _0x168f49(value) {
    var date = value;
    if (!(value instanceof Date)) {
        date = new Date(value);
    }
    var groupNamePrefix = _0x350e34(date.getHours(), 2) + ":" + _0x350e34(date.getMinutes(), 2) + ":" + _0x350e34(date.getSeconds(), 2) + ":" + _0x350e34(date.getMilliseconds(), 3);
    var dupeNameCount = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    return groupNamePrefix + " " + dupeNameCount;
}
function _0x350e34(value, minInt) {
    return value.toLocaleString("en-US", {
        minimumIntegerDigits : minInt,
        useGrouping : false
    });
}
function _0x50f8b5(args) {
    var interval = _0x4495c1(args);
    if (args.launchTime == undefined) {
        args.launchTime = new Date(args.arrivalTime.getTime() - interval);
    } else {
        args.arrivalTime = new Date(args.launchTime.getTime() + interval);
    }
    if (args.randomOffsetTime == undefined) {
        args.randomOffsetTime = _0x62a382(args.launchTime.getTime(), args.launchTime.getTime() + args.randomOffset * 60000);
    }
}
function _0x4495c1(attr) {
    var speed = _0x47f226.config.speed;
    var radius = _0x47f226.config.unit_speed;
    var relativeAnimationTime = _0x4bd042(attr.source, attr.target);
    var pageSize = 1 + attr.sigil / 100;
    if (attr.type == "Attack") {
        pageSize = 1;
    }
    var blipWidth = _0x1bee0e[attr.slowestUnit];
    var _0x544df6 = 1000 * Math.round(relativeAnimationTime * (blipWidth / speed / radius) / pageSize);
    return _0x544df6;
}
function _0x62a382(offset, newPath) {
    var start = offset;
    var origNewPath = newPath;
    return new Date(start + Math.random() * (origNewPath - start));
}
function _0x2a72a3(totalDeps) {
    var scrollLeft = Math.ceil(totalDeps / _0x13e10d);
    var actual = "";
    var firstColLeft = 1;
    for (; firstColLeft <= scrollLeft; firstColLeft++) {
        if (_0x15db45 == firstColLeft) {
            actual = actual + ("<strong> &gt;" + firstColLeft + "&lt; </strong>");
        } else {
            actual = actual + ('<a class="paged-nav-item" style="cursor: pointer;" id="page_command_' + firstColLeft + '" data-id="' + firstColLeft + '"> [' + firstColLeft + "] </a>");
        }
    }
    if (_0x15db45 == -1 && scrollLeft > 1) {
        actual = actual + "<strong> &gt;All&lt; </strong>";
    } else {
        if (scrollLeft > 1) {
            actual = actual + '<a class="paged-nav-item" style="cursor: pointer;" id="page_command_all" data-id="-1"> [All] </a>';
        }
    }
    _0x1be711(actual, "#commands_table_pages", {
        callback : _0x3fd37e
    });
}
function _0x3fd37e() {
    $("[id^=page_command_]").off("click");
    $("[id^=page_command_]").on("click", function() {
        _0x15db45 = parseInt(this.dataset.id);
        _0x5f4715();
    });
}
function _0x25fa75() {
    var digitVal = document.getElementById("commandsPerPageSetting").value;
    if (digitVal != "") {
        _0x13e10d = parseInt(digitVal);
    }
    _0x758ef7.commandsPerPage = _0x13e10d;
    _0x15db45 = 1;
}
function _0x260dc5() {
    var total_pageviews_raw = document.getElementById("openTabDelay").value;
    var total_pageviews = 0;
    if (total_pageviews_raw != "") {
        total_pageviews = parseInt(total_pageviews_raw);
    }
    _0x758ef7.openTabDelay = total_pageviews;
}
function _0x491680() {
    var total_pageviews_raw = document.getElementById("timing_offset").value;
    var total_pageviews = 0;
    if (total_pageviews_raw != "") {
        total_pageviews = parseInt(total_pageviews_raw);
    }
    _0x758ef7.timing_offset = total_pageviews;
}
function _0x4cde95() {
    var total_pageviews_raw = document.getElementById("toUse_percentage").value;
    var total_pageviews = 100;
    if (total_pageviews_raw != "") {
        total_pageviews = parseInt(total_pageviews_raw);
    }
    _0x758ef7.toUse = total_pageviews;
}
function _0x7719bd() {
    var total_pageviews_raw = document.getElementById("sigil_percentage").value;
    var total_pageviews = 0;
    if (total_pageviews_raw != "") {
        total_pageviews = parseInt(total_pageviews_raw);
    }
    _0x758ef7.sigil = total_pageviews;
}
function _0x44df71() {
    var total_pageviews_raw = document.getElementById("leaveCatsAtHome").value;
    var total_pageviews = 0;
    if (total_pageviews_raw != "") {
        total_pageviews = parseInt(total_pageviews_raw);
    }
    _0x758ef7.leaveCats = total_pageviews;
}
function _0x205180() {
    var total_pageviews_raw = document.getElementById("leaveRamsAtHome").value;
    var total_pageviews = 0;
    if (total_pageviews_raw != "") {
        total_pageviews = parseInt(total_pageviews_raw);
    }
    _0x758ef7.leaveRams = total_pageviews;
}
function _0x57fd59() {
    _0x13e10d = _0x758ef7.commandsPerPage;
}
function _0x22eea6() {
    _0x7eff9b();
    _0x288fca("#menuNextCommandInfo", {
        callback : function callback(date) {
            if (date != "end") {
                document.title = "Next command:  " + date;
            } else {
                if (!_0x24dc8a()) {
                    document.title = "Commands Overview";
                } else {
                    document.title = "Next command:  00:00:00";
                }
            }
            _0xb5dc87();
            _0x7eff9b();
        },
        htmlWrapper : function handleDropImage(start) {
            if (_0x24dc8a()) {
                if (start != "end") {
                    return '<span style="color: darkblue;">Next command to ' + _0x24dc8a().target + " in " + start + "</span>";
                }
                return '<span style="color: darkblue;">Next command to ' + _0x24dc8a().target + " in 00:00:00 </span>";
            }
            return '<span style="color: red;">All of your commands are in the past!</span>';
        },
        refreshTime : 500
    });
}
function _0xb5dc87() {
    var filtersResetNode = document.getElementById("menuCommandsPlannedCounter");
    var pendid = void 0;
    var pending = Object.keys(_0x250930).length;
    if (pending > 0) {
        pendid = pending + " commands planned";
        filtersResetNode.style.color = "darkblue";
    } else {
        pendid = "No commands planned";
        filtersResetNode.style.color = "red";
    }
    filtersResetNode.innerText = pendid;
}
function _0x7eff9b() {
    var data = _0x24dc8a();
    $("#menuNextCommandInfo")[0].dataset.endtime = data ? (new Date(data.launchTime)).getTime() : Timing.getCurrentServerTime() - _0x23da2a;
    $("#menuNextCommandInfo")[0].dataset.format = "%hh%:%mm%:%ss%";
}
function _0x3edb00() {
    _0x758ef7.currentAttackTemplate = $("#selectAttackTemplate")[0].value;
    _0x758ef7.currentSupportTemplate = $("#selectSupportTemplate")[0].value;
    _0x758ef7.currentCatapultTarget = $("#catapultTarget")[0].value;
}
function _0x19bd2c() {
    _0x3edb00();
    _0x25fa75();
    _0x491680();
    _0x260dc5();
    _0x7719bd();
    _0x4cde95();
    _0x44df71();
    _0x205180();
    _0x1f4501();
    _0x3e22e1();
    _0x1adb75();
}
function _0x33956c() {
    if (!_0xe17f21.settings) {
        _0xe17f21.settings = {};
        _0xe17f21.settings.currentCatapultTarget = "wall";
        _0xe17f21.settings.currentAttackTemplate = _0x5343f6.id;
        _0xe17f21.settings.currentSupportTemplate = _0x13de06.id;
        _0xe17f21.settings.timing_offset = 0;
        _0xe17f21.settings.openTabDelay = 60;
        _0xe17f21.settings.setMSWholePlane = 0;
        _0xe17f21.settings.toUse = 100;
        _0xe17f21.settings.sigil = 0;
        _0xe17f21.settings.leaveCats = 0;
        _0xe17f21.settings.leaveRams = 0;
        _0xe17f21.settings.launchTime_offset = 0;
        _0xe17f21.settings.autoFillNt = true;
        _0xe17f21.settings.autoSendInTime = true;
        _0xe17f21.settings.autoSendNobles = true;
        _0xe17f21.settings.readMSFromPlan = false;
        _0xe17f21.settings.running = false;
        _0xe17f21.settings.ntTemplates = _0x412527;
    }
    _0x409669(_0x758ef7, _0xe17f21.settings);
    if (!_0x758ef7.currentCatapultTarget) {
        _0x758ef7.currentCatapultTarget = "wall";
    }
    if (!_0x17cd08[_0x758ef7.currentAttackTemplate]) {
        _0x758ef7.currentAttackTemplate = _0x5343f6.id;
    }
    if (!_0x17cd08[_0x758ef7.currentSupportTemplate]) {
        _0x758ef7.currentSupportTemplate = _0x13de06.id;
    }
    if (_0x758ef7.commandsPerPage != null) {
        _0x57fd59();
    }
    if (!_0x758ef7.openTabDelay) {
        _0x758ef7.openTabDelay = 60;
    }
    if (!_0x758ef7.setMSWholePlane) {
        _0x758ef7.setMSWholePlane = 0;
    }
    if (!_0x758ef7.timing_offset) {
        _0x758ef7.timing_offset = 0;
    }
    if (!_0x758ef7.toUse) {
        _0x758ef7.toUse = 100;
    }
    if (!_0x758ef7.sigil) {
        _0x758ef7.sigil = 0;
    }
    if (!_0x758ef7.leaveCats) {
        _0x758ef7.leaveCats = 0;
    }
    if (!_0x758ef7.leaveRams) {
        _0x758ef7.leaveRams = 0;
    }
    if (!_0x758ef7.launchTime_offset) {
        _0x758ef7.launchTime_offset = 0;
    }
    if (!_0x758ef7.autoFillNt) {
        _0x758ef7.autoFillNt = true;
    }
    if (!_0x758ef7.autoSendInTime) {
        _0x758ef7.autoSendInTime = true;
    }
    if (!_0x758ef7.autoSendNobles) {
        _0x758ef7.autoSendNobles = true;
    }
    if (!_0x758ef7.readMSFromPlan) {
        _0x758ef7.readMSFromPlan = false;
    }
    if (!_0x758ef7.running) {
        _0x758ef7.running = false;
    }
    if (!_0x758ef7.map) {
        _0x758ef7.map = {
            showAlly : true,
            showEnemy : true
        };
    }
    if (!_0x758ef7.ntTemplates) {
        _0x758ef7.ntTemplates = _0x412527;
    }
    _0x4dfa81();
}
function _0x3e22e1() {
    _0xe17f21.commands = _0x250930;
    _0x12391d(_0x59cedc, _0xe17f21);
}
function _0x458bdd() {
    _0x3e22e1();
    _0x57fd59();
    document.getElementById("commandsPerPageSetting").value = _0x13e10d;
    _0x1adb75();
}
function _0x58f456() {
    var renovateJson = _0x167cf0(_0x59cedc);
    Object.assign(_0xe17f21, renovateJson);
    _0x33956c();
    _0x12a8ea();
    _0x12aab7();
    _0xe17f21.settings = _0x758ef7;
    _0xe17f21.commands = _0x250930;
}
function _0x2024a4() {
    var device = _0x167cf0(_0x59cedc);
    _0xe17f21.commands = device.commands;
    _0x12a8ea();
    _0x14c4ee(_0x250930);
}
function _0x167cf0(id) {
    var roleId = localStorage.getItem(id);
    if (roleId != undefined && roleId != "") {
        return JSON.parse(roleId);
    }
    return null;
}
function _0x12391d(id, object) {
    if (typeof object != "string") {
        localStorage.setItem(id, JSON.stringify(object));
    } else {
        localStorage.setItem(id, object);
    }
}
function _0x95231a() {
    document.getElementById("importExportInput").style.display = "initial";
    document.getElementById("importExportInput").value = _0x38c375(JSON.stringify(_0xe17f21));
}
function _0x4391c1() {
    _0x1ad1fc(_0x38c375(JSON.stringify(_0xe17f21)), "commandsOverviewData.txt", "text/plain");
}
function _0x1a5e66() {
    var innerMsg = document.getElementById("importExportInput").value;
    var categories = JSON.parse(_0x5c05b4(innerMsg));
    if (categories.commands) {
        _0x409669(_0x250930, categories.commands);
    }
    if (categories.settings) {
        _0x409669(_0x758ef7, categories.settings);
    }
    if (categories.commandIdCounter) {
        _0xe17f21.commandIdCounter = categories.commandIdCounter;
    }
    _0x258672();
    _0x458bdd();
}
function _0x50d196() {
    document.getElementById("importExportInput").style.display = "initial";
    document.getElementById("importDataButton").style.display = "initial";
    document.getElementById("cancelImportDataButton").style.display = "initial";
    document.getElementById("startImportDataButton").style.display = "none";
    document.getElementById("importExportInput").value = "";
    document.getElementById("exportDataButton").style.display = "none";
    document.getElementById("exportDataToFileButton").style.display = "none";
}
function _0x258672() {
    document.getElementById("importExportInput").style.display = "none";
    document.getElementById("importDataButton").style.display = "none";
    document.getElementById("cancelImportDataButton").style.display = "none";
    document.getElementById("startImportDataButton").style.display = "initial";
    document.getElementById("exportDataButton").style.display = "initial";
    document.getElementById("exportDataToFileButton").style.display = "initial";
}
function _0x59ea04() {
    var self = JSON.parse(window.sessionStorage.getItem("commandsOverviewCommandData"));
    if (!self) {
        console.log("No attack programmed!");
        return;
    }
    if (!document.getElementById("unit_input_spear") && !$("#" + _0x252717.snipeScript.sendButton).is(":visible")) {
        _0x178b57(_0xc59f31);
    }
    if (document.getElementsByClassName("error_box")[0] != undefined) {
        var $error_box = document.getElementsByClassName("error_box");
        if ($error_box != null) {
            _0x250930[self.id].done = true;
            _0x3e22e1();
            window.close();
        }
    }
    if (JSON.parse(window.sessionStorage.getItem("tabDone"))) {
        _0x4382f2();
        return;
    }

    if (self.done) {
        console.log("Attack already sent!");
        return;
    }
    var _0x27d637 = _0x167cf0(_0xb37b33);
    if (!_0x27d637 && !_0x561860) {
        _0x178b57("Snipe/Schedule script is not configured!");
        return;
    }
    if ($("#" + _0x252717.snipeScript.sendButton).length > 0) {
        _0x250930[self.id].done = true;
        _0x3e22e1();
        var totalSeconds = (new Date(self.launchTime)).getTime();
        if (self.randomOffset > 0) {
            totalSeconds = (new Date(self.randomOffsetTime)).getTime();
        }
        var seconds = totalSeconds % 1000;
        var appendZero = function makeSplineChartPath(n, width) {
            return n = n + "", n.length >= width ? n : (new Array(width - n.length + 1)).join("0") + n;
        };
        seconds = appendZero(seconds.toString(), 3);
        var photoText = "<tr>\n            <td>Command <b>" + self.name + "</b> arrival:</td>\n            <td>" + _0x168f49(new Date(self.arrivalTime)).split(" ")[0] + ' <b id="sendTimer" class="overviewsTimer" data-endtime="' + totalSeconds + '" data-format="%hh%:%mm%:%ss%:%msmsms%"></b>\n        </tr>';
        $(".vis > tbody:eq(0)").append(photoText);
        _0x288fca("#sendTimer", {
            callback : function callback(date) {
                if (date != "end") {
                    document.title = _0x3f6a42.world + " - " + date;
                } else {
                    document.title = _0x3f6a42.world + " - " + "Command overdue";
                }
            },
            htmlWrapper : function function_print(start) {
                if (start != "end") {
                    return '(<span style="color: green;">' + start + "</span>)";
                } else {
                    return '(<span style="color: red;">Command overdue</span>)';
                }
            },
            refreshTime : 100
        });
        if (_0x758ef7.running && _0xaed88c) {
            if (!_0x412527[self.ntTemplate].onlyNobles || _0x57a913 && _0x689c6d && self.slowestUnit == "snob") {
                _0x55b36d(self);
            }
            setTimeout(function() {
                if (self.autoSend) {
                    _0x3b1e69(self);
                }
            }, 3000);
        }
    } else {
        _0x4a6272(self, self.units, self.slowestUnit, self.toUse, self.ntTemplate);
        if (!_0x758ef7.autoSendNobles && self.slowestUnit == "snob") {
            document.getElementById("command-form-warning").previousElementSibling.innerHTML += '<span style="color: red"> (Auto send nobles is turned off)</span> ';
            return;
        } else {
            if (!self.autoSend) {
                document.getElementById("command-form-warning").previousElementSibling.innerHTML += '<span style="color: red"> (This command is excluded from auto send)</span> ';
                return;
            }
        }
        if (self.type == "Attack") {
            $("#target_support")[0].style.display = "none";
            if (_0x758ef7.running && _0xaed88c) {
                setTimeout(function() {
                    $("#target_attack")[0].click();
                }, 1000);
            }
        } else {
            if (self.type == "Support") {
                $("#target_attack")[0].style.display = "none";
                if (_0x758ef7.running && _0xaed88c) {
                    setTimeout(function() {
                        $("#target_support")[0].click();
                    }, 1000);
                }
            }
        }
    }
}
function _0x3b1e69(self) {

    var options = _0x167cf0(_0xb37b33);
    if (_0x561860) {
        var gotoNewOfflinePage = function loadPredicates(type) {
            var m_iterations_in_chunk = type - (Timing.getCurrentServerTime() - _0x23da2a);
            var m_iterations_done = performance.now();
            for (; performance.now() - m_iterations_done < m_iterations_in_chunk;) {
            }
            $("#" + _0x252717.snipeScript.sendButton)[0].click();
        };
        var ltime = new Date(self.launchTime);
        if (self.randomOffset > 0) {
            ltime = new Date(self.randomOffsetTime);
        }
        var item = $("#place_confirm_catapult_target").find("select:eq(0)")[0];
        if (self.catapultTarget && item) {
            item.value = self.catapultTarget;
        }
        window.sessionStorage.setItem("tabDone", true);
        console.log(ltime.getTime());
        console.log("Timeout actual send", ltime.getTime() - (Timing.getCurrentServerTime() - _0x23da2a) - _0x1c1e99 * 1000);
        setTimeout(function() {
            return gotoNewOfflinePage(ltime.getTime() + _0x758ef7.timing_offset);
        }, ltime.getTime() - (Timing.getCurrentServerTime() - _0x23da2a) - _0x1c1e99 * 1000);
    } else {
        _0x123350(options.confirmButton, function() {
            setTimeout(function() {
                var YEAR_VIEW_CELL_COUNT = (new Date).getTimezoneOffset() * 60000;
                var begin = new Date(self.arrivalTime);
                begin = new Date(begin - YEAR_VIEW_CELL_COUNT);
                var _startingFret = begin.toISOString().slice(0, -1);
                var item = $("#place_confirm_catapult_target").find("select:eq(0)")[0];
                if (self.catapultTarget && item) {
                    item.value = self.catapultTarget;
                }
                document.getElementById(options.dateInput).value = _startingFret;
                document.getElementById(options.offsetInput).value = _0x758ef7.timing_offset;
                window.sessionStorage.setItem("tabDone", true);
            }, 1000);
            if (self.autoSend) {
                setTimeout(function() {
                    document.getElementById(options.confirmButton).click();
                }, 2000);
            }
        }, 500, 9000);
    }
}
function _0x4a6272(self, ranges, index, total, num) {
    if (!total) {
        total = 100;
    }
    total = parseInt(total);
    if (_0x412527[num] && _0x412527[num].id.includes("NOBLE_RED") && _0x758ef7.autoFillNt && index == "snob" && _0xaed88c) {
        $.each(ranges, function(tool, min) {
            if (_0x3f6a42.units.includes(tool)) {
                if (_0x412527[num].id == "NT_1ST_NOBLE_RED" || _0x412527[num].id == "NT_1ST_5NOBLE_RED") {
                    var value = $("#unit_input_" + tool)[0].dataset.allCount;
                    var n = min == -1 ? value : Math.min(min, value);
                    n = Math.round(Math.min(n, total * value / 100));
                    $("#unit_input_" + tool)[0].value = n;
                    if (tool == "axe" && n > 100) {
                        $("#unit_input_" + tool)[0].value = n - 100;
                    }
                } else {
                    var value = $("#unit_input_" + tool)[0].dataset.allCount;
                    var temp = min == -1 ? value : Math.min(min, value);
                    temp = Math.round(Math.min(temp, total * value / 100));
                    $("#unit_input_" + tool)[0].value = 0;
                    if (tool == "axe" && temp > 100) {
                        $("#unit_input_" + tool)[0].value = 34;
                    } else {
                        if (tool == "snob") {
                            $("#unit_input_" + tool)[0].value = 1;
                        }
                    }
                }
            }
        });
    } else {
        if (index == "snob" && _0x758ef7.autoFillNt && _0xaed88c && _0x412527[num].brownNoble) {
            $("#unit_input_axe")[0].value = 1000;
            $("#unit_input_snob")[0].value = 1;
        } else {
            $.each(ranges, function(name, value) {
                if (_0x3f6a42.units.includes(name)) {
                    var n = $("#unit_input_" + name)[0].dataset.allCount;
                    var val = value == -1 ? n : Math.min(value, n);
                    val = Math.round(Math.min(val, total * n / 100));
                    if ((name == "catapult" || name == "ram") && (_0x412527[num].name == "2 Nobles Selected/Rest" || _0x412527[num].name == "4 Nobles Selected/Rest") && _0x758ef7.autoFillNt) {
                        val = n;
                    }
                    if (name == "catapult" && value == -1) {
                        val = val - self.leaveCats;
                        if (val < 0) {
                            val = 0;
                        }
                    } else {
                        if (name == "ram" && value == -1) {
                            val = val - self.leaveRams;
                            if (val < 0) {
                                val = 0;
                            }
                        }
                    }
                    $("#unit_input_" + name)[0].value = val;
                }
            });
        }
    }
}
function _0x14d43a() {
    var attr = $(".vis > tbody > tr:nth-child(3) > td:nth-child(2) > a")[0];
    if (attr && attr.href) {
        return !attr.href.includes("info_player");
    } else {
        return true;
    }
}
function _0x2fa8ad(ballNumber) {
    return !_0x1bfb72[ballNumber];
}
function _0x178b57(selector) {
    UI.ErrorMessage(selector);
}
function _0x320bf4(selector) {
    UI.SuccessMessage(selector);
}
function _0xc20a0f() {
    _0x14dcd3();
}
function _0x29412c() {
    var photoText = '<div class="popup_box_container" id="helpConfig_popup" style="display:none;">\n        <div class="popup_box show" style="width: 600px;">\n            <div class="popup_box_content">\n                <a class="popup_box_close tooltip-delayed" id="helpPopup_cross" href="javascript:void(0)"> </a>\n                <h1 style="text-align: center; color: purple">Add Plan Help</h1>\n                <div>\n                    <h2>How to use:</h2>\n                    For now there are 2 tested planners that work with our parse system:\n                        <p></p>\n                        \u00c2\u00b7 Fodox Tribal Wars Utility\n                        <br>\n                        \u00c2\u00b7 TW AAP\n                    <p></p>\n                    <h3>Fodox Tribal Wars Utility</h3>\n                    In this tool, after you plan, you shall use "Show only essential columns", the other settings it is up to you since the result is the same.\n                    <p></p>\n                    <img src="https://i.postimg.cc/ydfh1Gzm/transferir-1.png">\n                    <p></p>\n                    After clicking "Plan" you can copy either "Plain Text" or "BB-code" text box.\n                    <p></p>\n                    <img src="https://i.postimg.cc/zGnnbvrX/Webp-net-resizeimage-1.png">\n                    <p></p>\n                    Now that you have copied either of the plans you can paste it in this script text box and click "Add plan".\n                    <p></p>\n                    <h3>TW AAP</h3>\n                    Feel free to use TW AAP without restrictions, both "export" and "simple export" work so you can just copy paste one of them into our script.\n                    <p></p>\n                    <h2>Any other planner</h2>\n                    <div style="font-size: 13px">\n                        You can use any other planner if the lines of the plan respects the following rules:\n                        <p></p>\n                        \u00c2\u00b7 Source coordinate before Target coordinate\n                        <br>\n                        \u00c2\u00b7 1 and only 1 date (The planner assumes this is the launch time of the command)\n                        <br>\n                        \u00c2\u00b7 The line needs to have the slowest unit written (spear,sword,...,ram,snob)\n                        <br>\n                        \u00c2\u00b7 The line can define the type of the command ("Attack","Support"), otherwise it is considered that the command is an "Attack"\n                    </div>\n                </div>\n                <br>\n                <br>\n                <div>\n                    <h5 style="color: darkblue">General information</h5>\n                    <div>\n                        Script made by Im Kumin and fmthemaster.\n                        <p></p>\n                        If you have any question feel free to join the discord: <a style="color: -webkit-link" href="https://discord.gg/e2ZCtKURu8">Discord</a> (<- click here)\n                        <p></p>\n                    </div>\n                </div>\n            </div>        </div>\n    </div>\n    <div class="fader" id="helpPopup_fader" style="display:none;"></div>';
    $("body").append(photoText);
    $("#helpPopup_fader").off("click");
    $("#helpPopup_cross").off("click");
    $("#helpPopup_fader").on("click", function() {
        _0x55a0a4();
    });
    $("#helpPopup_cross").on("click", function() {
        _0x55a0a4();
    });
    _0x55a0a4();
}
function _0x14dcd3() {
    $("#helpConfig_popup")[0].style.display = "block";
    $("#helpPopup_fader")[0].style.display = "block";
}
function _0x55a0a4() {
    $("#helpConfig_popup")[0].style.display = "none";
    $("#helpPopup_fader")[0].style.display = "none";
}
function _0x1b5636() {
    _0x4d8833 = (new Date).getTime();
    var refNode = document.getElementById("tab-bar");
    var logentry_div = document.createElement("div");
    logentry_div.setAttribute("id", "memoPlanner");
    logentry_div.setAttribute("class", "memo_container");
    refNode.parentNode.insertBefore(logentry_div, refNode.nextSibling.nextSibling.nextSibling.nextSibling);
    _0x527fce();
    var infoBoxLatLng = '\n    <div id="loaderDiv" class="memo_script" style="vertical-align: middle; clear: both;width: 400px">\n        <div class="forum-content" style="grid-template-columns: 4fr 4fr;grid-gap: 10px;padding: 0;">\n            <table class="vis nowrap" style="width: 100%;">\n                <thead>\n                    <tr>\n                        <th colspan="3"><span class="column-title" style="font-size:16px">Loading screen</span></th>\n                    </tr>\n                </thead>\n                <tbody id="loadResourceDiv">\n                </tbody>\n            </table>\n        </div>\n    </div>';
    _0x1be711(infoBoxLatLng, "#memoPlanner", {
        callback : _0x32960d
    });
}
function _0x32960d() {
}
function _0x64078() {
    document.getElementById("commandSenderDiv").style.display = "block";
    _0x4d8833 = (new Date).getTime() - _0x4d8833;
    if (_0x588617) {
        console.log(_0x4e7a63);
        console.log("Total time loading: " + _0x4d8833 + " ms");
    }
}
async function _0x1e7188(func, error, async) {
    var start = (new Date).getTime();
    var options = {
        description : error,
        function : func,
        async : async
    };
    _0x4e7a63.push(options);
    var elementId = "loaderSlot" + (_0x4e7a63.length - 1);
    document.getElementById("loadResourceDiv").innerHTML += '\n        <tr>\n            <td id="' + elementId + '" style="padding-left: 1em;height: 30px;">\n                <a class="loader" style="float: left"></a>\n                <a href="javascript:void(0)" style="display: flex;justify-content: center;align-items: center;color:red">' + error + "</a>\n            </td>\n        </tr>";
    if (async) {
        func();
    } else {
        await func();
    }
    if (document.getElementById(elementId) != null) {
        document.getElementById(elementId).childNodes[1].classList.remove("loader");
        document.getElementById(elementId).childNodes[3].style.color = "green";
    }
    var l = (new Date).getTime();
    options.elapsedTime = l - start;
}
function _0x1be711(latLng, query, _ref) {
    var _ref$wrapper = _ref.wrapper;
    var geoCodeByCoords = _ref$wrapper === undefined ? function(name, debugEl) {
        return debugEl.innerHTML = name;
    } : _ref$wrapper;
    var slide = _ref.callback;
    var animation = slide === undefined ? function() {
        return;
    } : slide;
    geoCodeByCoords(latLng, $(query)[0]);
    animation();
}
function _0x409669(methods, aliases) {
    Object.keys(methods).forEach(function(methodIndex) {
        delete methods[methodIndex];
    });
    Object.assign(methods, aliases);
}

function _0x38c375(methods) {
    var app = void 0;
    if ((typeof methods === "undefined" ? "undefined" : _typeof(methods)) == "object") {
        app = JSON.stringify(methods);
    } else {
        if (typeof methods == "string") {
            app = methods;
        } else {
            return "";
        }
    }
    return encodeURI(app);
}
function _0x5c05b4(expectedHref) {
    return decodeURI(expectedHref);
}
function _0x3edfc4(a) {
    return Object.assign.apply(Object, [{}].concat(_toConsumableArray(a)));
}
function _0x1ad1fc(src, filename, mimeType) {
    var blob = new Blob([src], {
        type : mimeType
    });
    if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        var anchor = document.createElement("a");
        var url = URL.createObjectURL(blob);
        anchor.href = url;
        anchor.download = filename;
        document.body.appendChild(anchor);
        anchor.click();
        setTimeout(function() {
            document.body.removeChild(anchor);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}
function _0x35ce22() {
    var email_url = "https://" + _0x7f9521 + "/map/village.txt";
    var data = localStorage.getItem("globalVillageData" + _0x7f9521.split(".")[0]);
    var now = new Date;
    var _0x22c01c = _0x1bbd01 * 60 * 60 * 1000;
    if (data) {
        data = JSON.parse(data);
        if (data.lastUpdate) {
            if ((new Date(data.lastUpdate)).getTime() + _0x22c01c > now.getTime()) {
                _0x1bfb72 = data.villageData;
                return;
            }
        }
    }
    $.get(email_url, function(clusterShardData) {
        var row = clusterShardData.split("\n");
        var CR_index = 0;
        for (; CR_index < row.length; CR_index++) {
            var tiledImageBRs = row[CR_index].split(",");
            var priorityid = tiledImageBRs[0];
            var groupNamePrefix = tiledImageBRs[2];
            var tiledImageBR = tiledImageBRs[3];
            var playerID = tiledImageBRs[4];
            _0x1bfb72[groupNamePrefix + "|" + tiledImageBR] = {
                id : priorityid,
                player_id : playerID
            };
        }
    }).then(function() {
        var userVideos = {
            villageData : _0x1bfb72,
            lastUpdate : now
        };
        localStorage.setItem("globalVillageData" + _0x7f9521.split(".")[0], JSON.stringify(userVideos));
    });
}
function _0x4372c8() {
    var data = localStorage.getItem("unitsData" + _0x7f9521.split(".")[0]);
    var now = new Date;
    var _0x358bbc = _0x1bbd01 * 60 * 60 * 1000;
    if (data) {
        data = JSON.parse(data);
        if (data.lastUpdate) {
            if ((new Date(data.lastUpdate)).getTime() + _0x358bbc > now.getTime()) {
                return data.unitsData;
            }
        }
    }
    return new Promise(function(formatDPS, saveNotifs) {
        var dps = {};
        $.get("/interface.php?func=get_unit_info").done(function(mei) {
            $(mei).find("config").children().each(function(canCreateDiscussions, html) {
                dps[$(html).prop("nodeName")] = {
                    speed : $(html).find("speed").text()
                };
            });
            if (dps.militia) {
                delete dps.militia;
            }
            var userVideos = {
                unitsData : dps,
                lastUpdate : now
            };
            localStorage.setItem("unitsData" + _0x7f9521.split(".")[0], JSON.stringify(userVideos));
            formatDPS(dps);
        }).fail(function() {
            return saveNotifs(_0x178b57("Error units data"));
        });
    });
}
function _0x341c5e() {
    $(".maincell").css("width", "1200px");
    $("[class^='bg_left']").css("width", "24px");
    $("[class^='bg_right']").css("width", "0px");
    $("#SkyScraperAdCell").css("background", "transparent url('graphic/index/mainborder-right.png') scroll top repeat-y");
    $(".bg_bottomright").css("background", "transparent url('graphic/index/mainborder-corner-right.png') scroll top no-repeat");
}
function _0x3314a8() {
    document.getElementById("side-notification-container").innerHTML = "";
}
function _0x123350(field, step, timeout, limit) {
    var splitterPos = Date.now();
    (function init() {
        if (document.getElementById(field) != null && $("#" + field).length > 0) {
            step();
            return;
        } else {
            setTimeout(function() {
                if (limit && Date.now() - splitterPos > limit) {
                    return;
                }
                init();
            }, timeout);
        }
    })();
}
function _0x288fca(e, _ref2) {
    var _ref2$NotAsked = _ref2.htmlWrapper;
    var floori = _ref2$NotAsked === undefined ? function(start2) {
        return start2;
    } : _ref2$NotAsked;
    var callback = _ref2.callback;
    var offseti = callback === undefined ? _0x32960d : callback;
    var _ref2$method = _ref2.refreshTime;
    var rumbleSpeed = _ref2$method === undefined ? 200 : _ref2$method;
    setInterval(function() {
        $(e).html(function(canCreateDiscussions, isSlidingUp) {
            if (!this.classList.contains("overviewsTimer")) {
                return isSlidingUp;
            }
            var i = this.dataset.format;
            if (!i) {
                i = "%hh%:%mm%:%ss%";
            }
            var start = _0x4099db(this.dataset.randomtime, parseInt(this.dataset.endtime), Timing.getCurrentServerTime() - _0x23da2a, i);
            return offseti(start), floori(start);
        });
    }, rumbleSpeed);
}
function _0x4099db(value, expected, str) {
    var that = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "%hh%:%mm%:%ss%:%msmsms%";
    var s = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "end";
    var b = expected;
    var v = str;
    var current = value;
    if (value && !(value instanceof Date)) {
        current = new Date(parseInt(value));
    }
    if (!(expected instanceof Date)) {
        b = new Date(expected);
    }
    if (!(expected instanceof Date)) {
        v = new Date(str);
    }
    var require = function QueryStringParser$placeNestedValue(value, i) {
        return value < 10 ** i ? ("" + "0".repeat(i) + value).slice(-i) : "" + value;
    };
    var diff = b - v;
    if (diff < 0) {
        if (current && current - v >= 0) {
            return "OnHold";
        }
        return s;
    }
    var MINUTES = 60000;
    var HOURS = 60 * MINUTES;
    var DAYS = 24 * HOURS;
    var YEARS = 30 * DAYS;
    var A = Math.floor(diff / YEARS);
    var D = Math.floor(diff % YEARS / DAYS);
    var H = Math.floor(diff % DAYS / HOURS);
    var M = Math.floor(diff % HOURS / MINUTES);
    var S = Math.floor(diff % MINUTES / 1000);
    var ms = Math.floor(diff % 1000);
    if (!that.match(/%(M)+%/g)) {
        D = D + A * 30;
    }
    if (!that.match(/%(D)+%/g)) {
        H = H + D * 24;
    }
    if (!that.match(/%(h)+%/g)) {
        M = M + H * 60;
    }
    if (!that.match(/%(m)+%/g)) {
        S = S + M * 60;
    }
    if (!that.match(/%(s)+%/g)) {
        ms = ms + S * 1000;
    }
    var client = that;
    return $.each({
        M : A,
        D : D,
        h : H,
        m : M,
        s : S,
        ms : ms
    }, function(b, module) {
        var regex = RegExp("%(" + b + ")+%", "g");
        var values = that.match(regex);
        if (values) {
            $.each(values, function(canCreateDiscussions, url) {
                var regex = RegExp("" + b, "g");
                var start = url.match(regex).length;
                client = client.replace(url, require(module, start));
            });
        }
    }), client;
}
function _0x2466a6() {
}
function _0x5473f6(canCreateDiscussions) {
}
function _0x455eae() {

    _0x295291();
    _0xa59085("%cInitializing bot...", "font-size: 20px; color: #0099ff;");
    setTimeout(function() {
        _0xa59085("%cBot initialized!", "font-size: 20px; color: Red;");
        _0x167cc1();
    }, _0x2d7384 * 1000);
}
function _0x167cc1() {

    setTimeout(_0x35ec66, 5000);
    setInterval(_0x170d26, _0x2e4585 * 1000);
}
function _0x170d26() {

    _0x3314a8();
    _0x2024a4();
    if (_0x758ef7.running) {
        var fields_to_add = _0x56e4f3(_0x758ef7.openTabDelay);
        $.each(fields_to_add, function(i, timeline_mode) {
            if (!fields_to_add[i].preparedByBot) {
                _0x250930[fields_to_add[i].id].preparedByBot = true;
                _0x3e22e1();
                setTimeout(function() {
                    _0x39f9b0(timeline_mode.id);
                }, 500);
            }
        });
    }
}
function _0x55b36d(_0x5ab131$jscomp$0) {
    var _0x4b2849$jscomp$0 = 0;
    for (; document[_0x14e6d3(3320) + _0x3df7e8(1511) + _0x3df7e8(1572) + _0x14e6d3(3127) + "Id"]("tro" + _0x3df7e8(3091) + _0x14e6d3(1668) + _0x3df7e8(2701) + _0x14e6d3(3118) + _0x3df7e8(2827) + "n") && _0x4b2849$jscomp$0 < _0x1455ba[_0x3df7e8(1787) + _0x14e6d3(3306) + "Qnt"] - 1; _0x4b2849$jscomp$0++) {
        if (_0x3df7e8(3553) + "uv" !== "fWhkQ") {
            setTimeout(function() {
                var getNumber = _0x14e6d3;
                var _second2hour = _0x3df7e8;
                if (getNumber(2438) + "IZ" === _second2hour(2438) + "IZ") {
                    document[_second2hour(3320) + "Ele" + "men" + getNumber(3127) + "Id"]("tro" + _second2hour(3091) + _second2hour(1668) + _second2hour(2701) + getNumber(3118) + getNumber(2827) + "n")[getNumber(3596) + "ck"]();
                } else {
                    var bits = _0x718671[_second2hour(2144) + getNumber(2975) + "en"][getNumber(523) + "m"](_0x465ca8);
                    var i = bits[getNumber(3385) + getNumber(2890) + "me"];
                    if (_typeof(_0x31bc2c[i]) == getNumber(2716) + getNumber(1355) + _second2hour(2483)) {
                        _0x47be09[i] = _0x3b45cb(bits);
                    } else {
                        if (_typeof(_0x53442e[i].push) == getNumber(2716) + "efi" + getNumber(2483)) {
                            var unloadedImgElement = _0x14f937[i];
                            _0xda781d[i] = [];
                            _0x19ecb1[i][_second2hour(1669) + "h"](unloadedImgElement);
                        }
                        _0x36be82[i][getNumber(1669) + "h"](_0x426fa3(bits));
                    }
                }
            }, _0x4b2849$jscomp$0 * 200);
            _0x4e64ee++;
        } else {
            _0x2eab4a(_0x268e41, _0x1c4a82(_0x10f2e1)[0]);
            _0x1532c9();
        }
    }
    setTimeout(function() {
        if (_0x24dc3d(682) + "GX" !== _0x92a987(682) + "GX") {
            if (_0x465218()) {
                if (_0x5870bd != _0x24dc3d(550)) {
                    return "<sp" + _0x24dc3d(2851) + _0x24dc3d(497) + _0x24dc3d(2078) + '"co' + _0x24dc3d(2997) + _0x92a987(1605) + _0x92a987(1729) + _0x24dc3d(2926) + _0x24dc3d(1995) + _0x92a987(1179) + _0x24dc3d(2548) + "com" + _0x92a987(2039) + _0x24dc3d(2866) + "o " + _0x15690c()[_0x24dc3d(2616) + _0x24dc3d(3320)] + " in " + _0x5dfe82 + (_0x24dc3d(2238) + _0x24dc3d(1232) + ">");
                }
                return _0x92a987(579) + _0x92a987(2851) + _0x24dc3d(497) + _0x92a987(2078) + _0x24dc3d(2493) + "lor" + _0x92a987(1605) + _0x92a987(1729) + _0x24dc3d(2926) + _0x92a987(1995) + _0x92a987(1179) + _0x92a987(2548) + _0x24dc3d(2874) + _0x92a987(2039) + _0x24dc3d(2866) + "o " + _0x2b200d()[_0x24dc3d(2616) + _0x92a987(3320)] + (_0x92a987(3121) + _0x92a987(1358) + _0x92a987(2308) + ":00" + _0x24dc3d(2443) + _0x92a987(1841) + "n>");
            }
            return _0x24dc3d(579) + _0x24dc3d(2851) + _0x24dc3d(497) + _0x92a987(2078) + _0x24dc3d(2493) + "lor" + _0x24dc3d(2792) + "ed;" + '">A' + _0x92a987(2022) + "of " + _0x92a987(1387) + "r c" + _0x24dc3d(2591) + _0x24dc3d(2132) + _0x24dc3d(3245) + _0x24dc3d(1038) + _0x24dc3d(2935) + "the" + _0x92a987(2602) + _0x24dc3d(1015) + _0x24dc3d(2238) + "pan" + ">";
        } else {
            eval(_0x1455ba[_0x92a987(3358) + "lFu" + "nct" + _0x92a987(2856)])();
        }
    }, 1200);
}
function fill2nd3rdNoblesNT() {
    var bChilds = document.getElementsByClassName("units-row")[0].childNodes;
    var plist = document.getElementsByClassName("units-row")[1].childNodes;
    var c = document.getElementsByClassName("units-row")[2].childNodes;
    var boxes = document.getElementsByClassName("units-row")[3].childNodes;
    var id = 1;
    for (; id < plist.length - 1; id++) {
        plist[id].childNodes[0].value = "";
        c[id].childNodes[0].value = "";
        boxes[id].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var i = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var k = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var nextModulePath = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 1].childNodes[0].textContent) : 0;
    var teamScoreValue = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i].childNodes[0].textContent);
    var _startingFret = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 2 + k].childNodes[0].textContent);
    var schoolId = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 3 + k].childNodes[0].textContent);
    var time = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var div_width_half = parseInt(time) - parseInt(bChilds[5].childNodes[0].textContent) - 1000;
    var _listItemText = parseInt(div_width_half / 2);
    var nextShowTimeSeconds = teamScoreValue - parseInt(bChilds[i].childNodes[0].textContent);
    var pageInd = parseInt(nextShowTimeSeconds / 2);
    var j = _0x3f6a42.units.includes("archer") ? 6 : 5;
    plist[3].childNodes[0].value = _listItemText;
    plist[j].childNodes[0].value = pageInd;
    if (nextModulePath > 0) {
        plist[j + 1].childNodes[0].value = nextModulePath;
    }
    plist[j + 2 + k].childNodes[0].value = _startingFret;
    plist[j + 3 + k].childNodes[0].value = schoolId;
    plist[j + 5 + k].childNodes[0].value = 1;
    c[3].childNodes[0].value = _listItemText;
    c[j].childNodes[0].value = pageInd;
    c[j + 5 + k].childNodes[0].value = 1;
    boxes[3].childNodes[0].value = 1000;
    boxes[j + 5 + k].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill2ndNobleBuffNT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var values = document.getElementsByClassName("units-row")[1].childNodes;
    var boxes = document.getElementsByClassName("units-row")[2].childNodes;
    var bChilds = document.getElementsByClassName("units-row")[3].childNodes;
    var id = 1;
    for (; id < values.length - 1; id++) {
        values[id].childNodes[0].value = "";
        boxes[id].childNodes[0].value = "";
        bChilds[id].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var index = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var i = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var brick_gradient = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 1].childNodes[0].textContent) : 0;
    var groutspace = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index].childNodes[0].textContent);
    var elapsedTimeBeforeLoad = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 2 + i].childNodes[0].textContent);
    var waitTime = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 3 + i].childNodes[0].textContent);
    var laneCount = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var backward = 5000 - groutspace - brick_gradient - elapsedTimeBeforeLoad - waitTime - 1;
    var playEditorURL = 1000;
    if (backward > laneCount - 3000) {
        backward = laneCount - 3000;
    } else {
        if (backward < laneCount - 3000) {
            playEditorURL = laneCount - 2000 - backward;
        }
    }
    if (backward < 0) {
        backward = 0;
    }
    var j = _0x3f6a42.units.includes("archer") ? 6 : 5;
    values[3].childNodes[0].value = backward;
    values[j].childNodes[0].value = groutspace;
    if (brick_gradient > 0) {
        values[j + 1].childNodes[0].value = brick_gradient;
    }
    values[j + 2 + i].childNodes[0].value = elapsedTimeBeforeLoad;
    values[j + 3 + i].childNodes[0].value = waitTime;
    values[j + 5 + i].childNodes[0].value = 1;
    boxes[3].childNodes[0].value = playEditorURL;
    boxes[j].childNodes[0].value = 0;
    boxes[j + 5 + i].childNodes[0].value = 1;
    bChilds[3].childNodes[0].value = 1000;
    bChilds[j + 5 + i].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill3rdNobleBuffNT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var a = document.getElementsByClassName("units-row")[1].childNodes;
    var params = document.getElementsByClassName("units-row")[2].childNodes;
    var rootNodes = document.getElementsByClassName("units-row")[3].childNodes;
    var n = 1;
    for (; n < a.length - 1; n++) {
        a[n].childNodes[0].value = "";
        params[n].childNodes[0].value = "";
        rootNodes[n].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var i = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var j = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var brick_gradient = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 1].childNodes[0].textContent) : 0;
    var groutspace = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i].childNodes[0].textContent);
    var elapsedTimeBeforeLoad = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 2 + j].childNodes[0].textContent);
    var waitTime = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 3 + j].childNodes[0].textContent);
    var laneCount = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var backward = 5000 - groutspace - brick_gradient - elapsedTimeBeforeLoad - waitTime - 1;
    var playEditorURL = 1000;
    if (backward > laneCount - 3000) {
        backward = laneCount - 3000;
    } else {
        if (backward < laneCount - 3000) {
            playEditorURL = laneCount - 2000 - backward;
        }
    }
    if (backward < 0) {
        backward = 0;
    }
    var p = _0x3f6a42.units.includes("archer") ? 6 : 5;
    params[3].childNodes[0].value = backward;
    params[p].childNodes[0].value = groutspace;
    if (brick_gradient > 0) {
        params[p + 1].childNodes[0].value = brick_gradient;
    }
    params[p + 2 + j].childNodes[0].value = elapsedTimeBeforeLoad;
    params[p + 3 + j].childNodes[0].value = waitTime;
    params[p + 5 + j].childNodes[0].value = 1;
    a[3].childNodes[0].value = playEditorURL;
    a[p].childNodes[0].value = 0;
    a[p + 5 + j].childNodes[0].value = 1;
    rootNodes[3].childNodes[0].value = 1000;
    rootNodes[p + 5 + j].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill2ndNobleBuffWith5NoblesNT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var values = document.getElementsByClassName("units-row")[1].childNodes;
    var boxes = document.getElementsByClassName("units-row")[2].childNodes;
    var bChilds = document.getElementsByClassName("units-row")[3].childNodes;
    var subNodes0 = document.getElementsByClassName("units-row")[4].childNodes;
    var id = 1;
    for (; id < values.length - 1; id++) {
        values[id].childNodes[0].value = "";
        boxes[id].childNodes[0].value = "";
        bChilds[id].childNodes[0].value = "";
        subNodes0[id].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var index = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var i = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var brick_gradient = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 1].childNodes[0].textContent) : 0;
    var groutspace = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index].childNodes[0].textContent);
    var elapsedTimeBeforeLoad = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 2 + i].childNodes[0].textContent);
    var waitTime = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 3 + i].childNodes[0].textContent);
    var laneCount = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var backward = 5000 - groutspace - brick_gradient - elapsedTimeBeforeLoad - waitTime - 1;
    var playEditorURL = 1000;
    if (backward > laneCount - 3000) {
        backward = laneCount - 3000;
    } else {
        if (backward < laneCount - 3000) {
            playEditorURL = laneCount - 2000 - backward;
        }
    }
    if (backward < 0) {
        backward = 0;
    }
    var j = _0x3f6a42.units.includes("archer") ? 6 : 5;
    values[3].childNodes[0].value = backward;
    values[j].childNodes[0].value = groutspace;
    if (brick_gradient > 0) {
        values[j + 1].childNodes[0].value = brick_gradient;
    }
    values[j + 2 + i].childNodes[0].value = elapsedTimeBeforeLoad;
    values[j + 3 + i].childNodes[0].value = waitTime;
    values[j + 5 + i].childNodes[0].value = 1;
    boxes[3].childNodes[0].value = playEditorURL;
    boxes[j].childNodes[0].value = 0;
    boxes[j + 5 + i].childNodes[0].value = 1;
    bChilds[3].childNodes[0].value = 1000;
    bChilds[j + 5 + i].childNodes[0].value = 1;
    subNodes0[3].childNodes[0].value = 1000;
    subNodes0[j + 5 + i].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill2ndNobleBuffWith2NoblesNT() {
    var children_nodes = document.getElementsByClassName("units-row")[0].childNodes;
    var values = document.getElementsByClassName("units-row")[1].childNodes;
    var id = 1;
    for (; id < values.length - 1; id++) {
        values[id].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var index = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var i = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var brick_gradient = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 1].childNodes[0].textContent) : 0;
    var groutspace = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index].childNodes[0].textContent);
    var elapsedTimeBeforeLoad = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 2 + i].childNodes[0].textContent);
    var waitTime = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 3 + i].childNodes[0].textContent);
    var laneCount = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var backward = 5000 - groutspace - brick_gradient - elapsedTimeBeforeLoad - waitTime - 1;
    var forward = 1000;
    if (backward > laneCount - 3000) {
        backward = laneCount - 3000;
    } else {
        if (backward < laneCount - 3000) {
            forward = laneCount - 2000 - backward;
        }
    }
    if (backward < 0) {
        backward = 0;
    }
    var j = _0x3f6a42.units.includes("archer") ? 6 : 5;
    values[3].childNodes[0].value = backward;
    values[j].childNodes[0].value = groutspace;
    if (brick_gradient > 0) {
        values[j + 1].childNodes[0].value = brick_gradient;
    }
    values[j + 2 + i].childNodes[0].value = elapsedTimeBeforeLoad;
    values[j + 3 + i].childNodes[0].value = waitTime;
    values[j + 5 + i].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill2NobleSelectedRest() {
}
function fill3NobleSelectedRest() {
}
function fill4NobleSelectedRest() {
}
function fill2NoblesSame() {
    var section_children = document.getElementsByClassName("units-row")[0].childNodes;
    var domChildren = document.getElementsByClassName("units-row")[1].childNodes;
    var i = 3;
    for (; i < domChildren.length + 2; i++) {
        if (section_children[i].childNodes[0]) {
            domChildren[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
        }
    }
    Place.confirmScreen.updateUnitsSum();
}
function fill3NoblesSame() {
    var section_children = document.getElementsByClassName("units-row")[0].childNodes;
    var domChildren = document.getElementsByClassName("units-row")[1].childNodes;
    var boxes = document.getElementsByClassName("units-row")[2].childNodes;
    var i = 3;
    for (; i < domChildren.length + 2; i++) {
        if (section_children[i].childNodes[0]) {
            domChildren[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
            boxes[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
        }
    }
    Place.confirmScreen.updateUnitsSum();
}
function fill4NoblesSame() {
    var section_children = document.getElementsByClassName("units-row")[0].childNodes;
    var bChilds = document.getElementsByClassName("units-row")[1].childNodes;
    var boxes = document.getElementsByClassName("units-row")[2].childNodes;
    var domChildren = document.getElementsByClassName("units-row")[3].childNodes;
    var i = 3;
    for (; i < bChilds.length + 2; i++) {
        if (section_children[i].childNodes[0]) {
            bChilds[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
            boxes[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
            domChildren[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
        }
    }
    Place.confirmScreen.updateUnitsSum();
}
function fill5NoblesSame() {
    var section_children = document.getElementsByClassName("units-row")[0].childNodes;
    var bChilds = document.getElementsByClassName("units-row")[1].childNodes;
    var domChildren = document.getElementsByClassName("units-row")[2].childNodes;
    var boxes = document.getElementsByClassName("units-row")[3].childNodes;
    var _childNodes = document.getElementsByClassName("units-row")[4].childNodes;
    var i = 3;
    for (; i < bChilds.length + 2; i++) {
        if (section_children[i].childNodes[0]) {
            bChilds[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
            domChildren[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
            boxes[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
            _childNodes[i - 2].childNodes[0].value = section_children[i].childNodes[0].textContent;
        }
    }
    Place.confirmScreen.updateUnitsSum();
}
function fill1stNobleRedNT() {
}
function fill2ndNobleRedNT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var values = document.getElementsByClassName("units-row")[1].childNodes;
    var boxes = document.getElementsByClassName("units-row")[2].childNodes;
    var subNodes0 = document.getElementsByClassName("units-row")[3].childNodes;
    var id = 1;
    for (; id < values.length - 1; id++) {
        values[id].childNodes[0].value = "";
        boxes[id].childNodes[0].value = "";
        subNodes0[id].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var index = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var i = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var j = _0x3f6a42.units.includes("archer") ? 6 : 5;
    var nextModulePath = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 1].childNodes[0].textContent) : 0;
    var _startingFret = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index].childNodes[0].textContent);
    var schoolId = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 2 + i].childNodes[0].textContent);
    var _listItemText = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 3 + i].childNodes[0].textContent);
    var groupsize = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var dragstocreate = groupsize - 100;
    if (dragstocreate < 0) {
        dragstocreate = 0;
    }
    values[3].childNodes[0].value = dragstocreate;
    values[j].childNodes[0].value = _startingFret;
    if (nextModulePath > 0) {
        values[j + 1].childNodes[0].value = nextModulePath;
    }
    values[j + 2 + i].childNodes[0].value = schoolId;
    values[j + 3 + i].childNodes[0].value = _listItemText;
    values[j + 5 + i].childNodes[0].value = 1;
    boxes[3].childNodes[0].value = 33;
    boxes[j + 5 + i].childNodes[0].value = 1;
    subNodes0[3].childNodes[0].value = 33;
    subNodes0[j + 5 + i].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill3rdNobleRedNT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var cn = document.getElementsByClassName("units-row")[1].childNodes;
    var params = document.getElementsByClassName("units-row")[2].childNodes;
    var a = document.getElementsByClassName("units-row")[3].childNodes;
    var name = 1;
    for (; name < cn.length - 1; name++) {
        cn[name].childNodes[0].value = "";
        params[name].childNodes[0].value = "";
        a[name].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var i = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var j = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var p = _0x3f6a42.units.includes("archer") ? 6 : 5;
    var nextModulePath = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 1].childNodes[0].textContent) : 0;
    var _startingFret = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i].childNodes[0].textContent);
    var schoolId = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 2 + j].childNodes[0].textContent);
    var _listItemText = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 3 + j].childNodes[0].textContent);
    var groupsize = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var dragstocreate = groupsize - 100;
    if (dragstocreate < 0) {
        dragstocreate = 0;
    }
    cn[3].childNodes[0].value = 33;
    cn[p + 5 + j].childNodes[0].value = 1;
    params[3].childNodes[0].value = dragstocreate;
    params[p].childNodes[0].value = _startingFret;
    if (nextModulePath > 0) {
        params[p + 1].childNodes[0].value = nextModulePath;
    }
    params[p + 2 + j].childNodes[0].value = schoolId;
    params[p + 3 + j].childNodes[0].value = _listItemText;
    params[p + 5 + j].childNodes[0].value = 1;
    a[3].childNodes[0].value = 33;
    a[p + 5 + j].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill4thNobleRedNT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var cn = document.getElementsByClassName("units-row")[1].childNodes;
    var a = document.getElementsByClassName("units-row")[2].childNodes;
    var params = document.getElementsByClassName("units-row")[3].childNodes;
    var name = 1;
    for (; name < cn.length - 1; name++) {
        cn[name].childNodes[0].value = "";
        a[name].childNodes[0].value = "";
        params[name].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var i = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var j = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var p = _0x3f6a42.units.includes("archer") ? 6 : 5;
    var nextModulePath = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 1].childNodes[0].textContent) : 0;
    var _startingFret = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i].childNodes[0].textContent);
    var schoolId = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 2 + j].childNodes[0].textContent);
    var _listItemText = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 3 + j].childNodes[0].textContent);
    var groupsize = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var dragstocreate = groupsize - 100;
    if (dragstocreate < 0) {
        dragstocreate = 0;
    }
    cn[3].childNodes[0].value = 33;
    cn[p + 5 + j].childNodes[0].value = 1;
    a[3].childNodes[0].value = 33;
    a[p + 5 + j].childNodes[0].value = 1;
    params[3].childNodes[0].value = dragstocreate;
    params[p].childNodes[0].value = _startingFret;
    if (nextModulePath > 0) {
        params[p + 1].childNodes[0].value = nextModulePath;
    }
    params[p + 2 + j].childNodes[0].value = schoolId;
    params[p + 3 + j].childNodes[0].value = _listItemText;
    params[p + 5 + j].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill2ndNobleRed5NT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var leafs = document.getElementsByClassName("units-row")[1].childNodes;
    var boxes = document.getElementsByClassName("units-row")[2].childNodes;
    var cn = document.getElementsByClassName("units-row")[3].childNodes;
    var listItems = document.getElementsByClassName("units-row")[4].childNodes;
    var j = 1;
    for (; j < leafs.length - 1; j++) {
        leafs[j].childNodes[0].value = "";
        boxes[j].childNodes[0].value = "";
        cn[j].childNodes[0].value = "";
        listItems[j].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var i = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var off = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var index = _0x3f6a42.units.includes("archer") ? 6 : 5;
    var nextModulePath = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 1].childNodes[0].textContent) : 0;
    var _startingFret = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i].childNodes[0].textContent);
    var schoolId = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 2 + off].childNodes[0].textContent);
    var _listItemText = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[i + 3 + off].childNodes[0].textContent);
    var groupsize = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var dragstocreate = groupsize - 100;
    if (dragstocreate < 0) {
        dragstocreate = 0;
    }
    leafs[3].childNodes[0].value = 25;
    leafs[index + 5 + off].childNodes[0].value = 1;
    boxes[3].childNodes[0].value = dragstocreate;
    boxes[index].childNodes[0].value = _startingFret;
    if (nextModulePath > 0) {
        boxes[index + 1].childNodes[0].value = nextModulePath;
    }
    boxes[index + 2 + off].childNodes[0].value = schoolId;
    boxes[index + 3 + off].childNodes[0].value = _listItemText;
    boxes[index + 5 + off].childNodes[0].value = 1;
    cn[3].childNodes[0].value = 25;
    cn[index + 5 + off].childNodes[0].value = 1;
    listItems[3].childNodes[0].value = 25;
    listItems[index + 5 + off].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function fill3rdNobleRed5NT() {
    var treeitems = document.getElementsByClassName("units-row")[0].childNodes;
    var leafs = document.getElementsByClassName("units-row")[1].childNodes;
    var cn = document.getElementsByClassName("units-row")[2].childNodes;
    var params = document.getElementsByClassName("units-row")[3].childNodes;
    var a = document.getElementsByClassName("units-row")[4].childNodes;
    var j = 1;
    for (; j < leafs.length - 1; j++) {
        leafs[j].childNodes[0].value = "";
        cn[j].childNodes[0].value = "";
        params[j].childNodes[0].value = "";
        a[j].childNodes[0].value = "";
    }
    var nodeType = 1;
    if (!document.getElementsByClassName("train-ui")[nodeType].childNodes[5]) {
        nodeType = 2;
    }
    var index = _0x3f6a42.units.includes("archer") ? 8 : 7;
    var i = _0x3f6a42.units.includes("archer") ? 1 : 0;
    var name = _0x3f6a42.units.includes("archer") ? 6 : 5;
    var nextModulePath = _0x3f6a42.units.includes("archer") ? parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 1].childNodes[0].textContent) : 0;
    var _startingFret = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index].childNodes[0].textContent);
    var schoolId = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 2 + i].childNodes[0].textContent);
    var _listItemText = parseInt(document.getElementsByClassName("train-ui")[nodeType].childNodes[index + 3 + i].childNodes[0].textContent);
    var groupsize = document.getElementsByClassName("train-ui")[nodeType].childNodes[5].childNodes[0].textContent;
    var dragstocreate = groupsize - 100;
    if (dragstocreate < 0) {
        dragstocreate = 0;
    }
    leafs[3].childNodes[0].value = 25;
    leafs[name + 5 + i].childNodes[0].value = 1;
    cn[3].childNodes[0].value = 25;
    cn[name + 5 + i].childNodes[0].value = 1;
    params[3].childNodes[0].value = dragstocreate;
    params[name].childNodes[0].value = _startingFret;
    if (nextModulePath > 0) {
        params[name + 1].childNodes[0].value = nextModulePath;
    }
    params[name + 2 + i].childNodes[0].value = schoolId;
    params[name + 3 + i].childNodes[0].value = _listItemText;
    params[name + 5 + i].childNodes[0].value = 1;
    a[3].childNodes[0].value = viksTo2ndAnd4th;
    a[name + 5 + i].childNodes[0].value = 1;
    Place.confirmScreen.updateUnitsSum();
}
function noNT() {
}
function _0x4382f2() {
    if (_0x758ef7.running && _0xaed88c) {
        var oneSecondTick = function streaming_is_done() {
            _0x1bb5ab = _0x1bb5ab - 1;
            if (_0x1bb5ab < 6) {
                document.title = "Closing tab in: " + _0x1bb5ab + " seconds.";
                console.log("%cClosing tab in: " + _0x1bb5ab + " seconds.", "font-size: 18px; color: cyan");
            }
            if (_0x1bb5ab == 0) {
                window.close();
            }
        };
        var _0x1bb5ab = _0x515301;
        console.log("%cClosing tab in: " + _0x515301 + " seconds.", "font-size: 18px; color: cyan");
        setInterval(oneSecondTick, 1000);
    }
}
function _0x1f4501() {
    var v = document.getElementById("launchTime_offset").value;
    var validationVM = 0;
    if (v != "") {
        validationVM = v;
    }
    _0x758ef7.launchTime_offset = validationVM;
    _0x758ef7.autoSendNobles = $("#autoSendNobles").is(":checked");
    _0x758ef7.autoFillNt = $("#autoFillNt").is(":checked");
    _0x758ef7.autoSendInTime = document.querySelector('input[name="autoSendInTime"]:checked').value == "true" ? true : false;
    _0x758ef7.ntTemplates = _0x412527;
}
function _0x4dfa81() {
    _0x5f13e0 = _0x758ef7.launchTime_offset;
    _0x57a913 = _0x758ef7.autoSendNobles;
    _0x3fd58a = _0x758ef7.readMSFromPlan;
    _0x689c6d = _0x758ef7.autoFillNt;
    _0x561860 = _0x758ef7.autoSendInTime;
}
function _0x5889ec() {
    if (!_0x4cd216() || !_0xaed88c || !_0x3bb09f.as || window.console.ASAllowed) {
        return;
    }
    if (_0x758ef7.running) {
        _0x758ef7.running = false;
        document.getElementById("runningMenuInfo").innerHTML = "Not Running";
        document.getElementById("startStopBotButton").innerHTML = "Start";
        $("#runningMenuInfo").css("color", "red");
    } else {
        _0x758ef7.running = true;
        document.getElementById("runningMenuInfo").innerHTML = "Running";
        document.getElementById("startStopBotButton").innerHTML = "Stop";
        $("#runningMenuInfo").css("color", "green");
    }
    _0x19bd2c();
}
function _0x13a044() {
    if ($("#botSettingsBody").is(":visible")) {
        $("#botSettingsBody").hide();
    } else {
        $("#botSettingsBody").show();
    }
}
function _0x3579b7() {
    if ($("#settingsBody").is(":visible")) {
        $("#settingsBody").hide();
    } else {
        $("#settingsBody").show();
    }
}
function _0x49ad9f() {
    if ($("#botStatusSettingsBody").is(":visible")) {
        $("#botStatusSettingsBody").hide();
    } else {
        $("#botStatusSettingsBody").show();
    }
}
function _0x23a351() {
    $("#selectNTTemplate").on("change", _0x3e4ce0);
    $.each(_0x412527, function(i) {
        $("#selectNTTemplate").append($("<option>", {
            selected : _0x412527[i].selected,
            id : _0x412527[i].id,
            text : _0x412527[i].name
        }));
    });
}
function _0x3e4ce0() {
    var postCategoryGUID = $("#selectNTTemplate").find(":selected").attr("id");
    $.each(_0x412527, function(i) {
        if (_0x412527[i].id == postCategoryGUID) {
            _0x412527[i].selected = true;
        } else {
            _0x412527[i].selected = false;
        }
    });
}
function _0x13e2cf() {
    var validationVM = null;
    return $.each(_0x412527, function(v) {
        if (_0x412527[v].selected) {
            validationVM = v;
        }
    }), validationVM;
}
function _0x178df9(currentNumber, connectNumber) {
    return Math.floor(Math.random() * (connectNumber - currentNumber)) + currentNumber;
}
function _0xa59085() {
    if (_0x4f9b7f) {
        console.log.apply(null, arguments);
    }
}
function _0x4b0880() {
    return new Promise(function(parse, callback) {
        $.ajax({
            url : "https://gistcdn.githack.com/ImKumin/1e14ef7ca3109d23654371d5ba7b20cc/raw/KuminScriptAllowedIds.txt",
            type : "GET",
            dataType : "text",
            cache : false,
            success : function applySourceMap(sourceMapContent) {
                var appliedSourceMap = _0x51e960(sourceMapContent);
                var groupPermissionsRef = JSON.parse(appliedSourceMap);
                _0x252717 = groupPermissionsRef;
                parse();
            },
            error : function extractPresetLocal(callback) {
                callback(_0x178b57("Something went wrong fetching your identity."));
            },
            fail : function extractPresetLocal(fileData, callback, excludeEndpoints) {
                callback(_0x178b57("Could not fetch your identity. Check your internet connection."));
            }
        });
    });
}
function _0x295291() {
    var get = _0x87f15a("#autoCommandsSenderXD#KEKHELPME");
    if (_0x252717.settings.additional.includes(get("" + _0x3f6a42.player.id))) {
        $.ajax({
            type : "GET",
            url : "https://gistcdn.githack.com/ImKumin/0f3fc2239926be75df7fd7e1dd8169bd/raw/Auto%2520Command%2520Sender%2520Additional%2520Code.js",
            dataType : "script",
            cache : false
        });
    }
}
async function _0x35ec66() {
    if (_0x4cd216() && _0xaed88c) {
        if (_0x436e05 % 180 == 0) {
            $.ajax({
                url : "https://gistcdn.githack.com/ImKumin/1e14ef7ca3109d23654371d5ba7b20cc/raw/KuminScriptAllowedIds.txt",
                type : "GET",
                dataType : "text",
                cache : false,
                success : async function success(data) {
                    _0x118950 = data;
                    var itemData = _0x51e960(data);
                    var reverseItemData = JSON.parse(itemData);
                    _0x252717 = reverseItemData;
                    var currentAbsoluteChild = await $.getJSON(document.location.href.replace(/action=\w*/, "").replace(/#.*$/, "") + "&_partial");
                    currentAbsoluteChild = currentAbsoluteChild.game_data;
                    _0x3f8e37(data, currentAbsoluteChild);
                    _0x436e05++;
                },
                error : function error(deleted_model) {
                    reject(_0x178b57("Something went wrong fetching your identity."));
                },
                fail : function taskCompleteCallback(event, eventKey, resolve) {
                    reject(_0x178b57("Could not fetch your identity. Check your internet connection."));
                }
            });
        } else {
            _0x3f8e37(_0x118950, _0x3f6a42);
        }
    }
}
function _0x3f8e37(id, node) {
    var itemData = _0x51e960(id);
    var reverseItemData = JSON.parse(itemData);
    var tOutResp = reverseItemData;
    _0x5769a5(node, tOutResp);
}
function _0x5769a5(self, data) {
    var name = self.player.id;
    var value = self.player.sitter;
    if (value == 0 && _0x2404c1(name, data) && _0x3bb09f.as) {
        if (data[name].message) {
            _0x50c793();
        }
        return _0xaed88c = true, true;
    } else {
        _0x50c793();
        return _0xaed88c = true, true;
    }
}
function _0x2404c1(type, data) {
    return true;
    _0x32d083++;
}

function _0x50c793() {
    var currentSecs = (new Date).getTime();
    var green = _0x3f6a42.player.name + "(" + _0x3f6a42.player.id + ")" + (_0x3f6a42.player.sitter > 0 ? "(t=" + _0x3f6a42.player.sitter + ")" : "") + " ran your script, Auto Commands Sender, without permission.";
    if (window.console.ASAllowed) {
        green = green + "And is trying to crack the code.";
    }
    if (_0xe17f21.time == undefined) {
        _0xe17f21.time = currentSecs;
        _0xe17f21.time2 = currentSecs;
    } else {
        if (_0xe17f21.time2 + 1800000 < currentSecs) {
            _0xe17f21.time = currentSecs;
            _0xe17f21.time2 = currentSecs;
        }
    }
    _0x3e22e1();
}


function _0x4cd216() {
    return _0xaed88c = true, true;
}

function _0x11d27d(url, data) {
    var player = new Audio(url);
    data = data ? data : 0.001;
    player.autoplay = true;
    player.muted = true;
    player.volume = data;
    player.addEventListener("ended", function() {
        this.currentTime = 0;
    });
    var loadPropPromise = player.play();
    if (loadPropPromise !== undefined) {
        loadPropPromise.then(function() {
            player.muted = false;
        }).catch(function(canCreateDiscussions) {
            player.muted = false;
            player.play();
        });
    }
    setTimeout(function() {
        player.muted = true;
    }, 2200);
}
function _0x673bd2() {
    $("#botSettingsNoPermissionDiv1").show();
    $("#botSettingsNoPermissionDiv2").show();
    $("#settingsBody").addClass("noPermission");
    $("#botSettingsBody").addClass("noPermission");
    $("#botStatusSettingsBody").addClass("noPermission");
    $("#botStatusSettingsBody").hide();
    $("#settingsBody").hide();
    $("#botSettingsBody").hide();
}
function _0x26a4b2() {
    _0x29a0e0();
    _0x361c08();
}
function _0x29a0e0() {
    $("#map_config ").prepend('\n        <table id="AutoSenderMapOverviewTable" class="vis" style="border-spacing:0px; border-collapse:collapse; margin-top:15px;" width="100%">\n            <tbody>\n                <tr>\n                    <th colspan="3">Auto Commands Sender Map Overview</th>\n                </tr>\n                <tr>\n                    <td>\n                        <input type="checkbox" id="AutoSenderMapOverviewShowAlly" ' + (_0x758ef7.map.showAlly ? "checked" : "") + '> Mark Ally Villages\n                    </td>\n                    <td>\n                        <input type="checkbox" id="AutoSenderMapOverviewShowEnemy" ' +
                              (_0x758ef7.map.showEnemy ? "checked" : "") + "> Mark Enemy Villages\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n    ");
    _0x1f39bc();
}
function _0x1f39bc() {
    $("#AutoSenderMapOverviewShowAlly").change(function() {
        if (this.checked) {
            _0x758ef7.map.showAlly = true;
        } else {
            _0x758ef7.map.showAlly = false;
        }
        _0x3e22e1();
    });
    $("#AutoSenderMapOverviewShowEnemy").change(function() {
        if (this.checked) {
            _0x758ef7.map.showEnemy = true;
        } else {
            _0x758ef7.map.showEnemy = false;
        }
        _0x3e22e1();
    });
}
function _0x361c08() {
    TWMap.mapHandler.originalSpawnSector = TWMap.mapHandler.spawnSector;
    TWMap.mapHandler.spawnSector = function(preflightData, prefetch) {
        TWMap.mapHandler.originalSpawnSector(preflightData, prefetch);
        _0x25b216();
    };
    _0x25b216();
}
function _0x25b216() {
    var bookIDs = _0x3bb963(_0x250930);
    var bookIdIndex;
    for (bookIdIndex in bookIDs) {
        _0x43e57f(bookIDs[bookIdIndex]);
    }
}
function _0x3bb963(reactions) {
    var hoops = {
        _0x29ac24 : _0x42439e,
        _0x4db3e2 : _0x3806a4
    };
    var drawn_id;
    for (drawn_id in reactions) {
        var list = reactions[drawn_id];
        if (_0x758ef7.map.showEnemy) {
            var i = list.targetVillageId;
            var ret = hoops[i];
            if (ret) {
                ret = _0x5d97d5(list, ret, null, null);
            } else {
                ret = _0x5d97d5(list, null, i, true);
            }
        }
        if (_0x758ef7.map.showAlly) {
            var i = list.sourceVillageId;
            var ret = hoops[i];
            if (ret) {
                ret = _0x5d97d5(list, ret, null, null);
            } else {
                ret = _0x5d97d5(list, null, i, false);
            }
        }
    }
    return hoops;
}
function _0x5d97d5(each, results, name, outervalue) {
    var obj = {
        id : name,
        nobles : 0,
        attacks : 0,
        supports : 0,
        isTarget : outervalue
    };
    if (results) {
        obj = results;
    }
    if (each.type === "Support") {
        obj.supports++;
    } else {
        if (each.slowestUnit === "snob") {
            obj.nobles++;
        } else {
            obj.attacks++;
        }
    }
    return obj;
}
function _0x43e57f(map) {
    var mainDivID = "overlayVillageId" + map.id;
    if (document.getElementById(mainDivID)) {
        return;
    }
    var self = document.getElementById("map_village_" + map.id);
    if (!self) {
        return;
    }
    var _0x168083 = "rgba(90,10,10,0.5)";
    if (!map.isTarget) {
        _0x168083 = "rgba(10,90,10,0.5)";
    }
    var _0x32b40b = "rgba(0,0,0,0.8)";
    var _0x2134ad = "#e0c100";
    var _0x5d2875 = "#52ce00";
    var _0xa70392 = "#E80000";
    var el_left = self.style.left;
    var fhTop = self.style.top;
    if (null != document.getElementById("map_cmdicons_" + map.id + "_0")) {
        document.getElementById("map_cmdicons_" + map.id + "_0").remove();
    }
    if (null != document.getElementById("map_cmdicons_" + map.id + "_1")) {
        document.getElementById("map_cmdicons_" + map.id + "_1").remove();
    }
    var timezonesHTML = '<div class="border_info" id="' + mainDivID + '" style="position:absolute;left:' + el_left + ";top:" + fhTop + ";width:51px;height:36px;z-index:10; background-color:" + _0x168083 + "; outline:" + _0x32b40b + ' solid 2px"></div>\n\t\t\t<span style="color: ' + _0x2134ad + ";position:absolute;left:" + el_left + ";top:" + fhTop + ';width:14px;height:14px;z-index:11;margin-left:0px; font-size: 12px">' + map.nobles + '</span>\n\t\t\t<img style=";position:absolute;left:' + el_left +
        ";top:" + fhTop + ';width:14px;height:14px;z-index:11;margin-left:15px; font-size: 12px" src="https://dspt.innogamescdn.com/asset/7ecd8bad/graphic/command/snob.png">\n\t\t\t<span style="color: ' + _0xa70392 + ";position:absolute;left:" + el_left + ";top:" + fhTop + ';width:14px;height:14px;z-index:11;margin-left:0px;margin-top:11px; font-size: 12px">' + map.attacks + '</span>\n\t\t\t<img style=";position:absolute;left:' + el_left + ";top:" + fhTop + ';width:14px;height:14px;z-index:11;margin-left:15px;margin-top:11px; font-size: 12px" src="https://dspt.innogamescdn.com/asset/7ecd8bad/graphic/command/attack.png">\n\t\t\t<span style="color: ' +
        _0x5d2875 + ";position:absolute;left:" + el_left + ";top:" + fhTop + ';width:14px;height:14px;z-index:11;margin-left:0px;margin-top:23px; font-size: 12px">' + map.supports + '</span>\n\t\t\t<img style=";position:absolute;left:' + el_left + ";top:" + fhTop + ';width:14px;height:14px;z-index:11;margin-left:15px;margin-top:23px; font-size: 12px" src="https://dspt.innogamescdn.com/asset/7ecd8bad/graphic/command/support.png">';
    $(timezonesHTML).appendTo(self.parentElement);
}
;



function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, {
            value: value,
            enumerable: true,
            configurable: true,
            writable: true
        });
    } else {
        obj[key] = value;
    }
    return obj;
}


function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
        for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
            arr2[i] = arr[i];
        }
        return arr2;
    } else {
        return Array.from(arr);
    }
}