this.obsidianScripts = {
    ScriptAC: {
        addCommand: function (_0x2afe93) {
            var _0x69f938 = $(".units-row").length;
            var _0x42daae = $(".train-ui td");
            var _0x12dcb5 = "<tr class=\"units-row\"><th class=\"train-name\" style=\"text-align: center\"><span>Ataque #" + (_0x69f938 + 1) + "</span><img src=\"/graphic/delete_14.png\" class=\"float_right\" style=\"cursor: pointer\" onclick=\"obsidianScripts.ScriptAC.deleteTrain('.train-container .container')\"></th>";
            game_data.units.slice(0, -1).forEach((_0x493c45, _0x1820c0) => _0x12dcb5 += "<td><input type=\"number\" data-unit=\"" + _0x493c45 + "\" name=\"train[2][" + _0x493c45 + "]\" style=\"width: 40px" + (_0x42daae[_0x1820c0].style.opacity ? "; opacity: 0.3;" : ";") + "\"></td>");
            _0x12dcb5 += "</tr>";
            return _0x69f938 < 5 && $(".train-container .container").append(_0x12dcb5);
        },
        calculateTimes: function (_0x54d2d0, _0x1d74d6, _0xe480b5, _0x5b62bc, _0x56b465, _0x2e0391, _0x5bf5a2, _0x21bb85, _0x421b0b) {
            var _0x34c45b = obsidianScripts.calculateDistance(_0x5bf5a2, _0x21bb85);
            var _0x37ddd5 = _0x34c45b * _0x421b0b * 60000;
            var _0x45cb8b = _0xe480b5 != 2 ? _0x2e0391 : _0x5b62bc + Math.random() * (_0x56b465 - _0x5b62bc);
            var _0x35a7f3 = new Date(Math.round((_0x54d2d0 - _0x37ddd5) / 1000) * 1000 + _0x45cb8b);
            return _0x35a7f3 > _0x1d74d6 && _0x34c45b > 0 && _0x35a7f3;
        },
        commandsStatus: function (_0x2d18ab) {
            $(_0x2d18ab).each(function (_0x124b00) {
                var _0x596ad9 = $("#" + this.id);
                var _0x1b5d5f = $(this.cells);
                var _0x5e10fc = _0x1b5d5f[1].textContent;
                var _0x1616f = _0x1b5d5f.slice(-4)[0].textContent;
                var _0x449b84 = new Date(obsidianScripts.convertToValidFormat(_0x1616f)) - Timing.getCurrentServerTime();
                if (_0x449b84 > 0) {
                    return _0x596ad9.children().slice(-3)[0].firstChild.textContent = obsidianScripts.secondsToHms(_0x449b84 / 1000);
                } else {
                    return _0x596ad9.remove() && !obsidianScripts.ScriptAC.saveScript() && obsidianScripts.ScriptAC.logScript("AVISO: A coordenada " + _0x5e10fc + " expirou e foi deletado!", "alert");
                }
            });
            return !undefined;
        },
        duplicateCoordinate: function (_0x206461) {
            var _0x14290b = {};
            $(".registered tr").each((_0x1ba40a, _0x39eecf) => _0x14290b[_0x39eecf.cells[1].textContent] = true);
            return $(".combinations-found tr").each(function (_0x5c261e) {
                if (typeof _0x14290b[this.cells[1].textContent] == "boolean") {
                    return this.style.opacity = "0.5";
                } else {
                    return this.style.opacity = "";
                }
            });
        },
        deleteTrain: function (_0x1bd4f0) {
            return document.querySelector(_0x1bd4f0).lastElementChild.lastChild.remove();
        },
        deleteCommand: function (_0x4b9ab3) {
            var _0x24ef9b = _0x4b9ab3.closest("tr");
            _0x24ef9b.remove();
            this.infoScript(_0x24ef9b, false);
            this.duplicateCoordinate();
            return this.saveScript();
        },
        formatDateTime: function (_0x472baf) {
            return ("" + _0x472baf.getDate()).padStart(2, "0") + "/" + ("" + (_0x472baf.getMonth() + 1)).padStart(2, "0") + "/" + _0x472baf.getFullYear() + " " + ("" + _0x472baf.getHours()).padStart(2, "0") + ":" + ("" + _0x472baf.getMinutes()).padStart(2, "0") + ":" + ("" + _0x472baf.getSeconds()).padStart(2, "0") + ":" + ("" + _0x472baf.getMilliseconds()).padStart(3, "0");
        },
        generateID: function (_0x42a579) {
            var _0x1af0cc = {
                length: _0x42a579
            };
            return Array.from(_0x1af0cc).reduce(_0x3008d3 => _0x3008d3 += "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".charAt(Math.random() * 62), "");
        },
        init: function (_0x3f59ed) {
            var _0x1bdb71 = JSON.parse(localStorage.getItem("scriptUpdatedAC"));
            var _0x4a1248 = _0x1bdb71 && (_0x1bdb71.soundChecked && $("#sound").prop("checked", true), _0x1bdb71.innerHTML);
            return $(".registered").html(_0x4a1248) && this.commandsStatus(_0x4a1248) && this.RequestXML(_0x4a1248) && this.infoScript(_0x4a1248, true) && Timing.tickHandlers.timers.init();
        },
        infoScript: function (_0x2fc05f, _0x245e07) {
            $(_0x2fc05f).each(function (_0x4a7167) {
                var _0x29818c = $(".info-targets tr");
                var _0x277d1c = $("#" + this.id);
                var _0x99f9f3 = $(this.cells);
                var _0x2a5543 = _0x99f9f3[2].textContent;
                var _0x9400fe = $(".info-targets tr:contains(" + _0x2a5543 + ")");
                var _0x2f429f = Number(_0x99f9f3.slice(-5)[0].textContent);
                var _0x43c2ca = _0x99f9f3.slice(-2)[0].textContent != "ATACAR" ? _0x245e07 ? [0, 1, _0x2f429f] : [0, -1, -_0x2f429f] : _0x245e07 ? [1, 0, _0x2f429f] : [-1, 0, -_0x2f429f];
                if (!_0x277d1c.length && _0x245e07) {
                    return undefined;
                } else if (!_0x29818c.filter((_0xe8eb0e, _0x3b97b6) => _0x3b97b6.cells[1].textContent == _0x2a5543).length) {
                    return $(".info-targets").append("<tr><td align=\"center\">" + (_0x29818c.length + 1) + "</td><td style=\"text-align: center; width: 70px;\">" + _0x99f9f3[2].innerHTML + "</td><td class=\"td-targets\"><span style=\"color: #c14848;\">" + _0x43c2ca[0] + "</span>&nbsp;ATAQUE(S)</td><td class=\"td-targets\"><span style=\"color: #53853b;\">" + _0x43c2ca[1] + "</span>&nbsp;APOIO(S)</td><td class=\"td-targets\"><span style=\"color: #6d6db3\">" + _0x43c2ca[2] + "</span>&nbsp;NOBRE(S)</td></tr>");
                } else {
                    return _0x9400fe.children().slice(2).each((_0x2c0937, _0x46e748) => _0x46e748.innerHTML = _0x46e748.innerHTML.replace(/(?<=>)\d+(?=<)/, Number(_0x46e748.textContent.charAt(0)) + _0x43c2ca[_0x2c0937])).filter((_0x3532b5, _0x360575) => _0x360575.textContent.charAt(0) > 0).length || _0x9400fe.remove() && _0x29818c.filter((_0x1473bd, _0x3a3d04) => _0x3a3d04.cells[1].textContent != _0x2a5543).each((_0x85f3ba, _0xbbb7ba) => _0xbbb7ba.cells[0].textContent = _0x85f3ba + 1);
                }
            });
            return !undefined;
        },
        logScript: function (_0xb65933, _0x168626) {
            var _0x431c28 = document.querySelector("#logScript");
            var _0x3f030e = document.querySelector("#time").textContent;
            var _0x1941ff = document.createElement("span");
            var _0x6d460d = document.createElement("br");
            var _0x2bc354 = _0x168626 != "error" ? _0x168626 != "alert" ? ["#53853b", "✅"] : ["#8b8b44", "⚠️"] : ["#c14848", "❌"];
            _0x1941ff.style.color = _0x2bc354[0];
            _0x1941ff.textContent = _0x2bc354[1] + " " + _0x3f030e + " - " + _0xb65933;
            _0x431c28.append(_0x1941ff);
            _0x431c28.append(_0x6d460d);
            return _0x431c28.scrollTop = _0x431c28.scrollHeight;
        },
        msScript: function (_0x463c63) {
            var _0x305023 = $("#ms-Script")[0].value;
            var _0x10e1b8 = $("#ScriptAC #fixed").closest("td");
            var _0x19fbb4 = $("#ScriptAC").find("#min, #max").closest("td");
            if (_0x305023 != 2) {
                return _0x10e1b8.css("display", "table-cell") && _0x19fbb4.css("display", "none");
            } else {
                return _0x10e1b8.css("display", "none") && _0x19fbb4.css("display", "table-cell");
            }
        },
        opacityColumn: function (_0xd7908e) {
            var _0x48b787 = _0xd7908e.target.closest("th");
            if (_0xd7908e.target.localName == "img" && !_0x48b787.style.opacity) {
                return _0x48b787.style.opacity = "0.3";
            } else {
                return _0x48b787.style.opacity = "";
            }
        },
        pickUpUnits: function (_0xe0b217) {
            var _0xdf4b1a = _0xe0b217.closest("tr").cells;
            var _0x249763 = $(_0xdf4b1a).find("option:selected")[0].textContent;
            switch (_0x249763) {
                case "AGENDAR":
                    var _0x42181a = $(".registered tr").length;
                    var _0x2fd757 = "<tr id=\"" + this.generateID(9) + "\"><td align=\"center\">" + (_0x42181a + 1) + "</td><td style=\"text-align: center; width: 70px;\">" + _0xdf4b1a[1].innerHTML + "</td><td style=\"text-align: center; width: 70px;\">" + _0xdf4b1a[2].innerHTML + "</td>";
                    var _0x14fc8e = _0xdf4b1a[_0xdf4b1a.length - 4].textContent;
                    var _0x1907a0 = new Date(obsidianScripts.convertToValidFormat(_0x14fc8e)) - Timing.getCurrentServerTime();
                    Array.from(_0xdf4b1a).slice(3, -4).forEach((_0x206153, _0x44372e) => _0x2fd757 += "<td style=\"" + _0x206153.style.cssText + " width: 35px;\">" + _0x206153.textContent + "</td>");
                    _0x2fd757 += "<td style=\"white-space: nowrap\">" + _0x14fc8e + "</td><td style=\"color: #c14848;\"><span class=\"timer\">" + obsidianScripts.secondsToHms(_0x1907a0 / 1000) + "</span></td><td class=\"description\">" + $("select.choose option:selected")[0].textContent + "</td><td class=\"close\" onclick=\"obsidianScripts.ScriptAC.deleteCommand(this)\">X</td></tr>";
                    $(".registered").append(_0x2fd757);
                    this.RequestXML(_0x2fd757);
                    this.infoScript(_0x2fd757, true);
                    this.duplicateCoordinate();
                    this.saveScript();
                    break;
                case "ALTERAR":
                    var _0x2fd757 = [_0xdf4b1a[1].innerHTML, _0xdf4b1a[2].innerHTML, _0xdf4b1a[_0xdf4b1a.length - 4].innerHTML, _0xdf4b1a[_0xdf4b1a.length - 3].innerHTML.replace("class=\"\"", "class=\"timer\"")];
                    Array.from(_0xdf4b1a).slice(3, -4).forEach((_0x47da12, _0x24524f) => $("#ScriptAC [class*=unit-item]").eq(_0x24524f).text(_0x47da12.textContent)[0].style.cssText = _0x47da12.style.cssText);
                    document.querySelectorAll(".vis-train td").forEach((_0xe0db2, _0x2d0bb0) => _0xe0db2.innerHTML = _0x2fd757[_0x2d0bb0]);
                    if ($(".units-row").remove()) {
                        $("#troop_confirm_train").trigger("click");
                    }
                    break;
            }
            ;
            return $(_0xe0b217).closest("tr").remove() && Timing.tickHandlers.timers.init();
        },
        pickUpTrain: function (_0x2e0e82) {
            var _0x1cdd0c = "";
            var _0x2bee75 = $(".vis-train td");
            var _0xcd1db = $(".registered tr").length;
            document.querySelectorAll(".units-row").forEach((_0x3fb424, _0x3a8984) => {
                _0x1cdd0c += "<tr id=\"" + this.generateID(9) + "\"" + (!_0x3a8984 ? " class=\"main-train\"" : " class=\"content-train\"") + "><td align=\"center\">" + (_0xcd1db + _0x3a8984 + 1) + "</td><td style=\"text-align: center; width: 70px;\">" + _0x2bee75[0].innerHTML + "</td><td style=\"text-align: center; width: 70px;\">" + _0x2bee75[1].innerHTML + "</td>";
                Array.from(_0x3fb424.cells).slice(1).forEach((_0x255870, _0x6bd3fd) => _0x1cdd0c += "<td style=\"" + (_0x255870.firstChild.value != 0 ? "color: #C3FFA5;" : "opacity: 0.3;") + " width: 35px;\">" + Number(_0x255870.firstChild.value) + "</td>");
                var _0x31deb1 = _0x2bee75[2].textContent;
                var _0x3d23bd = new Date(obsidianScripts.convertToValidFormat(_0x31deb1)) - Timing.getCurrentServerTime();
                _0x1cdd0c += "<td style=\"white-space: nowrap\">" + _0x31deb1 + "</td><td style=\"color: #c14848\"><span class=\"timer\">" + obsidianScripts.secondsToHms(_0x3d23bd / 1000) + "</span></td><td class=\"description\">TREM " + (_0x3a8984 + 1) + "</td><td class=\"close\" onclick=\"obsidianScripts.ScriptAC.deleteCommand(this)\">X</td></tr>";
            });
            if ($(".units-row").length && _0x1cdd0c.match(/(?<=width: 35px;">)\d+(?=<\/td>)/g).some(_0x38d607 => _0x38d607 > 0)) {
                return $(".registered").append(_0x1cdd0c) && !Timing.tickHandlers.timers.init() && this.RequestXML(_0x1cdd0c) && this.infoScript(_0x1cdd0c, true) && this.duplicateCoordinate() && this.saveScript();
            } else {
                return UI.ErrorMessage("Não existem entradas disponíveis!");
            }
        },
        reorderCommands: function (_0x4dacaf) {
            var _0x561dc7 = [];
            $(".registered tr").each(function (_0x4e4433) {
                var _0x238d50 = $(this.cells).slice(-4)[0].textContent;
                var _0x52fa42 = "<tr id=\"" + this.id + "\"" + (this.className ? " class=\"" + this.className + "\"" : "") + ">" + this.innerHTML.replace(/class=""/, "class=\"timer\"") + "</tr>";
                var _0x2161e4 = {
                    content: _0x52fa42,
                    launchTime: _0x238d50
                };
                _0x561dc7.push(_0x2161e4);
            });
            _0x561dc7.sort((_0x2ce2b0, _0x1fc794) => new Date(obsidianScripts.convertToValidFormat(_0x2ce2b0.launchTime)) - new Date(obsidianScripts.convertToValidFormat(_0x1fc794.launchTime)));
            var _0x236de6 = _0x561dc7.map((_0x4058fe, _0x1878c4) => _0x4058fe.content.replace(/>\d+</, ">" + (_0x1878c4 + 1) + "<")).join("");
            return $(".registered").html(_0x236de6) && !Timing.tickHandlers.timers.init() && this.saveScript();
        },
        RequestPages: function (_0x148126, _0x1cac0f) {
            return $.ajax({
                url: game_data.link_base_pure + "overview_villages&mode=units&action=change_page_size&type=own_home&group=0",
                method: "POST",
                data: "page_size=1000&h=" + game_data.csrf
            }).then(async _0x4554dc => {
                var _0x2b518c = $(_0x4554dc).find(".paged-nav-item");
                var _0xa9779a = [_0x4554dc];
                for (var _0xa30df4 = 0; _0xa30df4 < _0x2b518c.length - 1; _0xa30df4++) {
                    var _0x218422 = await $.get(game_data.link_base_pure + ("overview_villages&mode=units&type=own_home&group=0&page=" + (_0xa30df4 + 1)));
                    _0xa9779a.push(_0x218422);
                }
                ;
                return _0xa9779a.forEach(_0x1cac0f);
            });
        },
        RequestXML: function (_0x31f8c6) {
            return $(_0x31f8c6).each(function (_0x2e7828) {
                var _0x93a371 = $(this.cells);
                var _0x5a892d = this.id;
                var _0x3d2609 = _0x93a371[1].textContent;
                var _0xef9447 = _0x93a371[2].textContent;
                var _0x23c459 = APIUpdated.database[_0x3d2609].villageID;
                var _0x127d08 = obsidianScripts.ScriptAC.generateID(22) + "%3A" + obsidianScripts.ScriptAC.generateID(14) + "&template_id=&source_village=" + _0x23c459;
                var _0x22431c = _0x93a371.slice(3, -4).map((_0x4bf424, _0x4cd23c) => _0x4cd23c.style.color && (_0x127d08 += "&" + game_data.units[_0x4bf424] + "=" + _0x4cd23c.textContent, APIUpdated.units[game_data.units[_0x4bf424]]));
                var _0x299776 = Math.round(obsidianScripts.calculateDistance(_0x3d2609, _0xef9447) * Math.max(..._0x22431c) * 60000 / 1000) * 1000;
                var _0x57f187 = new Date(obsidianScripts.convertToValidFormat(_0x93a371.slice(-4)[0].textContent));
                var _0x48bf60 = _0x93a371.slice(-2)[0].textContent != "ATACAR" ? ["support=Apoio", "Enviar%20apoio"] : ["attack=Ataque", "Enviar%20ataque"];
                var [_0x2c9f72, _0x48c620] = _0xef9447.split("|");
                _0x127d08 += "&x=" + _0x2c9f72 + "&y=" + _0x48c620 + "&target_type=coord&input=" + _0x2c9f72 + "%7C" + _0x48c620 + "&" + _0x48bf60[0];
                return obsidianScripts.ScriptAC.timerCommand(_0x127d08, _0x23c459, _0x5a892d, _0x48bf60[1], _0x3d2609, _0xef9447, _0x57f187, _0x299776);
            });
        },
        saveScript: function (_0x208c83) {
            var _0x36f170 = document.querySelector(".registered").innerHTML.replace(/class=""/g, "class=\"timer\"");
            var _0x412598 = {
                soundChecked: $("#sound").prop("checked"),
                innerHTML: _0x36f170
            };
            return localStorage.setItem("scriptUpdatedAC", JSON.stringify(_0x412598));
        },
        startPlanner: async function (_0x3edf16) {
            var _0xa5051 = $(".arrival")[0].value;
            var _0x8e398c = $("#time")[0].textContent;
            var _0x5e4b00 = new Date(obsidianScripts.convertToValidFormat(_0xa5051));
            var _0x1257c4 = new Date(obsidianScripts.convertToValidFormat(_0x8e398c));
            var [_0x3b2dd7, _0x18319e, _0xdd9021, _0xb1455] = $("#ScriptAC").find("#ms-Script, #fixed, #min, #max").map((_0x45a8fb, _0x433ca0) => Number(_0x433ca0.value));
            var _0x5e3dbf = [...$(".custom input")].reduce((_0x42cedc, _0x256c2b, _0x29deaf) => {
                _0x42cedc[game_data.units[_0x29deaf]] = Number(_0x256c2b.value);
                return _0x42cedc;
            }, {});
            var _0x524cdc = [...$(".units-select").children().slice(3, -4)].reduce((_0x15ffb3, _0xddc073, _0x1fe3bc) => {
                var _0x10699f = game_data.units[_0x1fe3bc];
                var _0xe8a863 = {
                    [_0x10699f]: APIUpdated.units[_0x10699f]
                };
                if (!_0xddc073.style.opacity) {
                    _0x15ffb3.push(_0xe8a863);
                }
                return _0x15ffb3;
            }, []).sort((_0x3abe71, _0x8e6cd6) => Object.values(_0x8e6cd6)[0] - Object.values(_0x3abe71)[0]).map(_0x1ed57b => Object.keys(_0x1ed57b)[0]);
            document.querySelectorAll("textarea").forEach(_0x5550f2 => /\d{1,3}\|\d{1,3}/.test(_0x5550f2.value) && (_0x5550f2.value = _0x5550f2.value.match(/\d{1,3}\|\d{1,3}/g).reduce((_0x41f657, _0x58e318) => _0x41f657 + " " + _0x58e318)));
            var _0x24ee2c = [];
            var _0x2f283d = document.querySelector(".coordinates").value.split(" ").reduce((_0x524611, _0x3ef702) => {
                _0x524611[_0x3ef702] = true;
                return _0x524611;
            }, {});
            await this.RequestPages("troops", _0x330918 => $(_0x330918).find(".quickedit-label").each(function (_0x47ee0b) {
                var _0x514fee = this.textContent.match(/\d{1,3}\|\d{1,3}/)[0];
                if (typeof _0x2f283d[_0x514fee] == "boolean") {
                    var _0x4db192 = [...$(this).closest("tr").find(".unit-item").slice(0, -1)].reduce((_0x364edd, _0x22abd4, _0xd73429) => {
                        _0x364edd[game_data.units[_0xd73429]] = Number(_0x22abd4.textContent);
                        return _0x364edd;
                    }, {});
                    var _0x592589 = Object.values(_0x5e3dbf).some(_0x4f7a75 => _0x4f7a75 > 0) ? [_0x5e3dbf, true] : [_0x4db192, false];
                    var {
                        spear: _0x899a7b,
                        sword: _0x2f56f7,
                        axe: _0x3f7c23,
                        archer: _0x24e9f6,
                        spy: _0x169e03,
                        light: _0x16a23f,
                        marcher: _0x3282b5,
                        heavy: _0x5ce38d,
                        ram: _0x29a2e4,
                        catapult: _0x11f2ed,
                        knight: _0xc6d5c3,
                        snob: _0x530576
                    } = _0x592589[0];
                    document.querySelector(".targets").value.split(" ").forEach(_0x2bf1cc => $(_0x524cdc).each(function (_0x4d2aec, _0x5e57ca) {
                        var _0x6d5301 = obsidianScripts.ScriptAC.calculateTimes(_0x5e4b00, _0x1257c4, _0x3b2dd7, _0x18319e, _0xdd9021, _0xb1455, _0x514fee, _0x2bf1cc, APIUpdated.units[_0x5e57ca]);
                        if (!_0x6d5301) {
                            return undefined;
                        } else {
                            return (_0x592589[1] ? _0x4db192[_0x5e57ca] >= _0x592589[0][_0x5e57ca] ? _0x592589[0][_0x5e57ca] : undefined : _0x592589[0][_0x5e57ca]) && !_0x24ee2c.push({
                                realUnits: _0x4db192,
                                unit: _0x5e57ca,
                                coord: _0x514fee,
                                target: _0x2bf1cc,
                                spear: _0x899a7b,
                                sword: _0x2f56f7,
                                axe: _0x3f7c23,
                                archer: _0x24e9f6,
                                spy: _0x169e03,
                                light: _0x16a23f,
                                marcher: _0x3282b5,
                                heavy: _0x5ce38d,
                                ram: _0x29a2e4,
                                catapult: _0x11f2ed,
                                knight: _0xc6d5c3,
                                snob: _0x530576,
                                launchTime: _0x6d5301
                            });
                        }
                    }));
                }
                ;
            }));
            _0x24ee2c.sort((_0x243d70, _0x24c5c6) => _0x243d70.launchTime - _0x24c5c6.launchTime).splice(200);
            if (!_0x24ee2c.length) {
                UI.ErrorMessage("Nenhuma combinação encontrada!");
            } else {
                var _0x31cb52 = String();
                _0x24ee2c.forEach((_0x5ecdd1, _0x1a78b2) => {
                    _0x31cb52 += "<tr><td align=\"center\">" + (_0x1a78b2 + 1) + "</td><td style=\"text-align: center; width: 70px\"><a href=\"/game.php?village=" + APIUpdated.database[_0x5ecdd1.coord].villageID + "&screen=overview\" target=\"_blank\" rel=\"noopener noreferrer\">" + _0x5ecdd1.coord + "</a></td><td style=\"text-align: center; width: 70px\"><a href=\"" + game_data.link_base_pure + "info_village&id=" + APIUpdated.database[_0x5ecdd1.target].villageID + "\"target=\"_blank\" rel=\"noopener noreferrer\">" + _0x5ecdd1.target + "</a></td>";
                    game_data.units.slice(0, -1).forEach(_0x3c865d => {
                        return _0x31cb52 += "<td style=\"" + (_0x524cdc.includes(_0x3c865d) && APIUpdated.units[_0x3c865d] <= APIUpdated.units[_0x5ecdd1.unit] && _0x5ecdd1.realUnits[_0x3c865d] >= _0x5ecdd1[_0x3c865d] && _0x5ecdd1[_0x3c865d] ? "color: #C3FFA5;\"" : "opacity: 0.3;") + "\">" + _0x5ecdd1[_0x3c865d] + "</td>";
                    });
                    _0x31cb52 += "<td>" + this.formatDateTime(_0x5ecdd1.launchTime) + "</td><td style=\"color: #c14848;\"><span class=\"timer\">" + obsidianScripts.secondsToHms((_0x5ecdd1.launchTime - _0x1257c4) / 1000) + "</span</td><td><select style=\"border: none;\"><option value=\"1\">AGENDAR</option><option value=\"2\">ALTERAR</option></td><td align=\"center\"><input type=\"button\" class=\"btn\" style=\"font-family: monospace\" onclick=\"obsidianScripts.ScriptAC.pickUpUnits(this)\" value=\"ENVIAR\"></td></tr>";
                });
                document.querySelector(".combinations-found").innerHTML = _0x31cb52;
                Timing.tickHandlers.timers.init();
                return this.duplicateCoordinate();
            }
            ;
        },
        sendCommand: function (_0x28ff88, _0x3d7dd1, _0x3315fb, _0x193a81, _0x50b11b, _0x3393f0) {
            function _0x3f0822(_0x33438c) {
                var _0x2d9860 = $("#" + _0x3315fb);
                return _0x2d9860.length && $.ajax({
                    url: "/game.php?village=" + _0x3d7dd1 + "&screen=place&action=command",
                    method: "POST",
                    data: _0x28ff88,
                    success: function (_0x1fc553) {
                        if ($("#sound").prop("checked")) {
                            TribalWars.playSound("chat");
                        }
                        var _0x12f68b = $(_0x1fc553).find(".error_box");
                        if (_0x12f68b.length) {
                            return obsidianScripts.ScriptAC.logScript("Houve um erro ao enviar a coordenada " + _0x193a81 + ", erro: " + _0x12f68b[0].textContent.trim(), "error");
                        } else {
                            return obsidianScripts.ScriptAC.logScript("A coordenada " + _0x193a81 + ", alvo: " + _0x50b11b + " foi enviada com sucesso!", "success");
                        }
                    },
                    error: function (_0x49e065) {
                        return obsidianScripts.ScriptAC.logScript("Verifique se a coordenada " + _0x193a81 + " foi enviada, não é possível enviar muitos comandos ao mesmo tempo!", "alert");
                    },
                    complete: function (_0x5cf855) {
                        _0x2d9860.remove();
                        obsidianScripts.ScriptAC.infoScript(_0x2d9860, false);
                        obsidianScripts.ScriptAC.duplicateCoordinate();
                        return obsidianScripts.ScriptAC.saveScript();
                    }
                });
            }
            return setTimeout(_0x3f0822, _0x3393f0 - Timing.getCurrentServerTime() - 0);
        },
        timerCommand: function (_0x196247, _0x10b2cf, _0x53877e, _0x30d60c, _0x59035e, _0x439cd5, _0xfdadb8, _0x3a4b3d) {
            function _0x21b29d(_0x10d48e) {
                var _0x1effe2 = $("#" + _0x53877e);
                if (_0x1effe2.length) {
                    $.ajax({
                        url: "/game.php?village=" + _0x10b2cf + "&screen=place&try=confirm",
                        method: "POST",
                        data: _0x196247,
                        success: function (_0x3c3f20) {
                            var _0x2ce60b = $(_0x3c3f20).find("#command-data-form");
                            var _0x1931b8 = _0x2ce60b.attr("action").includes("command") && obsidianScripts.hoursToSeconds(_0x2ce60b[0].innerHTML.match(/(?<=<td>)\d+:\d+:\d+(?=<\/td>)/)[0]) * 1000;
                            var _0x10bcc5 = _0x2ce60b.serialize() + ("&submit_confirm=" + _0x30d60c + "&h=" + game_data.csrf);
                            var _0x249b7c = $(_0x3c3f20).find(".error_box");
                            var _0x8bd107 = _0x1effe2.find("td").slice(-4);
                            var _0x5f130c = new Date(_0xfdadb8.getTime() + _0x3a4b3d - _0x1931b8);
                            var _0x5df282 = $("#time")[0].textContent;
                            var _0x5e44d2 = _0x5f130c - new Date(obsidianScripts.convertToValidFormat(_0x5df282));
                            if (!_0x249b7c.length) {
                                if (!_0x196247.includes("support") || _0x3a4b3d == _0x1931b8) {
                                    return obsidianScripts.ScriptAC.sendCommand(_0x10bcc5, _0x10b2cf, _0x53877e, _0x59035e, _0x439cd5, _0xfdadb8);
                                } else if (_0x3a4b3d > _0x1931b8) {
                                    _0x8bd107[1].innerHTML = _0x8bd107[1].innerHTML.replace(/class=""/, "class=\"timer\"").replace(/\d+:\d+:\d+/, obsidianScripts.secondsToHms(_0x5e44d2 / 1000));
                                    _0x8bd107[0].textContent = obsidianScripts.ScriptAC.formatDateTime(_0x5f130c);
                                    return !Timing.tickHandlers.timers.init() && obsidianScripts.ScriptAC.timerCommand(_0x196247, _0x10b2cf, _0x53877e, _0x30d60c, _0x59035e, _0x439cd5, _0x5f130c, _0x1931b8) && !obsidianScripts.ScriptAC.saveScript() && obsidianScripts.ScriptAC.logScript("AVISO: A coordenada " + _0x59035e + " foi reagendada devido o alvo " + _0x439cd5 + " estar com aflição ativada!", "alert");
                                } else {
                                    return obsidianScripts.ScriptAC.logScript("AVISO: A coordenada " + _0x59035e + " reagendada precisou ser deletado devido a aflição do alvo " + _0x439cd5 + " ter expirado!", "alert") && obsidianScripts.ScriptAC.timerCommand.removeElements();
                                }
                            } else {
                                return obsidianScripts.ScriptAC.logScript("Não foi possível requisitar o comando, erro: " + _0x249b7c[0].textContent.trim(), "error") && obsidianScripts.ScriptAC.timerCommand.removeElements();
                            }
                        },
                        error: function (_0x2b359d) {
                            obsidianScripts.ScriptAC.logScript("Muitos comandos estão sendo requisitados ao mesmo tempo, a coordenada " + _0x59035e + " foi deletado!", "error");
                            return obsidianScripts.ScriptAC.timerCommand.removeElements();
                        }
                    });
                }
                return obsidianScripts.ScriptAC.timerCommand.removeElements = function (_0x235f70) {
                    _0x1effe2.remove();
                    obsidianScripts.ScriptAC.infoScript(_0x1effe2, false);
                    obsidianScripts.ScriptAC.duplicateCoordinate();
                    return obsidianScripts.ScriptAC.saveScript();
                };
            }
            return setTimeout(_0x21b29d, _0xfdadb8 - Timing.getCurrentServerTime() - 10000);
        }
    },
    ScriptAF: {
        barbarianMapping: async function (_0x53afc7) {
            Dialog.show("loading", "<div style=\"text-align: center; font-family: math;\"><b>CARREGANDO...</b><br><br><img src=\"/graphic/throbber.gif\"></div>");
            var _0x21c5f1 = await this.RequestTroopsByGroups();
            var _0x3b6495 = [...$("#template_a input[name*=unit]")].reduce((_0xd6648a, _0xb04854, _0x282a63) => {
                _0xd6648a[_0xb04854.name.replace(/unit_/, "")] = Number(_0xb04854.value);
                return _0xd6648a;
            }, {});
            var _0x213e7f = document.querySelector("#radius").value;
            var _0xffc9f7 = Object.entries(APIUpdated.database).filter(([, {
                                                                         playerID: _0x3e7177
                                                                         }]) => _0x3e7177 == 0).map(([_0x3b8dac]) => _0x3b8dac);
            var _0x35ba9f = $("#template_a input[type=hidden]")[0].value;
            var _0x23c21d = [];
            var _0xb53193 = [];
            Object.entries(_0x21c5f1).forEach(([_0x295e38, _0x5b77dc]) => {
                var _0x1bee5c = Object.keys(_0x3b6495).reduce((_0x2c97c9, _0x4016da) => {
                    if (_0x3b6495[_0x4016da]) {
                        _0x2c97c9.push(_0x5b77dc[_0x4016da] / _0x3b6495[_0x4016da]);
                    }
                    return _0x2c97c9;
                }, []);
                var _0x4b2197 = Math.min(..._0x1bee5c);
                var _0x1b8b3a = _0xffc9f7.filter(_0x4ef1ed => !_0x23c21d.includes(_0x4ef1ed)).map(_0x560aa4 => ({
                    barbarian: _0x560aa4,
                    distance: obsidianScripts.calculateDistance(_0x295e38, _0x560aa4)
                })).filter(({
                    distance: _0x77aa6
                }) => _0x77aa6 <= Number(_0x213e7f)).sort((_0x386215, _0x6cce0a) => _0x386215.distance - _0x6cce0a.distance).slice(0, _0x4b2197).map(({
                    barbarian: _0x58bfd6
                }) => {
                    _0x23c21d.push(_0x58bfd6);
                    return {
                        village_id: APIUpdated.database[_0x295e38].villageID,
                        template_id: _0x35ba9f,
                        target_id: APIUpdated.database[_0x58bfd6].villageID
                    };
                });
                return _0xb53193.push(..._0x1b8b3a);
            });
            if (_0xb53193.length) {
                return this.sendFarms(_0xb53193, "mapping");
            } else {
                return !UI.ErrorMessage("Nenhum mapeamento encontrado!") && Dialog.close();
            }
        },
        changeSelect: function (_0x2fe940) {
            if (_0x2fe940.target.value != 2) {
                return $("#" + _0x2fe940.target.id + "-fixed").css("display", "table-cell") && $("#" + _0x2fe940.target.id + "-min, #" + _0x2fe940.target.id + "-max").css("display", "none");
            } else {
                return $("#" + _0x2fe940.target.id + "-min, #" + _0x2fe940.target.id + "-max").css("display", "table-cell") && $("#" + _0x2fe940.target.id + "-fixed").css("display", "none");
            }
        },
        filterUnits: function (_0x3d5e93) {
            var _0x559b85 = ["snob", "ram", "catapult", "militia"];
            return game_data.units.filter(_0x194b28 => !_0x559b85.includes(_0x194b28));
        },
        filterReports: function (_0x6d1a) {
            var _0x224ce1 = [{
                url: "&group_id=-1",
                method: "GET",
                data: null
            }, {
                url: "&action=set_filter_max_loot",
                method: "POST",
                data: null
            }, {
                url: "&action=set_filter_own_reports",
                method: "POST",
                data: null
            }, {
                url: "&action=set_filter_current_village",
                method: "POST",
                data: null
            }, {
                url: "&action=set_filter_icon",
                method: "POST",
                data: "filter_attack_type=1&filter_icon_operator=AND"
            }, {
                url: "&action=set_filter_dots",
                method: "POST",
                data: "filter_dots=0"
            }, {
                url: "&action=set_filter_subject",
                method: "POST",
                data: "filter_subject="
            }];
            return new Promise(async function (_0x7ea64e) {
                for (var _0x1ddbc0 of _0x224ce1) {
                    await $.ajax({
                        url: game_data.link_base_pure + "report&mode=all" + _0x1ddbc0.url,
                        method: _0x1ddbc0.method,
                        data: _0x1ddbc0.data + ("&h=" + game_data.csrf)
                    });
                }
                ;
                return _0x7ea64e(this);
            });
        },
        init: function (_0x531356) {
            var _0x2291db = JSON.parse(localStorage.getItem("scriptUpdatedAF"));
            return _0x2291db && $("#ScriptAF input:not(.btn)").each((_0x974928, _0x2a2e33) => {
                _0x2a2e33[_0x2a2e33.type == "checkbox" ? "checked" : "value"] = _0x2291db.template[_0x974928];
                return undefined;
            }) && $("#ScriptAF option[value=" + _0x2291db.optionGroup + "]").prop("selected", true);
        },
        RequestXML: async function (_0x5e5c19) {
            $("#loading").html("<h4 style=\"padding: 6px; margin: 0px;\">PROCESSANDO TODOS OS DADOS 😎 AGUARDE FINALIZAR...</h4><span id=\"troops\" style=\"color: #53853b;\"></span><br><img src=\"/graphic/throbber.gif\" style=\"width: 40px;\">");
            var _0xfdf58a = await this.RequestTroopsByGroups();
            var _0x1836c2 = [...$("input#farm_start:checked").closest("tr")].reduce((_0x39fd42, _0x53cd11) => {
                _0x39fd42[_0x53cd11.id] = [...$(_0x53cd11).find("[name*=unit], #distance, [id*=dots]")].reduce((_0x5ad0bd, _0x308ea3) => {
                    _0x5ad0bd[!_0x308ea3.id ? _0x308ea3.name : _0x308ea3.id] = _0x308ea3.type == "text" ? Number(_0x308ea3.value) : _0x308ea3.checked;
                    return _0x5ad0bd;
                }, {});
                return _0x39fd42;
            }, {});
            var {
                template_a: _0x6e1320,
                template_b: _0xf8c3ea,
                template_c: _0x4d2b50
            } = _0x1836c2;
            var [_0x27a4a2, _0x4c0bad, _0xb11b10] = [$("#template_a input[type=hidden]")[0].value, $("#template_b input[type=hidden]")[0].value, [...$("#template_c [name*=unit]:checked")]];
            var _0x34b77e = [];
            var _0x42837c = [];
            var _0x3bdf7d = Object.entries(APIUpdated.database).filter(([, {
                                                                         playerID: _0x5cbd01
                                                                         }]) => _0x5cbd01 == 0).reduce((_0x1d3fc5, [_0x4f183e, {
                villageID: _0x53e979
            }]) => {
                _0x1d3fc5[_0x4f183e] = _0x53e979;
                return _0x1d3fc5;
            }, {});
            await this.RequestPages("report", function (_0x285328) {
                $(_0x285328).find("#report_list tr[class*=report]").each(function (_0x5e2570) {
                    var _0x2bd9a7 = this.textContent.match(/\d{1,3}\|\d{1,3}/g)[1];
                    var _0x353a79 = _0x3bdf7d[_0x2bd9a7];
                    var _0x4d7ce5 = $(this).find("img[src*=dots]").attr("src").match(/\w+(?=.png)/)[0];
                    var _0x3022ad = $(this).find("img[src*=loot]").attr("src");
                    var _0x24813e = this.className.match(/\d+/)[0];
                    var _0x27c578 = $(this).find("img[src*=spy]");
                    var _0xc7f7c0 = _0x3022ad && Number(_0x3022ad.match(/\d+(?=.png)/)[0]);
                    $.each(_0xfdf58a, function _0x4d2e05(_0x23ae19, _0x5d50dc) {
                        var _0x426e43 = obsidianScripts.calculateDistance(_0x23ae19, _0x2bd9a7);
                        var _0x231d18 = APIUpdated.database[_0x23ae19].villageID;
                        var _0x1bdaad = {
                            village_id: _0x231d18,
                            report_id: _0x24813e
                        };
                        if (obsidianScripts.ScriptAF.filterUnits().some(_0xd0f065 => _0x5d50dc[_0xd0f065] < 0)) {
                            return undefined;
                        } else if (!_0x353a79 || _0x42837c.includes(_0x2bd9a7)) {
                            return !!undefined;
                        } else {
                            return (_0x27c578.length && _0x4d2b50 && _0x426e43 <= _0x4d2b50.distance && _0x4d2b50[_0x4d7ce5 + "_dots"] ? _0x34b77e.push(_0x1bdaad) : _0x3022ad && _0xc7f7c0 && _0xf8c3ea && _0x426e43 <= _0xf8c3ea.distance && _0xf8c3ea[_0x4d7ce5 + "_dots"] ? _0x34b77e.push({
                                target_id: _0x353a79,
                                template_id: _0x4c0bad,
                                village_id: _0x231d18
                            }) : (!_0x3022ad || !_0xc7f7c0) && _0x6e1320 && _0x426e43 <= _0x6e1320.distance && _0x6e1320[_0x4d7ce5 + "_dots"] && _0x34b77e.push({
                                target_id: _0x353a79,
                                template_id: _0x27a4a2,
                                village_id: _0x231d18
                            })) && (_0x4d2e05.lastID = _0x34b77e.slice(-1)[0], _0x4d2e05.template_icon = _0x4d2e05.lastID.template_id == _0x27a4a2 ? _0x6e1320 : _0xf8c3ea, (_0x4d2e05.lastID.template_id ? obsidianScripts.ScriptAF.filterUnits().some(_0x34e7a8 => (_0xfdf58a[_0x23ae19][_0x34e7a8] -= _0x4d2e05.template_icon["unit_" + _0x34e7a8]) < 0) : _0xb11b10.every(_0x63eb4d => _0xfdf58a[_0x23ae19][_0x63eb4d.name.replace(/unit_/, "")] == 0)) && _0x34b77e.pop() || !_0x42837c.push(_0x2bd9a7));
                        }
                    });
                });
            });
            if (_0x34b77e.length) {
                return this.sendFarms(_0x34b77e, "farm");
            } else {
                return !UI.ErrorMessage("Nenhuma combinação encontrada! reiniciando...") && this.reloadFarms(this);
            }
        },
        RequestTroopsByGroups: async function (_0x1df8e7) {
            var _0x4d26fb = $("#ScriptAF .optionGroup option:selected")[0].value;
            var _0x39af95 = await this.RequestVillagesByGroups(_0x4d26fb);
            var _0x918019 = {};
            await this.RequestPages("troops", function (_0x59310c) {
                $(_0x59310c).find(".quickedit-label").each(function (_0x3c1f7e) {
                    var _0x4717f9 = this.textContent.match(/\d{1,3}\|\d{1,3}/)[0];
                    if (_0x39af95.includes(_0x4717f9)) {
                        return _0x918019[_0x4717f9] = [...$(this).closest("tr").find(".unit-item")].reduce((_0x1c216f, _0x181af2, _0x207fae) => {
                            _0x1c216f[game_data.units[_0x207fae]] = Number(_0x181af2.textContent);
                            return _0x1c216f;
                        }, {});
                    }
                    ;
                });
            });
            return _0x918019;
        },
        RequestVillagesByGroups: function (_0x32328a) {
            return $.ajax({
                url: game_data.link_base_pure + "groups&ajax=load_villages_from_group",
                method: "POST",
                group_id: _0x32328a
            }).then(_0x224842 => {
                return _0x224842.html.match(/\d{1,3}\|\d{1,3}/g);
            });
        },
        RequestPages: async function (_0x3ee321, _0x5d76d0) {
            var _0xa10a21 = _0x3ee321 == "troops" ? ["overview_villages&mode=units&action=change_page_size&type=own_home&group=0", true] : _0x3ee321 == "report" && ["report&action=change_page_size&mode=all&from=0", false];
            if (!_0xa10a21[1]) {
                await this.filterReports();
            }
            return $.ajax({
                url: game_data.link_base_pure + _0xa10a21[0],
                method: "POST",
                data: "page_size=1000&h=" + game_data.csrf
            }).then(async _0xbf204 => {
                var _0x5a78dc = $(_0xbf204).find(".paged-nav-item");
                var _0xd3a767 = [_0xbf204];
                for (var _0x4275be = 0; _0x4275be < _0x5a78dc.length; _0x4275be++) {
                    var _0x4081b9 = await $.get(game_data.link_base_pure + (_0xa10a21[1] ? "overview_villages&mode=units&type=own_home&group=0&page=" + (_0x4275be + 1) : "report&mode=all&from=" + (_0x4275be + 1) * 1000));
                    _0xd3a767.push(_0x4081b9);
                }
                ;
                return _0xd3a767.forEach(_0x5d76d0);
            });
        },
        RequestTemplateC: function (_0x2c4f4f) {
            var _0x2de645 = [...$(_0x2c4f4f.target).closest("tr").find("[name*=unit]")].reduce((_0x2cec16, _0x20372d) => _0x20372d.checked ? _0x2cec16 + "&" + _0x20372d.name.replace(/unit_/, "") + "=on" : _0x2cec16, "") + "&target_screen=am_farm";
            return _0x2c4f4f.target.name && _0x2c4f4f.target.name.includes("unit") && $.ajax({
                url: game_data.link_base_pure + ("am_farm&ajaxaction=change_troops&h=" + game_data.csrf),
                method: "POST",
                data: _0x2de645
            });
        },
        reloadFarms: function (_0x2518a3) {
            var [_0x436333, _0x17bfb6, _0x5d3a8f] = $("#time-farm-fixed, #time-farm-min, #time-farm-max").map((_0x1741dd, _0x5c4cb1) => obsidianScripts.hoursToSeconds(_0x5c4cb1.value));
            var _0x4fd478 = $("#time-farm option:selected")[0].textContent != "RANDOM" ? _0x436333 : _0x17bfb6 + Math.random() * (_0x5d3a8f - _0x17bfb6);
            this.reloadFarms.timers = setTimeout(_0x25ff11 => this.RequestXML(this), _0x4fd478 * 1000);
            return $("#loading").find("h4").html("REINICIANDO A FARM EM: <span class=\"timer\" style=\"color: #c14848\">" + obsidianScripts.secondsToHms(_0x4fd478) + "</span>&nbsp;🕐").nextAll("div, span, img, br").remove() && Timing.tickHandlers.timers.init();
        },
        templateSave: function (_0x277306) {
            return $.get(game_data.link_base_pure + "am_farm").then(_0x483961 => {
                var [_0x57bebc, _0x5079fb, _0x24429f, _0x33af34] = [...$(_0x483961).find("input[name*=template]")].map(_0x39dcde => _0x39dcde.value);
                var _0x306003 = $("#template_a input[name*=unit]");
                var _0x50675b = $("#template_b input[name*=unit]");
                var _0x28e47f = "template[" + _0x57bebc + "][id]=" + _0x57bebc + "&template[" + _0x57bebc + "][new]=" + _0x5079fb + this.filterUnits().reduce((_0x2daa2a, _0x342a04, _0xac443d) => _0x2daa2a + ("&" + _0x342a04 + "[" + _0x57bebc + "]=" + _0x306003[_0xac443d].value), "") + ("&template[" + _0x24429f + "][id]=" + _0x24429f + "&template[" + _0x24429f + "][new]=" + _0x33af34) + this.filterUnits().reduce((_0x327e0c, _0x459cdf, _0x44d370) => _0x327e0c + ("&" + _0x459cdf + "[" + _0x24429f + "]=" + _0x50675b[_0x44d370].value), "") + ("&h=" + game_data.csrf);
                return this.createNewTemplate(_0x28e47f);
            });
        },
        createNewTemplate: function (_0x3453d9) {
            return $.ajax({
                url: game_data.link_base_pure + "am_farm&action=edit_all",
                method: "POST",
                data: encodeURI(_0x3453d9),
                success: function (_0x40d524) {
                    var [_0x16d07c, _0x5a005c, _0x33c08e, _0x2c02db] = [...$(_0x40d524).find("input[name*=template]")].map(_0x54828d => _0x54828d.value);
                    $("#ScriptAF input[type=hidden]").each((_0x5c3cb3, _0x1a7499) => !_0x5c3cb3 ? _0x1a7499.value = _0x16d07c : _0x5c3cb3 == 1 ? _0x1a7499.value = _0x5a005c : _0x5c3cb3 == 2 ? _0x1a7499.value = _0x33c08e : _0x1a7499.value = _0x2c02db);
                    return !UI.SuccessMessage("O Modelo foi salvo com sucesso!") && obsidianScripts.ScriptAF.saveScript();
                }
            });
        },
        sendFarms: function (_0x47b685, _0x53cf12) {
            var _0x3e09f8 = _0x53cf12 == "farm" ? !!$("#loading").find("h4").html("AUTOFARM INICIADO! 🏇").after("<div class=\"progress-bar live-progress-bar progress-bar-alive\" style=\"width: auto; margin: 6px;\"><div style=\"background: rgb(146, 194, 0);\"></div><span class=\"label\"></span></div>").closest("div").find("img").attr("src", "/graphic/loading.gif") : _0x53cf12 == "mapping" && $(".popup_box_content").html("<div style=\"text-align: center; font-family: math;\"><b>MAPEAMENTO INICIADO! 🔍 AGUARDE...</b><br><br><div class=\"progress-bar live-progress-bar progress-bar-alive\"><div style=\"background: rgb(146, 194, 0);\"></div><span class=\"label\"></span></div><br><img src=\"/graphic/loading.gif\"></div>") && !$("#popup_box_loading").css("width", "500px");
            var _0x461455 = $(".progress-bar");
            var [_0x5c7af2, _0xca9e48, _0x2b82eb] = $("#interval-farm-fixed, #interval-farm-min, #interval-farm-max").map((_0x32efb1, _0x21549f) => Number(_0x21549f.value));
            var _0x3f58c0 = $("#interval-farm option:selected")[0].textContent != "RANDOM" ? _0x5c7af2 : _0xca9e48 + Math.random() * (_0x2b82eb - _0xca9e48);
            function _0x3dc039(_0x3c8ede) {
                var _0x46a139 = _0x47b685[_0x3c8ede].village_id;
                var _0x2e19a6 = _0x47b685[_0x3c8ede].template_id;
                var _0x849c8c = _0x47b685[_0x3c8ede].target_id;
                var _0x2a174a = _0x47b685[_0x3c8ede].report_id;
                var _0x121595 = "/game.php?village=" + _0x46a139 + "&screen=am_farm&mode=farm&ajaxaction=farm";
                var _0x4090cf = _0x2a174a ? [_0x121595 + "_from_report&json=1", {
                    report_id: _0x2a174a,
                    h: game_data.csrf
                }] : [_0x121595 + "&json=1", {
                    target: _0x849c8c,
                    template_id: _0x2e19a6,
                    source: _0x46a139
                }];
                TribalWars.post(_0x4090cf[0], null, _0x4090cf[1], function (_0x50df09) {
                    return $("#loading").find("span").html("<br><b>" + _0x50df09.success + "</b><br>") && obsidianScripts.ScriptAF.sendFarms.init(_0x3c8ede);
                }, function (_0x822a0d) {
                    var _0x1a276b = _("8c51142762038a5b8e6c677aabcbedff");
                    if (_0x822a0d == _0x1a276b && !obsidianScripts.ScriptAF.sendFarms.timers.forEach(_0xcad38 => clearTimeout(_0xcad38)) && _0x47b685.splice(0, _0x47b685.length, ..._0x47b685.filter(_0x58a677 => _0x58a677.village_id != _0x46a139))) {
                        obsidianScripts.ScriptAF.sendFarms.timers.splice(0, obsidianScripts.ScriptAF.sendFarms.timers.length, ..._0x47b685.map((_0x1c1a76, _0x1f5a6e) => setTimeout(_0x3dc039, _0x1f5a6e * _0x3f58c0, _0x1f5a6e)));
                    }
                    return !_0x47b685.length && obsidianScripts.ScriptAF.sendFarms.init(0);
                });
            }
            this.sendFarms.timers = _0x47b685.map((_0x3d3cc2, _0x2cf019) => setTimeout(_0x3dc039, _0x2cf019 * _0x3f58c0, _0x2cf019));
            return this.sendFarms.init = function (_0x537b1b) {
                UI.updateProgressBar(_0x461455, _0x537b1b + 1, _0x47b685.length);
                return (_0x537b1b + 1 == _0x47b685.length || !_0x47b685.length) && (!_0x3e09f8 ? Dialog.close() : obsidianScripts.ScriptAF.reloadFarms(this));
            };
        },
        stopScript: function (_0x35b02c) {
            if (this.sendFarms.timers) {
                [...this.sendFarms.timers, this.reloadFarms.timers].forEach(_0x38bf03 => clearTimeout(_0x38bf03));
            }
            return document.querySelector("#loading").innerHTML = "";
        },
        saveScript: function (_0x21558e) {
            var _0x1bbc9f = $("#ScriptAF .optionGroup option:selected")[0].value;
            var _0x266a34 = $("#ScriptAF input:not(.btn)").map((_0x55ac6a, _0x1b73e5) => _0x1b73e5.type == "checkbox" ? _0x1b73e5.checked : _0x1b73e5.value);
            var _0x55dff5 = {
                template: _0x266a34,
                optionGroup: _0x1bbc9f
            };
            return localStorage.setItem("scriptUpdatedAF", JSON.stringify(_0x55dff5));
        }
    },
    ScriptAM: {
        callResources: async function (_0x1c3fdd) {
            var [_0x7b57e1, _0x1e7acf] = $("#ScriptAM").find(".optionVillage option:selected, .optionGroup option:selected").map((_0x20f64b, _0xe72e53) => !_0x20f64b ? APIUpdated.database[_0xe72e53.textContent.match(/\d{1,3}\|\d{1,3}/)[0]].villageID : _0xe72e53.value);
            var _0x1bae7c = $("#time-resources")[0].value;
            var [_0x465d72, _0xd60c26, _0x1cab22] = $("#time-resources-fixed, #time-resources-min, #time-resources-max").map((_0x332ffc, _0x25dd80) => obsidianScripts.hoursToSeconds(_0x25dd80.value));
            var _0x5dc9ea = _0x1bae7c != 2 ? _0x465d72 : _0xd60c26 + Math.random() * (_0x1cab22 - _0xd60c26);
            var _0x151eaa = "target_id=0&select-all=on";
            await this.RequestPages("market", _0x7b57e1, _0x1e7acf, _0x1b16e3 => $(_0x1b16e3).find("tr.supply_location").each(function (_0x318101) {
                var _0x381bac = [...$(this).find("td a, td.wood, td.stone, td.iron, td.traders")].reduce((_0x21b537, _0x4bafca, _0x17b94a) => {
                    _0x21b537[!_0x4bafca.className ? "village" : _0x4bafca.className] = !_0x17b94a ? this.dataset.village : Number(_0x17b94a != 4 ? _0x4bafca.dataset.res : this.dataset.capacity);
                    return _0x21b537;
                }, {});
                var _0x3ca66a = (_0x381bac.wood + _0x381bac.stone + _0x381bac.iron) / _0x381bac.traders;
                var _0x4fce34 = _0x3ca66a == Infinity ? undefined : !~~_0x3ca66a ? 1 : _0x3ca66a;
                return _0x4fce34 && (_0x151eaa += "&resource[" + _0x381bac.village + "][wood]=" + ~~(_0x381bac.wood / _0x4fce34) + "&resource[" + _0x381bac.village + "][stone]=" + ~~(_0x381bac.stone / _0x4fce34) + "&resource[" + _0x381bac.village + "][iron]=" + ~~(_0x381bac.iron / _0x4fce34) + "&select-village=" + _0x381bac.village);
            }));
            if (!_0x1c3fdd.checked) {
                return ![this.callResources.timers, this.ResourcesTable.timers].forEach(_0x12e4eb => clearTimeout(_0x12e4eb)) && $("#end-time-resources").html("00:00:00");
            } else {
                if (_0x151eaa.includes("village")) {
                    await $.ajax({
                        url: "/game.php?village=" + _0x7b57e1 + "&screen=market&ajaxaction=call&h=" + game_data.csrf,
                        method: "POST",
                        data: encodeURI(_0x151eaa)
                    });
                }
                return (await this.ResourcesTable(_0x7b57e1, _0x1e7acf)) && $("#end-time-resources").html("<span class=\"timer\">" + obsidianScripts.secondsToHms(_0x5dc9ea) + "</span>") && !Timing.tickHandlers.timers.init() && (this.callResources.timers = setTimeout(_0x103b0e => this.callResources(_0x1c3fdd), _0x5dc9ea * 1000));
            }
        },
        RequestXML: async function (_0x5a04c7) {
            var _0x28c005 = $(".optionVillage option:selected").text().match(/\d{1,3}\|\d{1,3}/)[0];
            var _0x331bf9 = APIUpdated.database[_0x28c005].villageID;
            var _0x5279d2 = $("#time-coins")[0].value;
            var [_0x4fcd8d, _0x24603e, _0x41b4a7] = $("#time-coins-fixed, #time-coins-min, #time-coins-max").map((_0x3ae4a6, _0x17c0a0) => obsidianScripts.hoursToSeconds(_0x17c0a0.value));
            var _0x1646ff = _0x5279d2 != 2 ? _0x4fcd8d : _0x24603e + Math.random() * (_0x41b4a7 - _0x24603e);
            await $.get("/game.php?village=" + _0x331bf9 + "&screen=snob&mode=train").then(async _0x1b1f8d => {
                var _0x29109c = $(_0x1b1f8d).find("#coin_mint_fill_max");
                return _0x29109c.length && (await $.ajax({
                    url: "/game.php?village=" + _0x331bf9 + "&screen=snob&action=coin",
                    method: "POST",
                    data: "count=" + _0x29109c[0].textContent.match(/\d+/) + "&h=" + game_data.csrf
                }));
            });
            if ($("#end-time-coins").html("<span class=\"timer\">" + obsidianScripts.secondsToHms(_0x1646ff) + "</span>")) {
                Timing.tickHandlers.timers.init();
            }
            return this.RequestXML.timers = setTimeout(_0x1d7a68 => this.RequestXML(this), _0x1646ff * 1000);
        },
        RequestPages: function (_0x16e11c, _0x3b011b, _0x2d2f31, _0x3da091) {
            var _0xb5ca99 = _0x16e11c == "transport" ? ["overview_villages&mode=trader&action=change_page_size&type=inc", "overview_villages&mode=trader&type=inc&type=inc"] : _0x16e11c == "market" && ["market&mode=call&action=change_call_pagesize", "market&mode=call&order=distance&dir=ASC&target_id=" + _0x3b011b];
            var _0x5edbbe = {
                url: "/game.php?village=" + _0x3b011b + "&screen=" + _0xb5ca99[0] + "&group=" + _0x2d2f31,
                method: "POST",
                data: "page_size=1000&h=" + game_data.csrf
            };
            return $.ajax(_0x5edbbe).then(async _0x56fb01 => {
                var _0x3e8e23 = $(_0x56fb01).find("a.paged-nav-item");
                var _0x27db48 = [_0x56fb01];
                for (var _0x43e7c0 = 0; _0x43e7c0 < _0x3e8e23.length - 1; _0x43e7c0++) {
                    var _0x20eff7 = await $.get("/game.php?village=" + _0x3b011b + "&screen=" + _0xb5ca99[1] + "&page=" + (_0x43e7c0 + 1));
                    _0x27db48.push(_0x20eff7);
                }
                ;
                return _0x27db48.forEach(_0x3da091);
            });
        },
        RequestVillages: function (_0x162bf8) {
            return $.ajax({
                url: game_data.link_base_pure + "groups&ajax=load_villages_from_group",
                method: "POST",
                group_id: 0
            }).then(_0x5dbce6 => {
                var _0x50c8b0 = "<select class=\"optionVillage\" style=\"width: 100%;\">";
                $(_0x5dbce6.html).find("#group_table tr").each(function (_0x195c83) {
                    return _0x50c8b0 += "<option value=\"" + _0x195c83 + "\">" + this.textContent.replace(/\s+/g, " ").trim() + "</option>";
                });
                _0x50c8b0 += "</select>";
                return _0x50c8b0;
            });
        },
        ResourcesTable: async function (_0x1f6d32, _0x24d1e1) {
            var _0x3882e5 = String();
            await this.RequestPages("transport", _0x1f6d32, _0x24d1e1, function (_0x1b5121) {
                var _0x95ca2a = $(_0x1b5121).find("#trades_table");
                return _0x95ca2a.length && (_0x3882e5 += _0x95ca2a[0].innerHTML);
            });
            if (_0x3882e5) {
                $("#resources-table").html("<table id=\"container\" width=\"100%\">" + _0x3882e5 + "</table>").find("tr").each((_0x11407e, _0x4ecf66) => $(_0x4ecf66.cells).each((_0x2e15cd, _0x1e1e32) => [0, 1, 4, 7].includes(_0x2e15cd) ? $(_0x1e1e32).remove() : undefined) && !_0x4ecf66.className.includes("selected") && _0x11407e ? $(_0x4ecf66).remove() : undefined);
            } else {
                $("#resources-table").html("<span style=\"font-size: smaller; color: #6d6db3;\">NÃO EXISTEM TRANSPORTES PARA SEREM EXIBIDOS!</span>");
            }
            Timing.tickHandlers.timers.init();
            return this.ResourcesTable.timers = setTimeout(_0x49ddd5 => this.ResourcesTable(_0x1f6d32, _0x24d1e1), 60000 + Math.random() * 120000);
        },
        stopScript: function (_0x1f4f07) {
            return !clearTimeout(this.RequestXML.timers) && $("#end-time-coins").html("00:00:00");
        },
        timeScript: function (_0x5c9e58) {
            if (_0x5c9e58.target.value != 2) {
                return $("#" + _0x5c9e58.target.id + "-fixed").css("display", "table-cell") && $("#" + _0x5c9e58.target.id + "-min, #" + _0x5c9e58.target.id + "-max").css("display", "none");
            } else {
                return $("#" + _0x5c9e58.target.id + "-min, #" + _0x5c9e58.target.id + "-max").css("display", "table-cell") && $("#" + _0x5c9e58.target.id + "-fixed").css("display", "none");
            }
        }
    },
    ScriptAI: {
        init: function (_0x22d38d) {
            this.init.scriptUpdated = JSON.parse(localStorage.getItem("scriptUpdatedAI")) ?? this.saveScript(this);
            if (this.init.scriptUpdated.incomingsChecked) {
                $("#ScriptAI #start").prop("checked", true);
            }
            var _0x115714 = document.querySelector("#incomings_cell");
            var _0x37c320 = document.createElement("input");
            new MutationObserver(_0x3d2398 => this.init.scriptUpdated.incomingsChecked && game_data.player.incomings > this.init.scriptUpdated.incomingsTarget ? this.RequestXML(this) : this.saveScript(this)).observe(_0x115714, {
                subtree: true,
                childList: true
            });
            _0x37c320.type = "hidden";
            return _0x115714.append(_0x37c320);
        },
        RequestXML: function (_0x3353a4) {
            return $.ajax({
                url: game_data.link_base_pure + "overview_villages&subtype=attacks&mode=incomings&group=0",
                method: "GET",
                success: function (_0x1c3ada) {
                    var _0x20b0bd = $(_0x1c3ada).find("#incomings_form").serialize();
                    _0x20b0bd += _0x20b0bd.match(/(?<=\%5B)\d+(?=\%5D)/g).reduce((_0x3c25e2, _0x3a6711) => _0x3c25e2 += "&id_" + _0x3a6711 + "=on", "") + "&all=on&label=Etiqueta";
                    return obsidianScripts.ScriptAI.sendIncomings(_0x20b0bd);
                }
            });
        },
        sendIncomings: function (_0x36b4f2) {
            return $.ajax({
                url: game_data.link_base_pure + ("overview_villages&mode=incomings&action=process&type=unignored&subtype=all&h=" + game_data.csrf),
                method: "POST",
                data: _0x36b4f2,
                complete: function (_0x39f773) {
                    return obsidianScripts.ScriptAI.saveScript(this);
                }
            });
        },
        saveScript: function (_0x50438e) {
            var _0x1392f4 = document.querySelector("#ScriptAI #start").checked;
            var _0x21a270 = {
                incomingsTarget: game_data.player.incomings,
                incomingsChecked: _0x1392f4
            };
            localStorage.setItem("scriptUpdatedAI", JSON.stringify(_0x21a270));
            return this.init.scriptUpdated = _0x21a270;
        }
    },
    calculateDistance: function (_0x67adfb, _0x41846a) {
        var [_0x415a61, _0x1ffa4a] = _0x67adfb.split("|");
        var [_0x34be21, _0x302807] = _0x41846a.split("|");
        var _0x5d6122 = Math.abs(_0x415a61 - _0x34be21);
        var _0x40ef12 = Math.abs(_0x1ffa4a - _0x302807);
        return Math.sqrt(_0x5d6122 * _0x5d6122 + _0x40ef12 * _0x40ef12);
    },
    convertToValidFormat: function (_0x1a7c53) {
        var [_0x1bac09, _0x449aa0] = _0x1a7c53.split(" ");
        var [_0x18854a, _0x3866f4, _0x468c4c] = _0x1bac09.split("/");
        return _0x468c4c + "-" + _0x3866f4 + "-" + _0x18854a + " " + _0x449aa0;
    },
    hoursToSeconds: function (_0x56bc46) {
        return _0x56bc46.split(":").map(Number).reduce((_0x5038f6, _0x415903, _0xcbf45e) => _0x5038f6 + (!_0xcbf45e ? _0x415903 * 3600 : _0xcbf45e == 1 ? _0x415903 * 60 : _0x415903), 0);
    },
    init: async function (_0x330fe4) {
        var _0x50ffef = "<div id=\"obsidian-scripts\" class=\"vis content-border\" style=\"display: none; position: fixed; top: 10%; left: 20%; border-radius: 8px; z-index: 7;\">\n            <style>#obsidian-scripts, #obsidian-scripts td, #obsidian-scripts #container-planner, #obsidian-scripts #pay-Script {background: #36393f !important;}#obsidian-scripts th, #obsidian-scripts td[rowspan], #obsidian-scripts .vis_item, #obsidian-scripts h3, #obsidian-scripts h4 {background-color: #202225 !important;background-image: none;}#obsidian-scripts .info_box {background: #202225 url(https://dsbr.innogamescdn.com/asset/234518f7/graphic/questionmark.png) no-repeat 4px center;}#obsidian-scripts input[type=\"text\"], #obsidian-scripts input[type=\"number\"], #obsidian-scripts input[type=\"time\"], #obsidian-scripts textarea, #obsidian-scripts .info_box, #obsidian-scripts small, #obsidian-scripts th, #obsidian-scripts td, #obsidian-scripts h3, #obsidian-scripts h4 {color: rgb(255, 255, 223);}#obsidian-scripts input[type=\"button\"], #obsidian-scripts button {background-image: linear-gradient(#6e7178 0%, #36393f 30%, #202225 80%, black 100%);}#obsidian-scripts input[type=\"text\"], #obsidian-scripts input[type=\"number\"], #obsidian-scripts input[type=\"time\"] {background: none;outline: 1px solid light-dark(rgb(118, 118, 118), rgb(133, 133, 133));border: 0px;}#obsidian-scripts select {background: none;color: #6d6db3;}#obsidian-scripts a {color: #ae441c;}</style>\n            <img src=\"https://twdevtools.github.io/images/obsidian-minimize.png\" style=\"position: absolute; z-index: 1; top: 5px; right: 6px; cursor: pointer; width: 25px;\" onclick=\"obsidianScripts.openAndHiddenScripts('#obsidian-scripts', 'none')\">\n            <table width=\"100%\">\n                <thead>\n                    <style>.title-th {border-radius: 6px 6px 0px 0px;text-align: center;font-size: 14px;padding: 6px;}.header-th {text-align: center;font-family: sans-serif;border-radius: 4px;padding: 4px;}</style>\n                    <tr>\n                        <th class=\"title-th\" style=\"font-size: 16px;\" colspan=\"10\">OBSIDIAN SCRIPTS</th>\n                    </tr>\n                    <tr>\n                        <th class=\"header-th\"><a class=\"a-color\" href=\"#\" onclick=\"obsidianScripts.openAndHiddenTabs('#history-Obsidian')\">HISTÓRIA</a></th>\n                        <th class=\"header-th\"><a class=\"a-color\" href=\"#\" onclick=\"obsidianScripts.openAndHiddenTabs('#welcome-Script')\">BEM-VINDO</a></th>\n                        <th class=\"header-th\"><a class=\"a-color\" href=\"#\" onclick=\"obsidianScripts.openAndHiddenTabs('#ScriptAC')\">AGENDADOR</a></th>\n                        <th class=\"header-th\"><a class=\"a-color\" href=\"#\" onclick=\"obsidianScripts.openAndHiddenTabs('#ScriptAF')\">FARMADOR</a></th>\n                        <th class=\"header-th\"><a class=\"a-color\" href=\"#\" onclick=\"obsidianScripts.openAndHiddenTabs('#ScriptAM')\">CUNHADOR DE MOEDAS</a></th>\n                        <th class=\"header-th\"><a class=\"a-color\" href=\"#\" onclick=\"obsidianScripts.openAndHiddenTabs('#ScriptAI')\">ETIQUETADOR</a></th>\n                        <th class=\"header-th\"><a class=\"a-color\" href=\"#\" onclick=\"obsidianScripts.openAndHiddenTabs('#help-me')\">AJUDA</a></th>\n                    </tr>\n                </thead>\n            </table>\n            <div id=\"history-Obsidian\" style=\"display: none; text-align: center; overflow: auto; font-size: 13px; font-weight: bold; max-width: 600px; height: 400px; padding: 12px; color: rgb(151 149 148);\">\n                <h3 style=\"border-radius: 8px; padding: 6px;\">HISTORIA DO GRUPO OBSIDIAN</h3>\n                <img src=\"https://twdevtools.github.io/images/obsidian-scripts.png\" style=\"position: absolute; z-index: -1; opacity: 0.3; width: 400px; left: 18%;\">\n                Há muito tempo, uma civilização avançada chamada Obsidian dominava a arte do código e da tecnologia. Sua habilidade em desenvolver algoritmos e sistemas era incomparável, permitindo-lhes controlar, criar realidades virtuais e alterar a estrutura da própria informação.<br><br>\n                Essa tecnologia não se limitava apenas ao mundo físico, mas também permeava os domínios digitais e metafísicos. Os códigos de Obsidian eram como chaves mestras que desbloqueavam os segredos do universo, permitindo a compreensão e manipulação de energias cósmicas e dimensões além da nossa percepção convencional.<br><br>\n                No auge de seu poder, a civilização de Obsidian foi ameaçada por uma grande catástrofe natural. Para preservar seu conhecimento e evitar que caísse em mãos erradas, eles decidiram esconder seus arquivos em uma cidade subterrânea, protegida por armadilhas mágicas e guardiões poderosos.<br><br>\n                Centenas de anos se passaram, e a cidade de Obsidian se perdeu na história, tornando-se uma lenda. Até que um arqueólogo destemido, motivado por histórias antigas, descobriu o local da cidade perdida. Ao explorar suas ruínas, ele encontrou os antigos arquivos de Obsidian.<br><br>\n                Ao analisar esses arquivos, descobre que o conhecimento contido neles é verdadeiramente impressionante. No entanto, ele também percebe a responsabilidade monumental que carrega ao ter acesso a esse poder.<br><br>\n                O arqueólogo, diante da magnitude do conhecimento contido nos arquivos de Obsidian, enfrenta uma escolha crucial. Com a ajuda dos Alquimistas da Luz, o arqueólogo embarca em uma jornada para compreender e aplicar sabiamente o conhecimento de Obsidian. Eles estabelecem um conselho de sábios, composto por especialistas de diversas áreas, para orientar o uso ético e responsável dessa tecnologia avançada.<br><br>\n                Juntos, eles começam a desenvolver soluções para os desafios globais utilizando os princípios de tecnologia aprendidos com os antigos códigos de Obsidian, eles transformam a sociedade de forma positiva, guiando-a rumo a um futuro equilibrado e próspero.<br><br>\n                Assim, o legado de Obsidian é honrado não apenas pela redescoberta de seu conhecimento, mas também pela forma como é aplicado para o bem de toda a humanidade, trazendo uma nova era de progresso e consciência para o mundo.\n            </div>\n            <div id=\"welcome-Script\" style=\"display: block; padding: 12px;\">\n                <h3 style=\"text-align: center; padding: 6px; border-radius: 8px;\">BEM VINDO AO OBSIDIAN SCRIPTS!</h3>\n                <img src=\"https://twdevtools.github.io/images/obsidian-scripts.png\" style=\"position: absolute; width: 90px; z-index: 1; top: 90px; left: 4px;\">\n                <div class=\"vis\" style=\"margin: 25px 5px 25px 80px;\"><h4>EXPIRA:&nbsp;<span id=\"time-expire\" style=\"color: #53853b;\"></span>&nbsp;EM:&nbsp;<span id=\"timer-end\" class=\"timer\" style=\"color: #c9c922;\"></span>&nbsp;<a href=\"#\" style=\"text-decoration: underline; font-style: italic;\" onclick=\"obsidianScripts.openAndHiddenScripts('#pay-Script', 'block')\">CLIQUE AQUI PARA RENOVAR O PODER DA OBSIDIAN!</a></h4></div>\n                <div class=\"vis\"><h4 style=\"margin: 0px;\">DISCORD WEBHOOK:</h4><small style=\"margin: 0px 0px 0px 7px;\">Este método serve para enviar notificações para seu discord sobre alertas na sua conta, como captcha e etc...<br>&nbsp;&nbsp;Como criar um webhook:&nbsp;<a href=\"https://support.discord.com/hc/pt-br/articles/228383668-Usando-Webhooks\" target=\"_blank\" rel=\"noopener noreferrer\">Clique Aqui</a></small><input type=\"text\" style=\"font-size: 13pt; width: 97%; margin: 8px;\" placeholder=\"https://discord.com/api/webhooks/...\"></div>\n                <div class=\"vis\"><h4 style=\"margin: 0px;\">EXECUTAR/ABRIR A OBSIDIAN:</h4><img src=\"https://twdevtools.github.io/images/obsidian-screen.jpeg\" style=\"border-radius: 20px;\"><br><small style=\"margin: 0px 0px 0px 7px;\">Aqui deve-se configurar a localidade onde a Obsidian será executado.<br></small><input type=\"text\" style=\"font-size: 13pt; width: 97%; margin: 8px;\" placeholder=\"ex: screen=accountmanager...\"></div>\n                <div class=\"vis\"><h4 style=\"margin: 0px;\">REDES SOCIAIS:</h4><img src=\"https://twdevtools.github.io/images/obsidian-whatsapp.png\" style=\"width: 25px; margin: 4px;\"><a href=\"#\" style=\"vertical-align: 11px; cursor: default;\">+55 48-98824-2773</a><img src=\"https://twdevtools.github.io/images/obsidian-discord.png\" style=\"width: 33px;\"><a href=\"https://discord.gg/SBpuDZ5\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"vertical-align: 11px;\">Clique Aqui</a></div>\n                <div class=\"serverTime\" style=\"position: absolute; font-size: small; bottom: 3px; right: 5px; color: #c9c922;\"><span id=\"time\"></span></div>\n            </div>\n            <div id=\"ScriptAM\" style=\"display: none;\">\n                <div id=\"title-Script\"><h3 style=\"text-align: center; border-radius: 8px 8px 0px 0px; padding: 6px; margin: 0px;\">AUTO CUNHAGEM</h3></div>\n                <div id=\"options\" class=\"vis\" style=\"font-weight: bold; border-width: 1px 0px 1px 0px; margin: 0px;\">\n                    <style>#obsidian-scripts input[id^=time-] {height: 14px;vertical-align: 1px;}</style>\n                    <table width=\"100%\">\n                        <tbody>\n                            <tr>\n                                <td align=\"center\">COORDENADA PARA CUNHAR:</td>\n                                <td>" + (await obsidianScripts.ScriptAM.RequestVillages()) + "</td>\n                            </tr>\n                            <tr>\n                                <td align=\"center\">VERIFICAR A CADA:</td>\n                                <td><select id=\"time-coins\" onchange=\"obsidianScripts.ScriptAM.timeScript(event)\"><option value=\"1\">FIXO</option><option value=\"2\">RANDOM</option></select>&nbsp;:&nbsp;<input id=\"time-coins-fixed\" type=\"time\" value=\"00:01\"><input id=\"time-coins-min\" type=\"time\" style=\"display: none;\" value=\"00:01\">&nbsp;:&nbsp;<input id=\"time-coins-max\" type=\"time\" style=\"display: none;\" value=\"00:05\"></td>\n                            </tr>\n                            <tr>\n                                <td style=\"text-align: center; padding: 4px;\">PRÓXIMA REMESSA DE MOEDAS:</td>\n                                <td id=\"end-time-coins\" style=\"color: #c14848;\">00:00:00</td>\n                            </tr>\n                            <tr>\n                                <th style=\"border-radius: 8px; text-align: center; padding: 4px;\" colspan=\"10\">PUXADOR AUTOMÁTICO DE RECURSOS</th>\n                            </tr>\n                            <tr>\n                                <td align=\"center\">GRUPO PARA PUXAR:</td>\n                                <td>" + (await obsidianScripts.RequestGroups()) + "</td>\n                            </tr>\n                            <tr>\n                                <td align=\"center\">PUXAR RECURSOS A CADA:</td>\n                                <td><select id=\"time-resources\" onchange=\"obsidianScripts.ScriptAM.timeScript(event)\"><option value=\"1\">FIXO</option><option value=\"2\">RANDOM</option></select>&nbsp;:&nbsp;<input id=\"time-resources-fixed\" type=\"time\" value=\"01:00\"><input id=\"time-resources-min\" type=\"time\" style=\"display: none;\" value=\"01:00\">&nbsp;:&nbsp;<input id=\"time-resources-max\" type=\"time\" style=\"display: none;\" value=\"03:00\"></td>\n                            </tr>\n                            <tr>\n                                <td style=\"text-align: center; padding: 4px;\">PRÓXIMA REMESSA DE RECURSOS:</td>\n                                <td id=\"end-time-resources\" style=\"color: #c14848;\">00:00:00</td>\n                            </tr>\n                            <tr>\n                                <td colspan=\"10\"><div id=\"resources-table\" style=\"text-align: center; white-space: nowrap; overflow: auto; max-height: 250px;\"></div></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>\n                <div id=\"user-actions\" style=\"margin: 4px 2px 4px;\"><input type=\"button\" class=\"btn\" onclick=\"obsidianScripts.ScriptAM.RequestXML(this)\" value=\"INICIAR CUNHADOR\"><input type=\"button\" class=\"btn\" onclick=\"obsidianScripts.ScriptAM.stopScript(this)\" value=\"PARAR\"></div>\n                <div id=\"request-resources\" class=\"vis\" style=\"position: absolute; border-radius: 8px; font-size: smaller; font-weight: bold; padding: 0px 1px 0px 5px; bottom: 5px; right: 5px; color: #c9c922; margin: 0px;\">PUXAR RECURSOS:<input type=\"checkbox\" style=\"vertical-align: -3px;\" onclick=\"obsidianScripts.ScriptAM.callResources(this)\"></div>\n            </div>\n            <div id=\"ScriptAC\" style=\"display: none;\">\n                <div class=\"title-content\"><h3 style=\"text-align: center; border-radius: 8px 8px 0px 0px; padding: 6px; margin: 0px;\">AGENDADOR DE COMANDOS</h3></div>\n                <style>#obsidian-scripts .registered td, #obsidian-scripts .combinations-found td, #obsidian-scripts .info-targets td {background-color: #202225 !important;}#obsidian-scripts .close {text-align: center;font-size: larger;font-weight: bold;color: #c14848;cursor: pointer;}#obsidian-scripts .description {text-align: center;font-weight: bold;font-size: 11px;color: #6d6db3;}</style>\n                <div class=\"container\" style=\"overflow: auto; max-height: 200px; outline: 1px solid #7D510F\">\n                    <table width=\"100%\">\n                        <thead>\n                            <tr>\n                                <th style=\"text-align: center\">#</th>\n                                <th>DE:</th>\n                                <th>PARA:</th>\n                                " + game_data.units.slice(0, -1).reduce((_0x45ea25, _0x5621c2) => _0x45ea25 += "<th><label for=\"unit_" + _0x5621c2 + "\"><img src=\"/graphic/unit/unit_" + _0x5621c2 + ".png\"></label></th>", "") + "\n                                <th>SAIDA:</th>\n                                <th>EM:</th>\n                                <th>TIPO:</th>\n                                <th style=\"text-align: center\">#</th>\n                            </tr>\n                        </thead>\n                        <tbody class=\"registered\"></tbody>\n                    </table>\n                </div>\n                <div id=\"infoScript\" style=\"overflow: auto; max-height: 150px; border-bottom: 1px solid #7D510F\">\n                    <table width=\"100%\">\n                        <thead>\n                            <tr>\n                                <th style=\"text-align: center\">#</th>\n                                <th style=\"text-align: center\">ALVO:</th>\n                                <th style=\"text-align: center; font-family: sans-serif;\"><img src=\"/graphic/command/attack_large.png\" style=\"padding: 3px; vertical-align: -5px\">&nbsp;ATAQUES AGENDADOS:</th>\n                                <th style=\"text-align: center; font-family: sans-serif;\"><img src=\"/graphic/command/support.png\" style=\"padding: 3px; vertical-align: -5px\">&nbsp;APOIOS AGENDADOS:</th>\n                                <th style=\"text-align: center; font-family: sans-serif;\"><img src=\"/graphic/command/snob.png\" style=\"padding: 3px; vertical-align: -5px\">&nbsp;NOBRES AGENDADOS:</th>\n                            </tr>\n                        </thead>\n                        <style>.td-targets {text-align: center;font-size: larger;font-family: sans-serif;}</style>\n                        <tbody class=\"info-targets\"></tbody>\n                    </table>\n                </div>\n                <div id=\"logScript\" style=\"overflow: auto; border-radius: 0px 0px 8px; outline: 1px solid #7D510F; width: 400px; height: 100px\"></div>\n                <div class=\"user-actions\">\n                    <table width=\"100%\">\n                        <tbody>\n                            <tr>\n                                <td style=\"font-size: 9px; padding: 0px 0px 0px 4px; color: #c14848\">EMITIR SOM AO ENVIAR COMANDO:&nbsp;<input id=\"sound\" type=\"checkbox\" style=\"vertical-align: -3px;\" onchange=\"obsidianScripts.ScriptAC.saveScript(this)\"></td>\n                            </tr>\n                            <tr>\n                                <td><input type=\"button\" class=\"btn\" style=\"margin: 2px;\" onclick=\"obsidianScripts.openAndHiddenScripts('#container-planner', 'block')\" value=\"PLANEJAMENTO DE COMANDOS\"><input type=\"button\" class=\"btn\" style=\"margin: 2px;\" onclick=\"obsidianScripts.ScriptAC.reorderCommands(this)\" value=\"REORDENAR COMANDOS AGENDADOS\"></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>\n                <div id=\"container-planner\" class=\"vis content-border\" style=\"display: none; border-radius: 8px 8px 8px 8px; z-index: 1; position: fixed; top: 10%; left: 40%; width: auto; height: auto\">\n                    <div class=\"close-Script\" style=\"position: absolute; top: 4px; right: 10px; z-index: 1; font-size: large\"><a onclick=\"obsidianScripts.openAndHiddenScripts('#container-planner', 'none')\" href=\"#\">X</a></div>\n                    <div class=\"vis-train\" style=\"display: none;\">\n                        <table width=\"250\" style=\"border-width: 0px 1px 1px 0px; border: solid #7d510f; border-radius: 0px 0px 8px;\">\n                            <tbody>\n                                <tr>\n                                    <th style=\"text-align: center\" colspan=\"2\"><span id=\"default_name_span\">COMANDO</span></th>\n                                </tr>\n                                <tr>\n                                    <th>DE:</th>\n                                    <td>000|000</td>\n                                </tr>\n                                <tr>\n                                    <th>PARA:</th>\n                                    <td>000|000</td>\n                                </tr>\n                                <tr>\n                                    <th>SAIDA:</th>\n                                    <td id=\"date_arrival\">00/00/0000 00:00:00</td>\n                                </tr>\n                                <tr>\n                                    <th>EM:</th>\n                                    <td style=\"color: #c14848\">0:00:00</td>\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    <div class=\"add-command\" style=\"margin: 6px\"><a href=\"#\" class=\"place-confirm-new-attack\" id=\"troop_confirm_train\" style=\"font-size: 14px; margin-left: 2px\" onclick=\"obsidianScripts.ScriptAC.addCommand(this)\"><img src=\"/graphic/plus.png\" style=\"vertical-align: -3px\">&nbsp;ADICIONAR COMANDO</a></div>\n                    <div class=\"train-container\" style=\"outline: 1px solid #7D510F\">\n                        <table class=\"container\" width=\"100%\">\n                            <style>#obsidian-scripts td[class*=unit-item] {background-color: #202225;}</style>\n                            <tbody>\n                                <tr>\n                                    <th style=\"text-align: center\" colspan=\"100\"><h3 style=\"margin: 0px\">ALTERAR COMANDO</h3></th>\n                                </tr>\n                                <tr>\n                                    <th style=\"text-align: center\">#</th>\n                                    " + game_data.units.slice(0, -1).reduce((_0x411246, _0x1aa9a8) => _0x411246 += "<th width=\"40\"><a href=\"#\" class=\"unit_link\" data-unit=\"" + _0x1aa9a8 + "\"><img src=\"/graphic/unit/unit_" + _0x1aa9a8 + ".png\"></a></th>", "") + "\n                                </tr>\n                                <tr class=\"train-ui\" style=\"display: table-row\">\n                                    <th style=\"text-align: center\">TOTAL</th>\n                                    " + game_data.units.slice(0, -1).reduce((_0x1957f3, _0x4bdc03) => _0x1957f3 += "<td class=\"unit-item-" + _0x4bdc03 + "\">0</td>", "") + "\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    <div class=\"user-actions\"><input type=\"button\" class=\"btn\" style=\"margin: 4px\" onclick=\"obsidianScripts.ScriptAC.pickUpTrain(this)\" value=\"AGENDAR\"></div>\n                    <div class=\"customize\" style=\"outline: 1px solid #7D510F\">\n                        <table class=\"container\" width=\"100%\">\n                            <tbody>\n                                <tr>\n                                    <th style=\"text-align: center\" colspan=\"100\"><h3 style=\"margin: 0px\">CUSTOMIZAR</h3></th>\n                                </tr>\n                                <tr>\n                                    <th style=\"text-align: center\">#</th>\n                                    " + game_data.units.slice(0, -1).reduce((_0x2f01b9, _0x2807c1) => _0x2f01b9 += "<th width=\"40\"><a href=\"#\" class=\"unit_link\" data-unit=\"" + _0x2807c1 + "\"><img src=\"/graphic/unit/unit_" + _0x2807c1 + ".png\"></a></th>", "") + "\n                                </tr>\n                                <tr class=\"custom\" style=\"display: table-row\">\n                                    <th style=\"text-align: center\">UNIDADES</th>\n                                    " + game_data.units.slice(0, -1).reduce((_0x44089c, _0x18c011) => _0x44089c += "<td><input type=\"text\" id=\"unit_" + _0x18c011 + "\" style=\"width: 35px;\" value=\"0\"></td>", "") + "\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    <div class=\"content\" style=\"font-size: 13pt; border-bottom: 1px solid #7D510F\">\n                        <table width=\"100%\">\n                            <style>.td-planner {border: 1px solid light-dark(rgb(118, 118, 118), rgb(133, 133, 133));}.input-planner {outline: none !important;font-size: 13pt;width: 46px;}</style>\n                            <tbody>\n                                <tr>\n                                    <th style=\"text-align: center; width: 150px\">HORA DE CHEGADA:</th>\n                                    <td class=\"td-planner\"><input type=\"text\" class=\"input-planner arrival\" style=\"width: 180px;\" placeholder=\"DD/MM/AAAA 00:00:00:000\"></td>\n                                    <td><select id=\"ms-Script\" onchange=\"obsidianScripts.ScriptAC.msScript(this)\" style=\"font-size: 13pt; width: 100%; height: 25px;\"><option value=\"1\">MS FIXO</option><option value=\"2\">MS RANDOM</option></select></td>\n                                    <td class=\"td-planner\" style=\"display: none;\">MIN:&nbsp;<input id=\"min\" class=\"input-planner\" type=\"number\" value=\"000\"></td>\n                                    <td class=\"td-planner\" style=\"display: none;\">MAX:&nbsp;<input id=\"max\" class=\"input-planner\" type=\"number\" value=\"999\"></td>\n                                    <td class=\"td-planner\">MS:&nbsp;<input id=\"fixed\" class=\"input-planner\" type=\"number\" value=\"500\"></td>\n                                </tr>\n                                <tr>\n                                    <th style=\"text-align: center;\">AGENDAR COMO:</th>\n                                    <td colspan=\"4\"><select class=\"choose\" style=\"font-size: 13pt; width: 100%;\"><option value=\"1\">ATACAR</option><option value=\"2\">APOIAR</option></select></td>\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    <div class=\"textarea-content\" style=\"margin-bottom: -4px\">\n                        <small style=\"margin-left: 2px\">*Os campos abaixo são formatados automaticamente!</small>\n                        <table width=\"100%\">\n                            <style>#obsidian-scripts textarea {background: none;font-size: 11pt;resize: none;width: 96%;height: 50px;}</style>\n                            <tbody>\n                                <tr>\n                                    <th><label for=\"coordinates\">SUAS ALDEIAS:</label></th>\n                                    <th><label for=\"targets\">SEUS ALVOS:</label></th>\n                                </tr>\n                                <tr>\n                                    <td><textarea class=\"coordinates\" placeholder=\"000|000 000|000...\"></textarea></td>\n                                    <td><textarea class=\"targets\" placeholder=\"000|000 000|000...\"></textarea></td>\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    <div class=\"user-actions\"><input type=\"button\" class=\"btn\" style=\"margin: 4px\" onclick=\"obsidianScripts.ScriptAC.startPlanner(this)\" value=\"CALCULAR TEMPOS\"><br><small style=\"margin-left: 2px\">*As unidades podem ser escolhidas clicando na imagem!</small></div>\n                    <div class=\"container\" style=\"overflow: auto; max-height: 300px\">\n                        <table width=\"100%\">\n                            <thead>\n                                <tr class=\"units-select\" style=\"cursor: default\" onclick=\"obsidianScripts.ScriptAC.opacityColumn(event)\">\n                                    <th style=\"text-align: center\">#</th>\n                                    <th>DE:</th>\n                                    <th>PARA:</th>\n                                    " + game_data.units.slice(0, -1).reduce((_0x506b93, _0x2b7f62) => _0x506b93 += "<th><img src=\"/graphic/unit/unit_" + _0x2b7f62 + ".png\"></th>", "") + "\n                                    <th>SAIDA:</th>\n                                    <th>EM:</th>\n                                    <th style=\"text-align: center\">#</th>\n                                    <th style=\"text-align: center\">#</th>\n                                </tr>\n                            </thead>\n                            <tbody class=\"combinations-found\"></tbody>\n                        </table>\n                    </div>\n                </div>\n            </div>\n            <div id=\"ScriptAF\" style=\"display: none;\">\n                <div id=\"title-Script\"><h3 style=\"text-align: center; border-radius: 8px 8px 0px 0px; padding: 6px; margin: 0px;\">AUTOFARM</h3></div>\n                <div id=\"templates\">\n                    <table class=\"vis\" style=\"width: 100%; outline: 1px solid #7D510F; padding: 2px;\">\n                        <tbody>\n                            " + ["template_a", "template_b", "template_c"].reduce((_0x4e3038, _0x95a557, _0x3e1174) => _0x4e3038 + ("<tr>\n                                <td rowspan=\"2\" align=\"center\" width=\"10%\"><a class=\"farm_icon farm_icon_" + _0x95a557.charAt(9) + " decoration\" href=\"#\" onclick=\"return false;\"></a></td>\n                                <th style=\"text-align: center; width: 70px; font-family: math;\">ON/OFF</th>\n                                " + obsidianScripts.ScriptAF.filterUnits().reduce((_0x5ae25d, _0x3e44a3) => _0x5ae25d + ("<th style=\"text-align: center\"><a href=\"#\" class=\"unit_link\" data-unit=\"" + _0x3e44a3 + "\"><img src=\"/graphic/unit/unit_" + _0x3e44a3 + ".png\"></a></th>"), "") + "\n                                <th style=\"text-align: center\"><img src=\"/graphic/rechts.png\"></th>\n                                " + ["blue", "green", "yellow", "red_yellow", "red_blue", "red"].reduce((_0x59b342, _0xa3d73) => _0x59b342 += "<th style=\"text-align: center;\"><img src=\"/graphic/dots/" + _0xa3d73 + ".png\"></th>", "") + "\n                                " + (!_0x3e1174 ? "<td rowspan=\"6\" width=\"15%\" align=\"center\"><div class=\"vis_item\"><input class=\"btn\" type=\"button\" onclick=\"obsidianScripts.ScriptAF.templateSave(this)\" value=\"Salvar\"></div></td>" : "") + "\n                            </tr>\n                            <tr id=\"" + _0x95a557 + "\" " + (_0x3e1174 != 2 ? "" : " onclick=\"obsidianScripts.ScriptAF.RequestTemplateC(event)\"") + ">\n                                " + (_0x3e1174 != 2 ? "<input type=\"hidden\" value=\"31219\"><input type=\"hidden\" value=\"0\">" : "") + "\n                                <td align=\"center\"><input id=\"farm_start\" type=\"checkbox\"></td>\n                                " + obsidianScripts.ScriptAF.filterUnits().reduce((_0xe6cd6e, _0x37ae57) => _0xe6cd6e + (_0x3e1174 != 2 ? "<td align=\"center\"><input name=\"unit_" + _0x37ae57 + "\" type=\"text\" style=\"width: 30px;\" value=\"0\"></td>" : "<td align=\"center\"><input name=\"unit_" + _0x37ae57 + "\" type=\"checkbox\" style=\"width: 30px;\" onclick=\"\" value=\"0\" " + (_0x37ae57 == "spy" ? "disabled" : "") + "></td>"), "") + "\n                                <td align=\"center\"><input id=\"distance\" type=\"text\" style=\"width: 30px;\" value=\"0\"></td>\n                                " + ["blue", "green", "yellow", "red_yellow", "red_blue", "red"].reduce((_0x367b33, _0x42ee13) => _0x367b33 + ("<td align=\"center\"><input id=\"" + _0x42ee13 + "_dots\" type=\"checkbox\" style=\"width: 30px;\"></td>"), "") + "\n                            </tr>"), "") + "\n                        </tbody>\n                    </table>\n                    <div id=\"loading\" style=\"text-align: center;\"></div>\n                </div>\n                <div id=\"options\" class=\"vis\" style=\"font-family: auto; margin: 0px;\">\n                    <table width=\"100%\">\n                        <tbody>\n                            <tr>\n                                <th style=\"text-align: center;\">SELECIONAR GRUPO:</th>\n                                <td>" + (await obsidianScripts.RequestGroups()) + "</td>\n                            </tr>\n                            <tr>\n                                <th style=\"text-align: center;\">INTERVALO ANTES DE VOLTAR A FARMAR:</th>\n                                <td><select id=\"time-farm\" onchange=\"obsidianScripts.ScriptAF.changeSelect(event)\"><option value=\"1\">FIXO</option><option value=\"2\">RANDOM</option></select>&nbsp;:&nbsp;<input id=\"time-farm-fixed\" type=\"time\" value=\"00:01\"><input id=\"time-farm-min\" type=\"time\" style=\"display: none;\" value=\"00:01\">&nbsp;:&nbsp;<input id=\"time-farm-max\" type=\"time\" style=\"display: none;\" value=\"00:05\"></td>\n                            </tr>\n                            <tr>\n                                <th style=\"text-align: center;\">TEMPO ENTRE OS ENVIOS:</th>\n                                <td><select id=\"interval-farm\" onchange=\"obsidianScripts.ScriptAF.changeSelect(event)\"><option value=\"1\">FIXO</option><option value=\"2\">RANDOM</option></select>&nbsp;:&nbsp;<input id=\"interval-farm-fixed\" type=\"number\" style=\"width: 56px;\" value=\"400\"><input id=\"interval-farm-min\" type=\"number\" style=\"display: none; width: 56px;\" value=\"400\">&nbsp;:&nbsp;<input id=\"interval-farm-max\" type=\"number\" style=\"display: none; width: 56px;\" value=\"800\"></td>\n                            </tr>\n                            <tr>\n                                <th style=\"text-align: center; color: #c9c922;\">RAIO DE BUSCA POR ALDEIA EM CAMPOS DO MAPEAMENTO:</th>\n                                <td><input id=\"radius\" type=\"number\" style=\"margin: 1px; width: 66px;\" placeholder=\"EX: 20\" value=\"20\"></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                    <div class=\"info_box\" style=\"margin: 0px 2px;\"><div class=\"content\" style=\"text-align: left; font-family: auto;\">LEIA-ME:<br>- O mapeamento automático utiliza&nbsp;<span style=\"color: #c14848\">SELECIONAR GRUPO - MODELO [A] DE TROPAS</span>&nbsp;ao realizar as buscas.<br>- O raio de buscas será realizado em todas as aldeias do grupo selecionado.</div></div>\n                    <div id=\"user-actions\" style=\"margin-bottom: 6px; margin: 4px;\"><input type=\"button\" class=\"btn\" onclick=\"obsidianScripts.ScriptAF.RequestXML(this)\" value=\"FARMAR\"><input type=\"button\" class=\"btn\" onclick=\"obsidianScripts.ScriptAF.stopScript(this)\" value=\"PAUSAR\"><input type=\"button\" class=\"btn\" value=\"MAPEAMENTO AUTOMÁTICO\" onclick=\"obsidianScripts.ScriptAF.barbarianMapping(this)\"></div>\n                </div>\n            </div>\n            <div id=\"ScriptAI\" style=\"display: none;\">\n                <div class=\"title-content\"><h3 style=\"text-align: center; border-radius: 8px 8px 0px 0px; font-size: 10pt; font-family: sans-serif; font-style: italic; padding: 6px; margin: 0px;\">ETIQUETADOR AUTOMÁTICO</h3></div>\n                <div class=\"user-actions\" style=\"text-align: center; font-size: larger; font-weight: bold; padding: 8px 0px 9px;\"><span style=\"color: #53853b;\">LIGAR</span>/<span style=\"color: #c14848;\">DESLIGAR:</span><input id=\"start\" type=\"checkbox\" style=\"vertical-align: -1px;\" onclick=\"obsidianScripts.ScriptAI.saveScript(event)\"></div>\n                <div class=\"info_box\" style=\"margin: 0px 8px 8px;\"><div class=\"content\" style=\"text-align: left; font-family: auto;\">LEIA-ME:<br>- Todos os ataques chegando são identificado automaticamente, o etiquetador cuidará sozinho de sua tarefa! 😎</div></div>\n            </div>\n            <div id=\"help-me\" style=\"display: none; overflow: auto; font-weight: bold; max-width: 650px; max-height: 400px; padding: 12px; color: rgb(151, 149, 148);\">\n                <h3 style=\"text-align: center; border-radius: 8px; padding: 6px;\">PERGUNTAS E RESPOSTAS</h3>\n                <div class=\"vis\"><h4>COMO DEVO USAR ESTE PACOTE DE SCRIPTS ?</h4><span>1 - Evite usar muitos dias consecutivos sem uma pausa, evite a pausa de \"15min\", isso não resolveria o problema, quando falamos em pausa, deve ser feito realmente uma pausa com demanda de tempo maior para a segurança da conta.<br><br>2 - O princípio anterior se aplica também ao uso da parte da noite, você pode usar a noite, mas evite ultrapassar 2 dias consecutivos, 3 é passível de banimento a noite, um ser humano não consegue ficar 3 dias sem dormir, evite o uso abusivo para construir uma rotina adequada e ideal dentro da sua conta.<br><br>3 - Sobre o uso do AutoFarm e me referindo a qualquer Script de AutoFarm que existe, o sistema assistente de saque é um dos dados que mais realiza requisições, mais do que qualquer outra coisa que você faz dentro do jogo, o uso inadequado do sistema AutoFarm pode arriscar sua conta, use com sabedoria para não haver precauções.</span></div>\n                <div class=\"vis\"><h4>TENHO MUITO RISCO DE TOMAR BANIMENTO ?</h4><span>Resposta: Qualquer Script ilegalizado tem riscos de banimento, não existe Script ilegalizado 100% a prova de Banimento, tenha ciência que você esta usando algo irregular, mas tenha certeza que se você tomar todas as precauções possíveis, sua conta estará muito mais protegida.</span></div>\n                <div class=\"vis\"><h4>POSSO USAR EM SEGUNDO PLANO ?</h4><span>Resposta: Todos os scripts desenvolvidos Pela Obsidian são executáveis em segundo plano. O segundo plano é um recurso disponibilizado pelo navegador para executar tarefas simultâneas mesmo com o navegador minimizado, a Obsidian Scripts é capaz de fazer isso.</span></div>\n                <div class=\"vis\"><h4>ESTE PACOTE DE SCRIPTS ENVIA MUITOS DADOS PARA O SERVIDOR ?</h4><span>Resposta: Não, todos os Scripts desenvolvidos pela Obsidian são maximizados para evitar o máximo de requisições possíveis, todos os dados que a Obsidian envia para o servidor são dados que um jogador normal enviaria sem uso de Scripts, lembre-se que o envio de dados é permitido pelo Servidor, não é irregular essa tarefa.</span></div>\n                <div class=\"vis\"><h4>SOBRE A MENSAGEM: \"BANCO DE DADOS\"</h4><span>A cada exatamente uma hora, a Obsidian solicita uma API universal do Tribal, essa API é uma biblioteca que contém informações necessárias que facilitam os códigos funcionarem com mais leviandade, evitando a demanda busca de informações e uso de dados excessivos do servidor, não se preocupe pois essa atualização é totalmente autorizado pelo fórum internacional do Tribal Wars, não é uma ação irregular e não colocará sua conta em risco.</span></div>\n            </div>\n            <div id=\"pay-Script\" class=\"vis content-border\" style=\"display: none; text-align: center; border-radius: 8px; position: fixed; top: 30%; left: 40%; z-index: 1; height: 300px;\">\n                <div class=\"close-Script\" style=\"position: absolute; top: 9px; right: 10px; z-index: 1; font-size: large\"><a onclick=\"obsidianScripts.openAndHiddenScripts('#pay-Script', 'none')\" href=\"#\">X</a></div>\n                <h3 style=\"border-radius: 8px 8px 0px 0px; padding: 12px; margin: 0px;\">PACOTES DA TECNOLOGIA DA OBSIDIAN</h3>\n                <div><style>#pay-Script img {width: 150px;}</style><img src=\"https://twdevtools.github.io/images/obsidian-scripts.png\"><img src=\"https://twdevtools.github.io/images/obsidian-green.png\"><img src=\"https://twdevtools.github.io/images/obsidian-purple.png\"><img src=\"https://twdevtools.github.io/images/obsidian-black.png\"></div>\n                <div id=\"obsidian-offers\" style=\"display: flex; justify-content: space-evenly; font-weight: bold; color: burlywood;\"><style>#obsidian-offers span {color: #ae441c;}</style><a href=\"https://mpago.la/2pXj5gk\" target=\"_blank\" rel=\"noopener noreferrer\"><button class=\"btn\">PACOTE MENSAL<br><span>R$ 29,90</span></button></a><a href=\"https://mpago.la/1cUdDP3\" target=\"_blank\" rel=\"noopener noreferrer\"><button class=\"btn\">PACOTE 3 MESES<br><span>R$ 69,90</span></button></a><a href=\"https://mpago.la/1xR3KU5\" target=\"_blank\" rel=\"noopener noreferrer\"><button class=\"btn\">PACOTE 6 MESES<br><span>R$ 149,90</span></button></a><a href=\"https://mpago.la/32ReZ59\" target=\"_blank\" rel=\"noopener noreferrer\"><button class=\"btn\">PACOTE ANUAL<br><span>R$ 289,90</span></button></a></div>\n                <div class=\"vis\" style=\"text-align: left; margin: 14px 24px; color: rgb(255, 255, 223);\"><h4 style=\"color: #c9c922;\">IMPORTANTE!</h4><span>&nbsp;Ao realizar o pagamento, enviar o comprovante com o nick do mesmo para autenticação.</a></span></div>\n            </div>\n        </div>";
        var _0x323e77 = new Date(_0x330fe4) - Timing.getCurrentServerTime();
        var _0x104844 = _0x330fe4 != "fixed" ? [_0x330fe4, _0x323e77 / 1000] : ["ILIMITADO!", "00:00:00"];
        if ($(".questlog").append("<div id=\"menu\" style=\"width: 25px; height: 25px; border: 1px solid #603000; background-color: #36393f; margin: 10px; background-image: url(https://twdevtools.github.io/images/obsidian-scripts.png); background-position: center; background-repeat: no-repeat; cursor: pointer; position: relative; text-align: center; box-shadow: rgba(60, 30, 0, 0.7) 2px 2px 2px; border-radius: 3px; background-size: contain;\" onclick=\"obsidianScripts.openAndHiddenScripts('#obsidian-scripts', 'block')\"></div>") && $(document.body).append(_0x50ffef).find("#obsidian-scripts, #container-planner, #pay-Script").draggable() && $("#time-expire").html(_0x104844[0])) {
            $("#timer-end").html(this.secondsToHms(_0x104844[1]));
        }
        this.init.updatedTime = Date.now();
        this.init.lastUpdated = JSON.parse(localStorage.getItem("lastUpdated"));
        if (!this.init.lastUpdated || this.init.lastUpdated + 3600000 <= this.init.updatedTime) {
            if (!UI.SuccessMessage("Atualizando o Banco de dados, aguarde...") && (await this.RequestAPI()) && this.indexedDB("newUpdated")) {
                localStorage.setItem("lastUpdated", JSON.stringify(this.init.updatedTime));
            }
        } else {
            this.indexedDB("updatedFalse");
        }
        new MutationObserver(_0x49f9a7 => $("#time").html("<strong>" + _0x49f9a7[0].target.nextElementSibling.innerText + " " + _0x49f9a7[0].target.innerText + "</strong>")).observe($(".server_info")[0], {
            subtree: true,
            childList: true
        });
        Timing.tickHandlers.timers.handleTimerEnd = function (_0xfbc13d) {
            return (this.id != "timer-end" && this.closest("tbody").className == "combinations-found" || this.closest("div").id == "resources-table") && this.closest("tr").remove();
        };
        this.ScriptAC.init();
        this.ScriptAF.init();
        return this.ScriptAI.init();
    },
    indexedDB: function (_0x137583) {
        var _0x1269e2 = indexedDB.open("villagesAPI");
        _0x1269e2.onupgradeneeded = _0x1c2f71 => _0x1c2f71.target.result.createObjectStore("APIUpdated", {
            autoIncrement: true
        }).add(window.APIUpdated);
        return _0x1269e2.onsuccess = function (_0x1f1a36) {
            var _0x1a4c59 = _0x1f1a36.target.result.transaction("APIUpdated", "readwrite");
            var _0x4a38af = _0x1a4c59.objectStore("APIUpdated");
            var _0x49e4a8 = _0x4a38af.getAll();
            return _0x49e4a8.onsuccess = ({
                target: {
                    result: [_0x4f0f37]
                }
            }) => _0x137583 != "updatedFalse" ? _0x4a38af.clear() && _0x4a38af.add(window.APIUpdated) : window.APIUpdated = _0x4f0f37;
        };
    },
    openAndHiddenScripts: function (_0x362ad3, _0x5b9bb8) {
        return $(_0x362ad3).css("display", _0x5b9bb8);
    },
    openAndHiddenTabs: function (_0x2eac9f) {
        return $("#history-Obsidian, #welcome-Script, #ScriptAC, #ScriptAF, #ScriptAM, #ScriptAI, #help-me").css("display", "none") && $(_0x2eac9f).css("display", "block");
    },
    RequestAPI: function (_0x45622f) {
        return new Promise(async _0x42ad58 => {
            try {
                window.APIUpdated = {
                    database: await this.RequestData(),
                    units: await this.RequestUnits()
                };
                return !UI.SuccessMessage("Banco de dados atualizado!") && _0x42ad58(!undefined);
            } catch (_0x8c5d3d) {
                return !UI.ErrorMessage("O Banco de dados não foi atualizado! erro: " + _0x8c5d3d.message);
            }
            ;
        });
    },
    RequestData: function (_0x487a23) {
        return $.ajax({
            url: "/map/village.txt",
            method: "GET"
        }).then(_0x3da85a => {
            var _0x22c7bb = {};
            _0x3da85a.split("\n").forEach(_0x5ec1f2 => {
                var _0x51d020 = _0x5ec1f2.split(",");
                _0x22c7bb[_0x51d020[2] + "|" + _0x51d020[3]] = {
                    villageID: _0x51d020[0],
                    playerID: _0x51d020[4]
                };
            });
            return _0x22c7bb;
        });
    },
    RequestUnits: function (_0x4ac039) {
        return $.get("/interface.php?func=get_unit_info").then(_0x3c3731 => {
            var _0x221869 = {};
            $(_0x3c3731).find("config").children().each((_0x5c3de8, _0x4fa064) => _0x221869[_0x4fa064.tagName] = Number($(_0x4fa064).find("speed").prop("textContent")));
            return _0x221869;
        });
    },
    RequestGroups: function (_0x6b07df) {
        return $.get(game_data.link_base_pure + "groups&mode=overview&ajax=load_group_menu&").then(_0x40473d => {
            var _0x5ab0c8 = "<select class=\"optionGroup\" style=\"width: 100%;\">";
            _0x40473d.result.forEach(_0x54c32b => {
                if (_0x54c32b.type == "separator") {
                    return _0x5ab0c8 += "<option disabled></option>";
                } else {
                    return _0x5ab0c8 += "<option value=\"" + _0x54c32b.group_id + "\">" + _0x54c32b.name + "</option>";
                }
            });
            _0x5ab0c8 += "</select>";
            return _0x5ab0c8;
        });
    },
    RequestJSON: function (_0x306080) {
        //     var _0x42e5ee = document.currentScript.src;
        return $.ajax("https://twdevtools.github.io/database/scripts/authentication.txt").then(_0x439adc => {
            var _0x2dbdca = JSON.parse(_0x439adc)["obsidian-authentication"][game_data.player.name];
            this.init(_0x2dbdca);
        });
    },
    secondsToHms: function (_0x43fec1) {
        var _0x27d2a4 = Math.floor(_0x43fec1 / 3600);
        var _0x2d1919 = Math.floor(_0x43fec1 % 3600 / 60);
        var _0x3ab3f0 = Math.floor(_0x43fec1 % 60);
        return ("" + _0x27d2a4).padStart(2, "0") + ":" + ("" + _0x2d1919).padStart(2, "0") + ":" + ("" + _0x3ab3f0).padStart(2, "0");
    },
    version: "1.0.0",
    lastUpdated: "10/05/2024 22:00:00",
    author: "K I N G S",
    authorContact: "+55 48-98824-2773"
};
this.obsidianScripts.RequestJSON();